﻿using System;
using Blobby.Core.Components;
using Microsoft.Xna.Framework;

namespace Blobby.Core.Physics
{
    public class CollisionInfo
    {
        public ColliderComponent Left { get; set; }
        public ColliderComponent Right { get; set; }
        public float PenetrationDepth { get; set; }
        public Vector2 PenetrationVector { get; set; }

        public static CollisionInfo Inverse(CollisionInfo info)
        {
            CollisionInfo result = new CollisionInfo();
            result.Left = info.Right;
            result.Right = info.Left;
            result.PenetrationDepth = info.PenetrationDepth;
            result.PenetrationVector = -info.PenetrationVector;
            return result;
        }
    }
}