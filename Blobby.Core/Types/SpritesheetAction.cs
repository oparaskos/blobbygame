﻿using Microsoft.Xna.Framework;

namespace Blobby.Core
{
    public class SpritesheetAction
    {
        public const string DEFAULT_ACTION_NAME = "idle";

        public string Name { get; set; }
        public int RowNumber { get; set; }
        public int NumFrames { get; set; }
        public int Duration { get; set; }
        public int FrameWidth { get; set; } = 64;
        public int FrameHeight { get; set; } = 64;
        // TODO: Play type, eg looping, stopAtEnd, pingPong

        public int GetFrame(long actionStartTime, float playRate = 1.0f)
        {
            long now = System.Environment.TickCount;
            long elapsed = now - actionStartTime;
            long timePerFrame = (long) ((Duration / NumFrames) * playRate);
            return (int)(elapsed / timePerFrame);
        }

        public Rectangle GetFrameBounds(int frame)
        {
            return new Rectangle(
                (frame % NumFrames) * FrameWidth,
                RowNumber * FrameHeight,
                FrameWidth,
                FrameHeight
            );
        }

        public static SpritesheetAction Parse(string actionData)
        {
            string[] parts = actionData.Split(':');
            SpritesheetAction action = new SpritesheetAction();
            action.Name = parts[0];
            int rowNum, duration, numFrames, frameWidth, frameHeight;
            bool success = action.Name != null;
            success &= int.TryParse(parts[1], out rowNum);
            success &= int.TryParse(parts[2], out numFrames);
            success &= int.TryParse(parts[3], out duration);
            bool hasWidth = true, hasHeight = true;
            hasWidth &= int.TryParse(parts[4], out frameWidth);
            hasHeight &= int.TryParse(parts[5], out frameHeight);
            if (!success && hasWidth ^ hasHeight)
            {
                throw new System.ArgumentException("Expected to be in the format '[name (string)]:[row (int, pixels)]:[numFrames (int)]:[duration (int, milliseconds)]:[width (int, pixels, optional)]:[height (int, pixels, optional)]'");
            }
            action.RowNumber = rowNum;
            action.NumFrames = numFrames;
            action.Duration = duration;
            action.FrameWidth = frameWidth;
            action.FrameHeight = frameHeight;
            return action;
        }
    }
}
