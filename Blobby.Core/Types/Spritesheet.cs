using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Blobby.Core
{
    public class Spritesheet
    {
        public Vector2 Offset { get; set; }
        private Rectangle _sourceRectangle = Rectangle.Empty;
        public Texture2D _spriteTexture = null;
        public int _frameWidth = 0;
        public Texture2D SpriteTexture
        {
            get
            {
                return _spriteTexture;
            }
            set
            {
                _spriteTexture = value;
                CalculateFrameWidth();
            }
        }

        private void CalculateFrameWidth()
        {
            if (_spriteTexture != null)
            {
                _frameWidth = Width / NumFrames;
                _sourceRectangle = new Rectangle(0, 0, _frameWidth, Height);
            }
        }

        public int Width { get { return SpriteTexture.Width; } }
        public int Height { get { return SpriteTexture.Height; } }
        public bool Visible { get; set; } = true;
        private int _numFrames = 1;
        public int NumFrames
        {
            get
            {
                return _numFrames;
            }
            set
            {
                if (value < 1)
                {
                    throw new ArgumentNullException("value", "must be greater than one");
                }
                _numFrames = value;
                CalculateFrameWidth();
            }
        }
        public bool PastEnd { get; private set; } = false;
        public float Layer { get; set; } = 0f;
        public float Scale { get; set; } = 1f;
        public int Frame
        {
            get
            {
                return _sourceRectangle.X % _frameWidth;
            }
            set
            {
                if (value > NumFrames)
                {
                    PastEnd = true;
                }
                _sourceRectangle.X = _frameWidth * value;
            }
        }

        public Spritesheet(Texture2D texture, int numFrames = 1, float layer = 0)
        {
            SpriteTexture = texture;
            Offset = Vector2.Zero;
            Layer = ((-layer + 100) / 200);
            NumFrames = numFrames;
        }

        internal void Draw(SpriteBatch spriteBatch, Vector2 position, float angle, int? frame = null)
        {
            if (Visible)
            {
                if (frame != null)
                {
                    Frame = (int)frame;
                }
                Vector2 origin = new Vector2(((Width / NumFrames) / 2), (Height / 2));
                spriteBatch.Draw(SpriteTexture, position, _sourceRectangle, Color.White,
                    angle, Vector2.Add(origin, Offset), Scale, SpriteEffects.None, Layer);
            }
        }
    }
}