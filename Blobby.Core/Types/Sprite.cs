using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blobby.Core
{
    public class Sprite
    {
        public Vector2 Offset { get; set; }
        public Texture2D SpriteTexture { get; set; }
        public int Width { get { return SpriteTexture.Width; } }
        public int Height { get { return SpriteTexture.Height; } }
        public bool Visible { get; set; } = true;
        public Rectangle? SourceRectangle { get; set; } = null;
        public float Layer {get; set;} = 0f;
        public float Scale {get; set;} = 1f;
        public Color _realColour = Color.White;
        public Color _baseColour = Color.White;
        public float _baseAlpha = 1.0f;

        public Color Colour
        {
            get
            {
                return _baseColour;
            }
            set
            {
				_baseColour = value;
				_realColour = premultiply(_baseColour, _baseAlpha);
            }
        }

        public float Alpha 
        {
            get
            {
                return _baseAlpha;
			}
			set
			{
                _baseAlpha = value;
                _realColour = premultiply(_baseColour, _baseAlpha);
                
            }
        }

        public Sprite(Texture2D texture)
        {
            SpriteTexture = texture;
            Offset = Vector2.Zero;
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 position, float angle)
        {
            if (Visible)
            {
				Vector2 origin = new Vector2((Width / 2), (Height / 2));
                spriteBatch.Draw(
                    SpriteTexture, 
                    position,
                    SourceRectangle,
                    _realColour,
                    angle,
                    Vector2.Add(origin, Offset),
                    Scale,
                    SpriteEffects.None, 
                    (-Layer + 100) / 200);
            }
		}

		private static Color premultiply(Color baseColour, float baseAlpha)
		{
			byte r = (byte)MathHelper.Clamp(baseAlpha * baseColour.R, byte.MinValue, byte.MaxValue);
			byte g = (byte)MathHelper.Clamp(baseAlpha * baseColour.G, byte.MinValue, byte.MaxValue);
			byte b = (byte)MathHelper.Clamp(baseAlpha * baseColour.B, byte.MinValue, byte.MaxValue);
			byte a = (byte)MathHelper.Clamp(baseAlpha * byte.MaxValue, byte.MinValue, byte.MaxValue);
			return new Color(r, g, b, a);
		}
    }
}