using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blobby.Core.Components
{
    public class ResizableLineSprite
    {

        public int Width = 100;

        public Texture2D LeftEdge = null;
        public Texture2D RightEdge = null;
        public Texture2D BackgroundTexture = null;

        public Vector2 Position { get; set; } = Vector2.Zero;

        public float Angle { get; set; } = 0.0f;
        public float Layer = 0.1f;

        public void Draw(SpriteBatch spriteBatch)
        {
			int width = Width - (LeftEdge.Width + RightEdge.Width);
            spriteBatch.Draw(LeftEdge, 
                             Position,
                             null,
                             Color.White,
                             Angle, 
                             -new Vector2(0, -LeftEdge.Height / 2), 
                             1.0f, 
                             SpriteEffects.None,
                             Layer);
            
            spriteBatch.Draw(RightEdge,
                             Position,
                             null,
                             Color.White,
                             Angle, 
                             -new Vector2(Width - RightEdge.Width, -RightEdge.Height / 2),
                             1.0f,
                             SpriteEffects.None,
                             Layer);
            
            spriteBatch.Draw(BackgroundTexture,
                             Position, 
                             new Rectangle(0, 0, width, BackgroundTexture.Height),
                             Color.White, 
                             Angle, 
                             -new Vector2(LeftEdge.Width, -BackgroundTexture.Height / 2), 
                             1.0f, 
                             SpriteEffects.None,
                             Layer + 0.1f);
        }
    }
}