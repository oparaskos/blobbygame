using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blobby.Core.Components
{
    // TODO: Extract interface to render any sprite that can be sized to a given rectangle.
    public class ResizableRectangleSprite : IResizableRectangleSprite
    {
        public Color Color { get; set; } = Color.White;
        public Rectangle Bounds { get; set; } = new Rectangle(-150, -75, 300, 150);
        public Vector2 Position { get; set; } = Vector2.Zero;
        public float Angle { get; set; } = 0.0f;
        public float Layer { get; set; } = 0.1f;

        public bool IsLoaded
        {
            get
            {
                return (BackgroundTexture != null);
            }
        }

        public float MinWidth
        {
            get
            {
                if (BackgroundTexture == null)
                {
                    return 0;
                }
                float minLeftWidth = MathHelper.Max(LeftBorder.Width, MathHelper.Max(CornerBottomLeft.Width, CornerTopLeft.Width));
                float minRightWidth = MathHelper.Max(RightBorder.Width, MathHelper.Max(CornerBottomRight.Width, CornerTopRight.Width));
                return minLeftWidth + minRightWidth;
            }
        }

        public Texture2D LeftBorder = null;
        public Texture2D RightBorder = null;
        public Texture2D TopBorder = null;
        public Texture2D BottomBorder = null;
        public Texture2D CornerTopLeft = null;
        public Texture2D CornerTopRight = null;
        public Texture2D CornerBottomLeft = null;
        public Texture2D CornerBottomRight = null;
        public Texture2D BackgroundTexture = null;
        
        public void Draw(SpriteBatch spriteBatch)
        {
            // TODO: adjust so that the outer edges of the images meet the bounds
            Vector2 topLeft = new Vector2(Bounds.Left, Bounds.Top);
            Vector2 bottomLeft = new Vector2(Bounds.Left, Bounds.Bottom - CornerBottomLeft.Height);
            Vector2 topRight = new Vector2(Bounds.Right - CornerTopRight.Width, Bounds.Top);
            Vector2 bottomRight = new Vector2(Bounds.Right - CornerBottomRight.Width, Bounds.Bottom - CornerBottomRight.Height);
            DrawBackground(spriteBatch);
            DrawLeftEdge(spriteBatch);
            DrawRightEdge(spriteBatch);
            DrawTopEdge(spriteBatch);
            DrawBottomEdge(spriteBatch);
            DrawSprite(spriteBatch, CornerTopLeft, topLeft);
            DrawSprite(spriteBatch, CornerBottomLeft, bottomLeft);
            DrawSprite(spriteBatch, CornerTopRight, topRight);
            DrawSprite(spriteBatch, CornerBottomRight, bottomRight);
        }

        private void DrawBackground(SpriteBatch spriteBatch)
        {
            Rectangle bounds = new Rectangle(Bounds.Top + 3, Bounds.Left + 3, Bounds.Width - 6, Bounds.Height - 6);
            Vector2 topLeft = new Vector2(Bounds.Left + 3, Bounds.Top + 3);
            spriteBatch.Draw(BackgroundTexture, Position, bounds, Color, Angle, -topLeft, 1.0f, SpriteEffects.None, Layer + 0.1f);
        }

        private void DrawLeftEdge(SpriteBatch spriteBatch)
        {
            Vector2 topLeft = new Vector2(Bounds.Left, Bounds.Top);
            Rectangle leftEdge = new Rectangle(0, 0, LeftBorder.Width, Bounds.Height - CornerTopLeft.Height - CornerBottomLeft.Height);
            Vector2 leftEdgeOffset = new Vector2(topLeft.X, topLeft.Y + CornerTopLeft.Height);
            spriteBatch.Draw(LeftBorder, Position, leftEdge, Color, Angle, -leftEdgeOffset, 1.0f, SpriteEffects.None, Layer);
        }

        private void DrawRightEdge(SpriteBatch spriteBatch)
        {
            Vector2 topRight = new Vector2(Bounds.Right - RightBorder.Width, Bounds.Top);
            Rectangle rightEdge = new Rectangle(0, 0, RightBorder.Width, Bounds.Height - CornerTopRight.Height - CornerBottomRight.Height);
            Vector2 rightEdgeOffset = new Vector2(topRight.X, topRight.Y + CornerTopRight.Height);
            spriteBatch.Draw(RightBorder, Position, rightEdge, Color, Angle, -rightEdgeOffset, 1.0f, SpriteEffects.None, Layer);
        }

        private void DrawTopEdge(SpriteBatch spriteBatch)
        {
            Vector2 topLeft = new Vector2(Bounds.Left, Bounds.Top);
            Rectangle topEdge = new Rectangle(0, 0, Bounds.Width - CornerTopLeft.Width - CornerTopRight.Width, TopBorder.Height);
            Vector2 topEdgeOffset = new Vector2(topLeft.X + CornerTopLeft.Width, topLeft.Y);
            spriteBatch.Draw(TopBorder, Position, topEdge, Color, Angle, -topEdgeOffset, 1.0f, SpriteEffects.None, Layer);
        }

        private void DrawBottomEdge(SpriteBatch spriteBatch)
        {
            Vector2 bottomLeft = new Vector2(Bounds.Left, Bounds.Bottom - BottomBorder.Height);
            Rectangle bottomEdge = new Rectangle(0, 0, Bounds.Width - CornerBottomLeft.Width - CornerBottomRight.Width, BottomBorder.Height);
            Vector2 bottomEdgeOffset = new Vector2(bottomLeft.X + CornerBottomLeft.Width, bottomLeft.Y);
            spriteBatch.Draw(BottomBorder, Position, bottomEdge, Color, Angle, -bottomEdgeOffset, 1.0f, SpriteEffects.None, Layer);
        }

        private void DrawSprite(SpriteBatch spriteBatch, Texture2D sprite, Vector2 offset)
        {
            spriteBatch.Draw(sprite, Position, null, Color, Angle, -offset, 1.0f, SpriteEffects.None, Layer);
        }
    }
}