﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blobby.Core.Components
{
    public interface IResizableRectangleSprite
    {
        float Angle { get; set; }
        Rectangle Bounds { get; set; }
        Color Color { get; set; }
        float Layer { get; set; }
        Vector2 Position { get; set; }
        bool IsLoaded { get; }
        float MinWidth { get; }

        void Draw(SpriteBatch spriteBatch);
    }
}