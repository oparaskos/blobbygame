﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Blobby.Core;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Blobby.MyGame.Components
{
    public class HueSaturationValue
    {
        public float Hue { get; set; }
        public float Saturation { get; set; }
        public float Value { get; set; }

        public HueSaturationValue()
        {

		}

        public HueSaturationValue(Color color)
		{
			int max = Math.Max(color.R, Math.Max(color.G, color.B));
            int min = Math.Min(color.R, Math.Min(color.G, color.B));
            float delta = max - min;
			Value = max / 255.0f;

            if(max != 0)
            {
                Saturation = delta / max;
            } 
            else
            {
                Saturation = 0;
            }

            if(color.R == max) 
            {
                Hue = (color.G - color.B) / delta;
            }
            else if (color.G == max)
            {
                Hue = 2 + (color.B - color.R) / delta;
			}
			else
			{
                Hue = 4 + (color.R - color.G) / delta;
            }

            Hue *= 60;
            Hue += 360;
		}

        public HueSaturationValue(float hue, float saturation, float value)
		{
			Hue = hue;
            Saturation = saturation;
            Value = value;
        }

        public Color ToColor()
        {
            if (Hue > 359.999f)
            {
                Hue = 0.0f;
            }
            else
            {
                Hue /= 60.0f;
            }
            float fract = Hue - (float)Math.Floor(Hue);
            float V = Value;
            float P = Value * (1.0f - Saturation);
            float Q = Value * (1.0f - Saturation * fract);
            float T = Value * (1.0f - Saturation * (1.0f - fract));

            if (0.0f <= Hue && Hue < 1.0f)
            {
                return new Color(V, T, P);
            }
            else if (1.0f <= Hue && Hue < 2.0f)
            {
                return new Color(Q, V, P);
            }
            else if (2.0f <= Hue && Hue < 3.0f)
            {
                return new Color(P, V, T);
            }
            else if (3.0f <= Hue && Hue < 4.0f)
            {
                return new Color(P, Q, V);
            }
            else if (4.0f <= Hue && Hue < 5.0f)
            {
                return new Color(T, P, V);
            }
            else if (5.0f <= Hue && Hue < 6.0f)
            {
                return new Color(V, P, Q);
            }
            return new Color(0.0f, 0.0f, 0.0f);
        }
    }
}