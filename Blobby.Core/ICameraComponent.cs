using Microsoft.Xna.Framework;

namespace Blobby.Core
{
    public interface ICameraComponent
    {
        float Zoom { get; }
        Rectangle Bounds { get; }
        bool Active { get; }
        Matrix Transform { get; }
    }
}