﻿using Microsoft.Xna.Framework.Input;

namespace Blobby.Core
{
    public interface IConfigurableKeyboardInput
    {
        void RegisterKeyboardKey(string inputName, Keys key);
        void RegisterKeyboardKey(string v, params Keys[] keys);
    }
}