﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Runtime.Serialization;

namespace Blobby.Core
{
    [Serializable]
    public class RootGameObject : GameObject
    {

        public RootGameObject() : base("root") {
            subsystems = new Dictionary<Type, Subsystem>();
        }

        public void SetGame(IBlobbyGame value)
        {
            game = value;
		}

		public T AddSubsystem<T>() where T : Subsystem
		{
			return (T)AddSubsystem(typeof(T));
		}

        public Subsystem AddSubsystem(Type t)
		{
            if(!t.IsSubclassOf(typeof(Subsystem)))
            {
                throw new ArgumentException("Subsystems must derive from 'Subsystem'");
            }
            Subsystem subsystem = (Subsystem)Activator.CreateInstance(t);
			subsystems.Add(t, subsystem);
			return subsystem;
		}

        public override void Initialize() {
            InitializeSubsystems();
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
		{
			UpdateSubsystems(gameTime);
            base.Update(gameTime);
        }

        public override void Draw(SpriteBatch spriteBatch)
		{
			DrawSubsystems(spriteBatch);
            base.Draw(spriteBatch);
		}

		private void InitializeSubsystems()
		{
			foreach (Subsystem subsystem in subsystems.Values)
            {
                Console.Out.WriteLine("Initialising " + subsystem.GetType().FullName);
                subsystem.Initialize();
			}
		}

        private void UpdateSubsystems(GameTime gameTime)
		{
			foreach (Subsystem subsystem in subsystems.Values)
            {
                if (subsystem.Enabled)
                {
                    subsystem.Update(gameTime);
                }
			}
		}

        private void DrawSubsystems(SpriteBatch spriteBatch)
		{
			foreach (Subsystem subsystem in subsystems.Values)
			{
				subsystem.Draw(spriteBatch);
			}
		}
        
    }
}
