﻿using Microsoft.Xna.Framework.Content;

namespace Blobby.Core
{
    public class DefaultContentManager : IContentManager
    {
        ContentManager BaseContentManager { get; }

        public DefaultContentManager(ContentManager baseContentManager)
        {
            BaseContentManager = baseContentManager;
        }

        public string RootDirectory
        {
            get
            {
                return BaseContentManager.RootDirectory;
            }
            set
            {
                BaseContentManager.RootDirectory = value;
            }
        }

        public T Load<T>(string v)
        {
            return BaseContentManager.Load<T>(v);
        }
    }
}
