using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Runtime.Serialization;

namespace Blobby.Core
{
    [Serializable]
    public class BlobbyComponent : IGameComponent, IDisposable, IUpdateable, ISerializable
    {
        public event EventHandler<EventArgs> EnabledChanged;
        public event EventHandler<EventArgs> UpdateOrderChanged;
        public event EventHandler<EventArgs> OwnerChanged;

        public bool IsDisposed { get; private set; } = false;
        public GameObject Owner
        {
            get
            {
                return _owner;
            }
            set
            {
                if (_owner != value)
                {
                    _owner = value;
                    OwnerChanged?.Invoke(this, null);
                }
            }
        }
        public bool Enabled
        {
            get
            {
                return _enabled && (Owner == null || Owner.Enabled);
            }
            set
            {
                if (_enabled != value)
                {
                    _enabled = value;
                    EnabledChanged?.Invoke(this, null);
                }
            }
        }
        public int UpdateOrder
        {
            get
            {
                return _updateOrder;
            }
            set
            {
                if (_updateOrder != value)
                {
                    _updateOrder = value;
                    UpdateOrderChanged?.Invoke(this, null);
                }
            }
        }

        public IBlobbyGame Game
        {
            get
            {
                if (_game == null)
                {
                    _game = Owner.Game;
                }
                return _game;
            }
        }

        private GameObject _owner = null;
        private IBlobbyGame _game = null;
        private bool _enabled = true;
        private int _updateOrder = 0;

        public virtual void Initialize() { }
        public virtual void Update(GameTime gameTime) { }
        public virtual void Draw(SpriteBatch spriteBatch) { }
        public virtual void LoadContent() { }
        public void Dispose() { Dispose(true); }
        protected virtual void Dispose(bool nativeOnly)
        {
            IsDisposed = true;
            GC.SuppressFinalize(this);
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Enabled", Enabled);
            info.AddValue("UpdateOrder", UpdateOrder);
            info.AddValue("Assembly", GetType().Assembly.FullName);
            info.AddValue("Type", GetType().FullName);
            info.AddValue("Owner", Owner.Name);
        }
    }
}