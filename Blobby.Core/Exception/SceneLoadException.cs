﻿using System;
using System.Runtime.Serialization;
namespace Blobby.Core
{
    [Serializable]
    public class SceneLoadException : FormatException
    {
        public SceneLoadException()
        {
        }

        protected SceneLoadException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }

        public SceneLoadException(string message)
            : base(message)
        {
        }

        public SceneLoadException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
