﻿using Microsoft.Xna.Framework.Input;

namespace Blobby.Core
{
    public interface IConfigurableGamepadInput
    {
        void RegisterGamepadButton(string inputName, Buttons button);
        void RegisterGamepadButton(string inputName, params Buttons[] buttons);
    }
}