﻿using System;
using System.Collections.Generic;
using Blobby.Core;
using Microsoft.Xna.Framework;

namespace Blobby.Core.Steamworks
{
    public class SteamworksSubsystem : Subsystem
    {
        public Facepunch.Steamworks.Client SteamworksClient { get; private set; }

        public bool IsSteamRunning
        {
            get
            {
                if (null == SteamworksClient)
                {
                    return false;
                }
                return SteamworksClient.IsValid;
            }
        }

        public SteamworksSubsystem()
        {
        }

        /// <summary>
        /// Needs to be called before the GraphicsDevice is initialised.
        /// </summary>
        public static void PreInitialize(string[] args)
        {
            Facepunch.Steamworks.Config.ForcePlatform(DetectOS(), DetectArch());
            new Facepunch.Steamworks.Client(480);
        }

        public override void Initialize()
        {
            if (IsSteamRunning)
            {
                SteamworksClient = Facepunch.Steamworks.Client.Instance;
                SteamworksClient.Achievements.Trigger("ACH_WIN_ONE_GAME");
                Console.Out.WriteLine("Steam is started");
            }
            else
            {
                Enabled = false;
                Console.Error.WriteLine("Error, Steam may not be running.");
            }
        }

        private static Facepunch.Steamworks.Architecture DetectArch()
        {
            if (IntPtr.Size == 4)
            {
                Console.WriteLine("Detected x86 architecture");
                return Facepunch.Steamworks.Architecture.x86;
            }
            else
            {
                Console.WriteLine("Detected x64 architecture");
                return Facepunch.Steamworks.Architecture.x64;
            }
        }

        private static Facepunch.Steamworks.OperatingSystem DetectOS()
        {
            OperatingSystem os = Environment.OSVersion;
            PlatformID pid = os.Platform;
            switch (pid)
            {
                case PlatformID.Win32NT:
                case PlatformID.Win32S:
                case PlatformID.Win32Windows:
                case PlatformID.WinCE:
                    Console.WriteLine("This is a Windows operating system.");
                    return Facepunch.Steamworks.OperatingSystem.Windows;
                case PlatformID.Unix:
                    Console.WriteLine("This is a Unix operating system.");
                    return Facepunch.Steamworks.OperatingSystem.Linux;
                default:
                    Console.WriteLine("This platform identifier is probably OSX.");
                    return Facepunch.Steamworks.OperatingSystem.Osx;
            }
        }

        public override void Update(GameTime gameTime)
        {
            SteamworksClient?.Update();
        }

        protected override void Dispose(bool nativeOnly)
        {
            SteamworksClient?.Dispose();
            base.Dispose(nativeOnly);
        }
    }
}
