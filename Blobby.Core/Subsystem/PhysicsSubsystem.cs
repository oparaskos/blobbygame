﻿using Dynamics = FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using System.Linq;
using FarseerPhysics.Collision;
using FarseerPhysics.Dynamics;
using System.Collections.Generic;
using System;

namespace Blobby.Core.Physics
{
    public class PhysicsSubsystem : Subsystem
    {
        public const float STOPPED_SPEED = 0.001f;
        public Dynamics.World World { get; set; }

        public PhysicsSubsystem() {
            World = new Dynamics.World(Vector2.Zero);
        }

        public override void Update(GameTime gameTime) {
             World.Step(gameTime.ElapsedGameTime.Milliseconds);
        }

        public IEnumerable<GameObject> QueryGameObjectsAtLocation(Vector2 coordinates, float radius = 0.5f)
        {
            AABB aabb = new AABB(coordinates - new Vector2(radius, radius), 2 * radius, 2 * radius);
            IEnumerable<Fixture> fixtures = World.QueryAABB(ref aabb);
            var objects= fixtures
                //.Where((Fixture f) => (f.Body.Position - coordinates).LengthSquared() < Math.Pow(radius, 2))
                .Select((Fixture f) => ((BlobbyComponent)f.UserData))
                .Where((BlobbyComponent x) => x.Enabled)
                .Select((BlobbyComponent c) => c.Owner)
                .Where((GameObject o) => o.Enabled);
            return objects;
        }
    }
}