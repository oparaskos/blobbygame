﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using Blobby.Core;

namespace Blobby.Core
{

    public class SceneLoader
    {
        public RootGameObject LoadScene(string filename)
        {
            using (StreamReader r = new StreamReader(filename))
            {
                return LoadSceneFromMemory(r.ReadToEnd());
            }
        }

        public RootGameObject LoadSceneFromMemory(string json)
        {
            RootGameObject root = new RootGameObject();
            ReadJsonGameObject(JsonConvert.DeserializeObject<JsonGameObject>(json), root);
            InitializeSubsystems(root);
            return root;
		}

        private void InitializeSubsystems(RootGameObject root)
        {
            foreach (Assembly loadedAssembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                foreach(Type type in loadedAssembly.GetExportedTypes())
                {
                    if(type.IsSubclassOf(typeof(Subsystem)))
                    {
                        if(!root.HasSubsystem(type))
                        {
                            root.AddSubsystem(type);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Read game object from json into the provided gameObject
        /// </summary>
        /// <returns>The json game object.</returns>
        /// <param name="jsonGameObject">Json game object data from the file.</param>
        /// <param name="gameObject">Game object to read the data into.</param>
        private GameObject ReadJsonGameObject(JsonGameObject jsonGameObject, GameObject gameObject)
        {
            // gameObject.Name = name; // So that later i can reference other objects by name.
            gameObject.RelativePosition = new Vector2(jsonGameObject.Position[0], jsonGameObject.Position[1]);
            gameObject.RelativeRotation = jsonGameObject.Angle;
            ReadGameObjectComponents(jsonGameObject, gameObject);
            ReadGameObjectChildren(jsonGameObject, gameObject);
            return gameObject;
        }

        private void ReadGameObjectChildren(JsonGameObject jsonGameObject, GameObject gameObject)
        {
            if (jsonGameObject.Children != null)
            {
                foreach (string childName in jsonGameObject.Children.Keys)
                {
                    JsonGameObject childData = jsonGameObject.Children[childName];
                    GameObject child = new GameObject(childName);
                    gameObject.AddChild(ReadJsonGameObject(childData, child));
                }

            }
        }

        private void ReadGameObjectComponents(JsonGameObject jsonGameObject, GameObject gameObject)
        {
            if (jsonGameObject.Components != null)
            {
                foreach (JsonComponent component in jsonGameObject.Components)
                {
                    Assembly asm = GetAssemblyFromName(component);
                    Type type = asm.GetType(component.TypeName);
                    if (type.IsSubclassOf(typeof(BlobbyComponent)))
                    {
						BlobbyComponent addedComponent = gameObject.AddComponent(type);
                        AddPropertiesOnComponent(type, component, addedComponent);
                    }
                }
            }
        }

        private void AddPropertiesOnComponent(Type type, JsonComponent component, BlobbyComponent addedComponent)
        {
            // If properties aren't specified assume we don't need them.
            if (component.Properties == null)
            {
                return;
            }
            // For each property try to convert the string in the JSON file to an object that the component can use.
            foreach (string propertyName in component.Properties.Keys)
            {
                PropertyInfo info = type.GetProperty(propertyName);
                object propertyValue = ParseStringToObject(info, component.Properties[propertyName]);
                info.SetValue(addedComponent, propertyValue);
            }
        }

        private object ParseStringToObject(PropertyInfo info, string stringValue) {
			object propertyValue = null;
            // Handle some basic primitive types.
            if (info.PropertyType == typeof(Vector2))
            {
                propertyValue = new Vector2().Parse(stringValue);
            }
            else if (info.PropertyType == typeof(float))
            {
                propertyValue = float.Parse(stringValue);
            }
            else if (info.PropertyType == typeof(int))
            {
                propertyValue = int.Parse(stringValue);
            }
            else if (info.PropertyType == typeof(bool))
            {
                propertyValue = bool.Parse(stringValue);
            }
            else if (info.PropertyType == typeof(string))
            {
                propertyValue = stringValue;
            }
            else
            {
                propertyValue = LookForDeserialiseMethod(info, stringValue);
            }
            return propertyValue;
        }

        private object LookForDeserialiseMethod(PropertyInfo info, string propertyValue) {
            Type propertyType = info.PropertyType;
            MethodInfo foundParseMethod = propertyType.GetMethod("Parse");
			if (IsSuitableParseMethod(foundParseMethod, propertyType))
            {
                object thisobj = null;
                if(!foundParseMethod.IsStatic)
				{
					Console.WriteLine("Non-Static Parse Method, Creating new instance of object.");
                    return thisobj = Activator.CreateInstance(propertyType);
	            }
                object[] args = new object[1];
                args[0] = propertyValue;
                return foundParseMethod.Invoke(thisobj, args);
            }
            return null;
        }

        private bool IsSuitableParseMethod(MethodInfo info, Type targetType) {
            if (info == null)
			{
                string errorMessage = "Unsuitable Parse Method,";
                errorMessage += ("\tWhile trying to deserialise " + targetType.Name);
                errorMessage += ("\tCould not find Method 'Parse' in class " + targetType.FullName);
                throw new SceneLoadException(errorMessage);
			}
            string methodFullName = info.DeclaringType.FullName + "#" + info.Name;
            if (!info.IsPublic)
			{
				string errorMessage = "Unsuitable Parse Method,";
				errorMessage += ("\n\tWhile trying to deserialise " + targetType.Name);
                errorMessage += ("\n\tMethod '" + methodFullName + "' is not public.");
				throw new SceneLoadException(errorMessage);
			}
			if (info.ReturnType != targetType && !info.ReturnType.IsSubclassOf(targetType))
			{
                string errorMessage = "Unsuitable Parse Method,";
				errorMessage += ("\n\tWhile trying to deserialise " + targetType.Name);
                errorMessage += ("\n\tReturn type is not a subclass of '" + targetType.Name + "'.");
                errorMessage += ("\n\tExpected Method Signature 'public static " + targetType.Name + "' Parse(string inputValue).");
				errorMessage += ("\n\tGot: " + MethodInfoToBasicSignature(info));
				throw new SceneLoadException(errorMessage);
            }
			ParameterInfo[] parameters = info.GetParameters();
			if (parameters.Length != 1 || parameters[0].ParameterType != typeof(string))
			{
                string errorMessage = "Unsuitable Parse Method,";
				errorMessage += ("\n\tWhile trying to deserialise " + targetType.Name);
				errorMessage += ("\n\tExpected Method Signature 'public static " + targetType.Name + "' Parse(string inputValue).");
				errorMessage += ("\n\tGot: " + MethodInfoToBasicSignature(info));
				throw new SceneLoadException(errorMessage);
			}
            return true;
        }

        private string MethodInfoToBasicSignature(MethodInfo info) {
            string signature = ("public " + (info.IsStatic ? "static " : "")
                          + info.ReturnType.Name + " "
                    + info.Name + "(");
            ParameterInfo[] parameters = info.GetParameters();
            if (parameters != null && parameters.Length > 0)
            {
                ParameterInfo last = parameters[parameters.Length - 1];
                foreach (ParameterInfo param in parameters)
                {
                    signature += param.ParameterType.Name;
                    if(param != last) {
                        signature += ", ";
                    }
                }

            }
            signature += ")";
            return signature;
        }

        private Assembly GetAssemblyFromName(JsonComponent component) {
			Assembly asm = this.GetType().Assembly;
			if (component.AssemblyName != null)
			{
				AssemblyName an = new AssemblyName(component.AssemblyName);
				asm = Assembly.Load(an);
			}
            return asm;
        }

        private class JsonComponent
        {
            public string AssemblyName { get; set; }
            public string TypeName { get; set; }
            public Dictionary<string, string> Properties { get; set; }
        }

        private class JsonGameObject
        {
            public float[] Position { get; set; }
            public float Angle { get; set; }
            public List<JsonComponent> Components { get; set; }
            public Dictionary<string, JsonGameObject> Children { get; set; }
        }
    }
}