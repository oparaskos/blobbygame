using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Blobby.Core
{
    public interface IBlobbyGame : IUpdateable
    {
        ICameraComponent ActiveCamera { get; set; }
        IContentManager Content { get; }
        IGraphicsDevice GraphicsDevice { get; }
        IInputManager Input { get; }
        GameWindow Window { get; }

        void Exit();
    }
}