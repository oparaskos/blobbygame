﻿namespace Blobby.Core
{
    public interface IContentManager
    {
        T Load<T>(string v);
        string RootDirectory { get; set; }
    }
}