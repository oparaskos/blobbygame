using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Runtime.Serialization;

namespace Blobby.Core
{
    [Serializable]
    public class GameObject : IGameComponent, IDisposable, IUpdateable, ISerializable
    {
        #region Events
        /// <summary>
        /// Invoked when the value of the `Enabled` Property changes
        /// </summary>
        public event EventHandler<EventArgs> EnabledChanged;

        /// <summary>
        /// Invoked when the value of the `UpdateOrder` Property changes
        /// </summary>
        public event EventHandler<EventArgs> UpdateOrderChanged;
        #endregion

        #region Member fields
        private int _enumerationDepth = 0;
        private int EnumerationDepth
        {
            get
            {
                return _enumerationDepth;
            }
            set
            {
                _enumerationDepth = value;
                if (value == 0)
                {
                    AddMissingChildren();
                }
            }
        }

        private bool _initialised = false;
        private bool _enabled = true;
        private int _updateOrder = 0;
        private List<GameObject> _childrenToAdd = new List<GameObject>();
        private List<GameObject> _childrenToRemove = new List<GameObject>();
        private IComparer<IUpdateable> updateableComparer = UpdatableComparer.GetInstance();

        protected IBlobbyGame game = null;
        protected IDictionary<Type, Subsystem> subsystems = null;
        #endregion

        #region Properties
        public string Name { get; set; }
        public IDictionary<Type, BlobbyComponent> ComponentLookup { get; private set; }
        public List<BlobbyComponent> Components { get; private set; }
        public List<GameObject> Children { get; private set; }
        public GameObject Parent { get; private set; }
        public bool IsDisposed { get; private set; } = false;
        public Vector2 RelativePosition { get; set; } = Vector2.Zero;
        public float RelativeRotation { get; set; } = 0f;

        public IDictionary<Type, Subsystem> Subsystems
		{
			get
			{
                if (subsystems == null)
				{
                    subsystems = Parent.Subsystems;
				}
                return subsystems;
			}
        }

        public IBlobbyGame Game
        {
            get
            {
                if (game == null)
                {
                    game = Parent.Game;
                }
                return game;
            }
        }

        /// <summary>
        /// Find the top level parent by recursivley crawling to the top of the tree.
        /// </summary>
        public RootGameObject Root 
        {
            get
            {
                if(this.GetType() == typeof(RootGameObject) || this.GetType().IsInstanceOfType(typeof(RootGameObject))) 
                {
                    return (RootGameObject) this;
                }
                else
                {
                    return Parent.Root;
                }
            }
        }

        public Vector2 AbsolutePosition
        {
            get
            {
                if (Parent != null)
                {
                    var transformedPosition = Vector2.Transform(RelativePosition, Matrix.CreateRotationZ(Parent.AbsoluteRotation));
                    return Vector2.Add(transformedPosition, Parent.AbsolutePosition);
                }
                else
                {
                    return RelativePosition;
                }
            }
        }

        public float AbsoluteRotation
        {
            get
            {
                if (Parent != null)
                {
                    return (Parent.AbsoluteRotation + RelativeRotation);
                }
                else
                {
                    return RelativeRotation;
                }
            }
        }

        public bool Enabled
        {
            get
            {
                if(Parent == null)
                {
                    return _enabled;
                }
                return _enabled && Parent.Enabled;
            }
            set
            {
                if (_enabled != value)
                {
                    _enabled = value;
                    EnabledChanged?.Invoke(this, null);
                }
            }
        }

        public int UpdateOrder
        {
            get
            {
                return _updateOrder;
            }
            set
            {
                if (_updateOrder != value)
                {
                    _updateOrder = value;
                    UpdateOrderChanged?.Invoke(this, null);
                }
            }
        }

        public bool Initialised
        {
            get
            {
                return _initialised;
            }
        }
        #endregion

        /// <summary>
        /// Create a new game object with the given name
        /// </summary>
        /// <param name="name">
        /// The name to give the Game Object 
        /// (defaults to 'Game Object')
        /// </param>
        public GameObject(string name = "Game Object")
        {
            Name = name;
            ComponentLookup = new Dictionary<Type, BlobbyComponent>();
            Components = new List<BlobbyComponent>();
            Children = new List<GameObject>();
		}

        /// <summary>
        /// Initialise this gameobject by initialising its 
        /// components and its children in that order.
        /// </summary>
		public virtual void Initialize()
		{
			InitializeComponents();
			InitializeChildren();
            _initialised = true;
		}

        /// <summary>
        /// Called once per frame
        /// </summary>
        /// <param name="gameTime">
        /// The game time (time since last frame and 
        /// total time the game has been running).
        /// </param>
		public virtual void Update(GameTime gameTime)
		{
            if (!Enabled)
            {
                return;
            }
			UpdateComponents(gameTime);
			UpdateChildren(gameTime);
		}

        /// <summary>
        /// Called once per frame
        /// </summary>
        /// <param name="spriteBatch">
        /// SpriteBatch to draw the results of this frame to.
        /// </param>
		public virtual void Draw(SpriteBatch spriteBatch)
        {
            if (!Enabled)
            {
                return;
            }
            DrawComponents(spriteBatch);
			DrawChildren(spriteBatch);
		}

        /// <summary>
        /// Add a child to the Game object
        /// </summary>
        /// <param name="child">The child to add</param>
        public void AddChild(GameObject child)
        {
            if (child.Parent != null)
            {
                throw new ArgumentException("Cannot add a child to a GameObject without first detaching from parent");
            }
            child.Parent = this;
            if (!IsEnumerating())
            {
                Children.Add(child);
                Children.Sort(updateableComparer);
            }
            else
            {
                _childrenToAdd.Add(child);
            }
        }

        private bool IsEnumerating()
        {
            if(EnumerationDepth < 0)
            {
                throw new InvalidOperationException("enumerating has been freed more times than locked.");
            }
            return EnumerationDepth > 0;
        }

        /// <summary>
        /// Remove a child from the Game object
        /// </summary>
        /// <param name="child">The child to remove</param>
        public void RemoveChild(GameObject child)
        {
            if (!HasChild(child))
            {
                throw new ArgumentException("The child is not a direct descendant of this GameObject");
            }
            child.Parent = this;
            if (!IsEnumerating())
            {
                Children.Remove(child);
            }
            else
            {
                _childrenToRemove.Add(child);
            }
        }

        /// <summary>
        /// Does this child belong to this GameObject
        /// </summary>
        /// <param name="child">The child to look for</param>
        /// <returns>true if the child is a direct descendant of this GameObject</returns>
        private bool HasChild(GameObject child)
        {
            return child.Parent == this && Children.Contains(child);
        }

        /// <summary>
        /// Deconstruct the components so that they
        /// can be removed safely from the scene.
        /// </summary>
        private void DisposeComponents()
		{
            foreach(BlobbyComponent component in ComponentLookup.Values)
            {
                component.Dispose();
            }
        }

        /// <summary>
        /// Dispose all children and components.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
        }

        /// <summary>
        /// Dispose all children and components.
        /// </summary>
        public void Dispose(bool nativeOnly)
        {
            DisposeComponents();
            DisposeAllChildren();
            IsDisposed = true;
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Dispose children so they and their components
        /// can be safely removed from the scene.
        /// </summary>
        public void DisposeAllChildren()
        {
            foreach (GameObject child in Children)
            {
                child.Dispose();
            }
        }

        /// <summary>
        /// Dispose all children and this, remove all children and remove this from the parent.
        /// </summary>
        public void Destroy()
        {
            DisposeAllChildren();
            Dispose();
            Children.Clear();
            Parent.RemoveChild(this);
        }

        public void InitializeChildren()
        {
            ++EnumerationDepth;
            foreach (GameObject child in Children)
            {
                child.Initialize();
            }
            --EnumerationDepth;
        }

        public void InitializeComponents()
        {
            foreach (BlobbyComponent component in Components)
            {
                component.LoadContent();
                component.Initialize();
            }
        }

        /// <summary>
        /// Find all the children which match the given name
        /// </summary>
        /// <param name="name">The name to look for</param>
        /// <returns>An enumerable of all the children with the given name</returns>
        public IEnumerable<GameObject> FindChildrenByName(string name)
        {
            List<GameObject> results = new List<GameObject>();
            if (Name.Equals(name))
            {
                results.Add(this);
            }
            foreach (var child in Children)
            {
                results.AddRange(child.FindChildrenByName(name));
            }
            return results;
        }

        /// <summary>
        /// Once the game is not within a frame finish
        /// by adding components that were added during
        /// the frame and likewise removing those that were removed.
        /// </summary>
        private void AddMissingChildren()
        {
            if(IsEnumerating())
            {
                throw new InvalidOperationException("Cannot add to Children while something is enumerating over the children");
            }
            Children.AddRange(_childrenToAdd);
            _childrenToAdd.Clear();
            foreach (GameObject child in _childrenToRemove)
			{
				Children.Remove(child);
            }
            _childrenToRemove.Clear();
            Children.Sort(updateableComparer);
        }

        public void DrawComponents(SpriteBatch spriteBatch)
        {
            foreach (BlobbyComponent component in Components)
            {
                component.Draw(spriteBatch);
            }
        }

        public void UpdateComponents(GameTime gameTime)
        {
            foreach (BlobbyComponent component in Components)
            {
                component.Update(gameTime);
            }
        }

        private void UpdateChildren(GameTime gameTime)
        {
            ++EnumerationDepth;
            foreach (GameObject child in Children)
            {
                child.Update(gameTime);
            }
            --EnumerationDepth;
        }

        private void DrawChildren(SpriteBatch spriteBatch)
        {
            ++EnumerationDepth;
            foreach (GameObject child in Children)
            {
                child.Draw(spriteBatch);
            }
            --EnumerationDepth;
		}

        #region Internal - Non-Generic methods to add/remove components, children and subsystems
        internal BlobbyComponent GetComponent(Type componentType)
        {
            AssertHasComponent(componentType);
            return ComponentLookup[componentType];
        }

        internal bool HasComponent(Type componentType)
        {
            return ComponentLookup.ContainsKey(componentType);
        }

        internal void AddComponent(BlobbyComponent component)
        {
            AssertComponentUnique(component.GetType());
            ComponentLookup.Add(component.GetType(), component);
            Components.Add(component);
            Components.Sort(updateableComparer);
        }

        internal BlobbyComponent AddComponent(Type componentType)
        {
            AssertComponentUnique(componentType);
            BlobbyComponent component = (BlobbyComponent)Activator.CreateInstance(componentType, true);
            component.Owner = this;
            AddComponent(component);
            return component;
        }

        internal void AssertComponentUnique(Type componentType)
        {
            if (HasComponent(componentType))
            {
                throw new ArgumentException("This GameObject already has a component of type '"
                                + componentType.Name + "'", nameof(componentType));
            }
        }

        internal void AssertHasComponent(Type componentType)
        {
            if (!HasComponent(componentType))
            {
                throw new ArgumentException("The GameObject does not have the required component of type '"
                   + componentType.Name + "'", "componentType");
            }

        }

        internal void RemoveComponent(Type componentType)
        {
            AssertHasComponent(componentType);
            Components.Remove(ComponentLookup[componentType]);
            ComponentLookup.Remove(componentType);
        }

        internal BlobbyComponent FindComponentOnParent(Type t)
        {
            if (HasComponent(t))
            {
                return GetComponent(t);
            }
            return Parent?.FindComponentOnParent(t);
        }

        internal BlobbyComponent FindComponentOnChild(Type t)
        {
            if (HasComponent(t))
            {
                return GetComponent(t);
            }
            else
            {
                ++EnumerationDepth;
                BlobbyComponent component = UnsafeFindComponentOnChild(t);
                --EnumerationDepth;
                return component;
            }
        }

        internal BlobbyComponent UnsafeFindComponentOnChild(Type t)
        {
            foreach (GameObject child in Children)
            {
                BlobbyComponent c = child.FindComponentOnChild(t);
                if (c != null)
                {
                    return c;
                }
            }
            return null;
        }

        internal Subsystem GetSubsystem(Type t)
        {
            return Subsystems[t];
        }

        internal bool HasSubsystem(Type t)
        {
            return Subsystems.ContainsKey(t);
        }
        #endregion

        public override string ToString()
		{
            return base.ToString() + ": " + Name;
		}

        public GameObject(SerializationInfo info, StreamingContext context)
        {
            Name = info.GetString("Name");
            Components = (List<BlobbyComponent>)info.GetValue("Components", Components.GetType());
            Children = (List<GameObject>)info.GetValue("Children", Children.GetType());
            Enabled = info.GetBoolean("Enabled");
            UpdateOrder = (int) info.GetValue("UpdateOrder", UpdateOrder.GetType());
            RelativePosition = (Vector2) info.GetValue("RelativePosition", typeof(Vector2));
            RelativeRotation = info.GetSingle("RelativeRotation");
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("Name", Name);
            info.AddValue("Components", Components);
            info.AddValue("Children", Children);
            info.AddValue("Enabled", Enabled);
            info.AddValue("UpdateOrder", UpdateOrder);
            info.AddValue("RelativePosition", RelativePosition);
            info.AddValue("RelativeRotation", RelativeRotation);
        }
    }
}