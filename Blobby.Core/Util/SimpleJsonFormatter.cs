﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Text;

namespace Blobby.Core.Util
{
    class SimpleJsonFormatter : IFormatter
    {
        public ISurrogateSelector SurrogateSelector { get; set; }
        public SerializationBinder Binder { get; set; }
        public StreamingContext Context { get; set; }

        public object Deserialize(Stream serializationStream)
        {
            throw new NotImplementedException();
        }

        public void Serialize(Stream serializationStream, object graph)
        {
            if (graph is ISerializable)
            {
                ISerializable g = (ISerializable)graph;
                SerializationInfo s = new SerializationInfo(g.GetType(), new FormatterConverter());
                g.GetObjectData(s, new StreamingContext());
                writeToStream(serializationStream, "{");
                bool hasWrittenMembers = false;
                var enumerator = s.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    try
                    {
                        if (hasWrittenMembers)
                        {
                            writeToStream(serializationStream, ",");
                        }
                        writeToStream(serializationStream, "\"" + enumerator.Current.Name + "\":");
                        Serialize(serializationStream, enumerator.Current.Value);
                        hasWrittenMembers = true;
                    }
                    catch (SerializationException e)
                    {
                        throw new SerializationException("Error serializing field " + graph.GetType() + "." + enumerator.Current.Name, e);
                    }
                }
                writeToStream(serializationStream, "}");
            }
            else if (graph is IEnumerable<object>)
            {
                var i = 0;
                writeToStream(serializationStream, "[");
                foreach (object o in (IEnumerable<object>)graph)
                {
                    if(i > 0)
                    {
                        writeToStream(serializationStream, ",");
                    }
                    Serialize(serializationStream, o);
                    ++i;
                }
                writeToStream(serializationStream, "]");
            }
            else
            {
                writeToStream(serializationStream, "\"" + graph.ToString() + "\"");
            }
        }

        void writeToStream(Stream s, string content)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(content);
            s.Write(bytes, 0, bytes.Length);
        }
    }
}
