using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blobby.Core
{
    public static class NumberHelper
    {
        public static double Clamp(this double value, double min, double max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }

        public static float Clamp(this float value, float min, float max)
        {
            return (value < min) ? min : (value > max) ? max : value;
        }
        public static double Clamp(this double value, double max)
        {
            return (value > max) ? max : value;
        }

        public static float Clamp(this float value, float max)
        {
            return (value > max) ? max : value;
        }
    }
}