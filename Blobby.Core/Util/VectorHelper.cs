using System;
using Microsoft.Xna.Framework;
using System.Text.RegularExpressions;

namespace Blobby.Core
{
	public static class VectorHelper
	{
		private static Regex rgx = new Regex("[^\\d, ]+");
		private static Regex splitRgx = new Regex("[, \n]+");

		public static double Angle(this Vector2 lhs, Vector2 rhs)
		{
			return Math.Atan2(rhs.Y, rhs.X) - Math.Atan2(lhs.Y, lhs.X);
		}

		public static Point Parse(this Point lhs, string stringValue)
		{
			string[] parts = VectorParts(stringValue);
			lhs.X = int.Parse(parts[0]);
			lhs.Y = int.Parse(parts[1]);
			return lhs;
		}

		public static Vector2 Parse(this Vector2 lhs, string stringValue)
		{
			string[] parts = VectorParts(stringValue);
			lhs.X = float.Parse(parts[0]);
			lhs.Y = float.Parse(parts[1]);
			return lhs;
		}

		public static Vector3 Parse(this Vector3 lhs, string stringValue)
		{
			string[] parts = VectorParts(stringValue);
			lhs.X = float.Parse(parts[0]);
			lhs.Y = float.Parse(parts[1]);
			lhs.Z = float.Parse(parts[2]);
			return lhs;
		}

		public static Vector4 Parse(this Vector4 lhs, string stringValue)
		{
			string[] parts = VectorParts(stringValue);
			lhs.X = float.Parse(parts[0]);
			lhs.Y = float.Parse(parts[1]);
			lhs.Z = float.Parse(parts[2]);
			lhs.W = float.Parse(parts[2]);
			return lhs;
		}

		public static Quaternion Parse(this Quaternion lhs, string stringValue)
		{
			string[] parts = VectorParts(stringValue);
			lhs.X = float.Parse(parts[0]);
			lhs.Y = float.Parse(parts[1]);
			lhs.Z = float.Parse(parts[2]);
			lhs.W = float.Parse(parts[2]);
			return lhs;
		}

		public static Color Parse(this Color lhs, string stringValue)
		{
			string[] parts = VectorParts(stringValue);
			lhs.R = byte.Parse(parts[0]);
			lhs.G = byte.Parse(parts[1]);
            lhs.B = byte.Parse(parts[2]);
			if (parts.Length > 3)
			{
				lhs.A = byte.Parse(parts[3]);
			}
			return lhs;
			
		}

		public static Matrix Parse(this Matrix lhs, string stringValue)
		{
			string[] parts = VectorParts(stringValue);
			lhs.M11 = float.Parse(parts[0]);
			lhs.M12 = float.Parse(parts[1]);
			lhs.M13 = float.Parse(parts[2]);
			lhs.M14 = float.Parse(parts[3]);
			lhs.M21 = float.Parse(parts[4]);
			lhs.M22 = float.Parse(parts[5]);
			lhs.M23 = float.Parse(parts[6]);
			lhs.M24 = float.Parse(parts[7]);
			lhs.M31 = float.Parse(parts[8]);
			lhs.M32 = float.Parse(parts[9]);			lhs.M33 = float.Parse(parts[10]);
			lhs.M34 = float.Parse(parts[11]);
			lhs.M41 = float.Parse(parts[12]);
			lhs.M42 = float.Parse(parts[13]);			lhs.M43 = float.Parse(parts[14]);
			lhs.M44 = float.Parse(parts[15]);
			return lhs;
		}

		private static string[] VectorParts(string stringValue)
		{
			string s = rgx.Replace(stringValue, "");
			return splitRgx.Split(s);
		}
    }
}