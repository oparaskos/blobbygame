﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blobby.Core
{
    public static class GameObjectUtil
    {
        #region Find subsystems using generics
        public static T GetSubsystem<T>(this GameObject g) where T : Subsystem
        {
            return (T)g.GetSubsystem(typeof(T));
        }

        public static bool HasSubsystem<T>(this GameObject g) where T : Subsystem
        {
            return g.HasSubsystem(typeof(T));
        }
        #endregion

        #region Find components using generics
        public static T GetComponent<T>(this GameObject g) where T : BlobbyComponent
        {
            return (T)g.GetComponent(typeof(T));
        }

        public static bool HasComponent<T>(this GameObject g) where T : BlobbyComponent
        {
            return g.HasComponent(typeof(T));
        }

        public static T AddComponent<T>(this GameObject g) where T : BlobbyComponent
        {
            return (T)g.AddComponent(typeof(T));
        }

        public static void RemoveComponent<T>(this GameObject g) where T : BlobbyComponent
        {
            g.RemoveComponent(typeof(T));
        }

        public static T FindComponentOnParent<T>(this GameObject g) where T : BlobbyComponent
        {
            return (T)g.FindComponentOnParent(typeof(T));
        }

        /// <summary>
        /// Return the first instance of this component on a child GameObject
        /// </summary>
        /// <typeparam name="T">The type of component to look for</typeparam>
        /// <param name="g">the GameObject to start the search from</param>
        /// <returns>the first instance</returns>
        public static T FindComponentOnChild<T>(this GameObject g) where T : BlobbyComponent
        {
            return (T)g.FindComponentOnChild(typeof(T));
        } 
        #endregion
    }
}
