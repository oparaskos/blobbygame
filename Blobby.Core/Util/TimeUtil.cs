﻿using System;
namespace Blobby.Core.MathUtil
{
    public static class TimeUtil
    {
		public static double GetMillis()
		{
			DateTime Jan1970 = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			TimeSpan s = (DateTime.UtcNow - Jan1970);
			return s.TotalMilliseconds;
		}
    }
}
