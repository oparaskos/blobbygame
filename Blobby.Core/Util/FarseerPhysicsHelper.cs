﻿using System;
using FarseerPhysics.Collision;
using Microsoft.Xna.Framework;

namespace Blobby.Core.MathUtil
{
    public static class FarseerPhysicsHelper
    {
        public static Vector2 GetTopLeft(this Rectangle lhs)
        {
            return new Vector2(lhs.X, lhs.Y);
		}

		public static AABB ToAABB(this Rectangle lhs)
		{
			return new AABB(lhs.GetTopLeft(), lhs.Width, lhs.Height);
		}

        public static Rectangle ToRectangle(this AABB lhs)
		{
            return new Rectangle((int) lhs.LowerBound.X, (int)lhs.LowerBound.Y, (int)lhs.Width, (int)lhs.Height);
		}
    }
}
