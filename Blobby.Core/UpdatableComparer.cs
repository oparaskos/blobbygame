﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Blobby.Core
{
    internal class UpdatableComparer: IComparer<IUpdateable>
    {
        internal static UpdatableComparer instance = null;

        public int Compare(IUpdateable x, IUpdateable y)
        {
            return x.UpdateOrder - y.UpdateOrder;
        }

        /// <summary>
        /// Return a single instance rather than have lots of these doing nothing much else.
        /// </summary>
        /// <returns>the singleton updatableComparer instance</returns>
        internal static UpdatableComparer GetInstance()
        {
            if(instance == null)
            {
                instance = new UpdatableComparer();
            }
            return instance;
        }
    }
}