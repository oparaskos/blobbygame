﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blobby.Core
{
    public interface IGraphicsDevice
    {
        Viewport Viewport { get; }

        void Clear(Color black);
        Texture2D CreateTexture2D(int v1, int v2);
    }
}