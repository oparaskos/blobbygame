using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blobby.Core.Components
{
    public class UIPanelComponent : BlobbyComponent
    {
        public bool Visible { get; set; } = true;

        public Rectangle Bounds { get; set; } = new Rectangle(-150, -75, 300, 150);
        public ResizableRectangleSprite ResizableRectangleSprite = new ResizableRectangleSprite();
        private float layer = 0.05f;
        public float Layer
        {
            get
            {
                return (layer * 200) - 100;
            }
            set
            {
                layer = ((-value + 100) / 200);
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Visible)
            {
                if (ResizableRectangleSprite.BottomBorder == null)
                {
                    LoadTextures();
                }
                ResizableRectangleSprite.Position = Owner.AbsolutePosition;
                ResizableRectangleSprite.Angle = Owner.AbsoluteRotation;
                ResizableRectangleSprite.Bounds = Bounds;
                ResizableRectangleSprite.Layer = layer;
                ResizableRectangleSprite.Draw(spriteBatch);
            }
        }
        private void LoadTextures()
        {
            ResizableRectangleSprite.LeftBorder = Game.Content.Load<Texture2D>("UI/Panel/Border_Left");
            ResizableRectangleSprite.RightBorder = Game.Content.Load<Texture2D>("UI/Panel/Border_Right");
            ResizableRectangleSprite.TopBorder = Game.Content.Load<Texture2D>("UI/Panel/Border_Top");
            ResizableRectangleSprite.BottomBorder = Game.Content.Load<Texture2D>("UI/Panel/Border_Bottom");
            ResizableRectangleSprite.CornerTopLeft = Game.Content.Load<Texture2D>("UI/Panel/Corner_Top_Left");
            ResizableRectangleSprite.CornerTopRight = Game.Content.Load<Texture2D>("UI/Panel/Corner_Top_Right");
            ResizableRectangleSprite.CornerBottomLeft = Game.Content.Load<Texture2D>("UI/Panel/Corner_Bottom_Left");
            ResizableRectangleSprite.CornerBottomRight = Game.Content.Load<Texture2D>("UI/Panel/Corner_Bottom_Right");
            ResizableRectangleSprite.BackgroundTexture = Game.Content.Load<Texture2D>("UI/Panel/Background");
        }
    }
}