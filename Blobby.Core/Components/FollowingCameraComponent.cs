using Microsoft.Xna.Framework;
using Blobby.Core.MathUtil;
using System;

namespace Blobby.Core.Components
{
    public class FollowingCameraComponent : BlobbyComponent
    {
		private bool _focusing = false;
		private double _zoomChangeStart = 0;
		private double _zoomChangeDuration = 2000;
		private float _zoomFrom = 1;
		private float _zoomTo = 1;

		public GameObject Target { get; set; }

        /// <summary>
        /// Maximum rotation speec of the camera in Radians/Second
        /// </summary>
        public float RotationSpeed { get; set; } = 5f;

        public bool Focusing
        { 
            get
            {
                return _focusing;
            }
            set
            {
                _zoomChangeStart = TimeUtil.GetMillis();
                _focusing = value;
                if (_focusing)
                {
                    _zoomFrom = Owner.GetComponent<CameraComponent>().Zoom;
                    _zoomTo = 2f;
				}
				else
				{
					_zoomFrom = Owner.GetComponent<CameraComponent>().Zoom;
					_zoomTo = 1f;
                }
            }
        }

        public override void Update(GameTime gameTime) {
            Owner.RelativePosition = Target.AbsolutePosition;
            float difference = (Target.AbsoluteRotation +3.14f - Owner.AbsoluteRotation) % 6.28f - 3.14f;
            Owner.RelativeRotation = Owner.RelativeRotation 
                + Microsoft.Xna.Framework.MathHelper.Clamp(
                    (float) difference * RotationSpeed * (float) gameTime.ElapsedGameTime.TotalSeconds,
                    -Math.Abs(difference),
                    Math.Abs(difference));

            double zoomChangeElapsed = TimeUtil.GetMillis() - _zoomChangeStart;
            float zoom = Blobby.Core.MathUtil.MathHelper.Lerp(
                _zoomFrom, _zoomTo,
                Microsoft.Xna.Framework.MathHelper.Clamp((float)((double)zoomChangeElapsed / (double)_zoomChangeDuration), 0, 1));
            Owner.GetComponent<CameraComponent>().Zoom = zoom;
        }

    }
}