﻿using System;
using Microsoft.Xna.Framework;
using System.Runtime.Serialization;
using Blobby.Core.Util;
using System.IO;

namespace Blobby.Core.Components
{
    // Dump the world to a JSON file when the SNAPSHOT key is pressed.
    public class ObjectDumpComponent : BlobbyComponent
    {
        private bool lastFrameSnapshot = false;

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (Game.Input.GetButton("SNAPSHOT"))
            {
                if (!lastFrameSnapshot)
                {
                    lastFrameSnapshot = true;
                    DumpObjectToFile();
                }
            }
            else
            {
                lastFrameSnapshot = false;
            }
        }

        public void DumpObjectToFile()
        {
            IFormatter formatter = new SimpleJsonFormatter();
            using (FileStream outputStream = File.Create(Owner.Name + "-dump.json"))
            {
                try
                {
                    formatter.Serialize(outputStream, Owner);
                }
                catch (SerializationException ex)
                {
                    Console.Error.WriteLine(ex.Message);
                    Console.Error.WriteLine(ex.StackTrace);
                }
            }
        }
    }
}
