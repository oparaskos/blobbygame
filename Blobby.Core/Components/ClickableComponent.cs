﻿using System;
using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Blobby.Core.Components
{
    public class ClickableComponent : BlobbyComponent
	{
		public delegate void OnClickEventHandler(object sender, EventArgs e);
		public event OnClickEventHandler OnClick;

		public bool IsPressed { get; set; }
        public string Cursor { get; set; } = "UI\\Pointer_Active";

        public ClickableComponent() : base()
        {
        }

		public override void Update(GameTime gameTime)
		{
			base.Update(gameTime);
			if (IsPressed)
			{
				if (Mouse.GetState().LeftButton == ButtonState.Released)
				{
					IsPressed = false;
				}
			}
		}

        public void DoClick()
        {
            IsPressed = true;
            OnClick?.Invoke(this, null);
        }
    }
}
