using Blobby.Core;
using Microsoft.Xna.Framework;

namespace Blobby.Core.Components
{
    public class AutoScrollingBackgroundComponent : BlobbyComponent
    {
        public AutoScrollingBackgroundComponent() : base()
        {

        }
        public override void Update(GameTime gameTime)
        {
            Sprite s = Owner.GetComponent<SpriteRendererComponent>().ActiveSprite;
            CameraComponent c = (CameraComponent)Game.ActiveCamera;
            s.SourceRectangle = new Rectangle(
                (int)(gameTime.TotalGameTime.TotalMilliseconds * 0.005f),
                (int)(gameTime.TotalGameTime.TotalMilliseconds * 0.01f), 1280, 1280);
        }
    }
}