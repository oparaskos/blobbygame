using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blobby.Core.Components
{
    public class LineRendererComponent : BlobbyComponent
    {
        public bool Visible { get; set; } = true;

        public int Width { get; set; }
        public ResizableLineSprite ResizableLineSprite = new ResizableLineSprite();
        
        private float layer = 0.05f;
        public float Layer
        {
            get
            {
                return (layer * 200) - 100;
            }
            set
            {
                layer = ((-value + 100) / 200);
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Visible)
            {
                if (ResizableLineSprite.LeftEdge == null)
                {
                    LoadTextures();
                }
                ResizableLineSprite.Position = Owner.AbsolutePosition;
                ResizableLineSprite.Angle = Owner.AbsoluteRotation;
                ResizableLineSprite.Width = Width;
                ResizableLineSprite.Layer = layer;
                ResizableLineSprite.Draw(spriteBatch);
            }
        }
        private void LoadTextures()
        {
            ResizableLineSprite.LeftEdge = Game.Content.Load<Texture2D>("UI/Button/ProgressBar_Background_Left");
            ResizableLineSprite.RightEdge = Game.Content.Load<Texture2D>("UI/Button/ProgressBar_Background_Right");
            ResizableLineSprite.BackgroundTexture = Game.Content.Load<Texture2D>("UI/Button/ProgressBar_Background_Mid");
        }
    }
}