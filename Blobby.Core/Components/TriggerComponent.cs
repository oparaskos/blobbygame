using Blobby.Core.Physics;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Factories;

namespace Blobby.Core.Components
{
    public class TriggerComponent : PhysicsBodyComponent
	{
		// TODO: Probably there are bugs when there is more than one fixture
		public OnCollisionEventHandler OnCollision;
		public OnSeparationEventHandler OnSeparation;

        public override void Initialize()
		{
            Body = BodyFactory.CreateBody(
				PhysicsSubsystem.World, Owner.AbsolutePosition,
                Owner.AbsoluteRotation, BodyType.Static, this);
            Body.IsSensor = true;
		}

        public override void RegisterCollider(ColliderComponent collider)
		{
            base.RegisterCollider(collider);
			collider.Fixture.IsSensor = true;
			collider.Fixture.OnCollision += this.DoCollision;
            collider.Fixture.OnSeparation += this.DoSeparation;
		}

		public bool DoCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
		{
            this.OnCollision?.Invoke(fixtureA, fixtureB, contact);
			return true;
		}

		public void DoSeparation(Fixture fixtureA, Fixture fixtureB)
		{
            this.OnSeparation?.Invoke(fixtureA, fixtureB);
		}
    }
}