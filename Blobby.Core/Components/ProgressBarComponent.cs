using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blobby.Core.Components
{
    public class ProgressBarComponent : BlobbyComponent
    {
        public bool Visible { get; set; } = true;
        public Color Color { get; set; } = Color.White;

        private Rectangle _maxBounds;
        public Rectangle MaxBounds
        {
            get
            {
                return _maxBounds;
            }
            set
            {
                _maxBounds = value;
                RecalculateBounds();
            }
        }
        private Rectangle Bounds { get; set; } = new Rectangle(-150, -75, 300, 150);
        private float _progress = 1.0f;
        public float Progress
        {
            get
            {
                return _progress;
            }
            set
            {
                _progress = value;
                RecalculateBounds();
            }
        }

        public IResizableRectangleSprite ResizableRectangleSprite = new ResizableRectangleSprite();
        private float layer = 0.1f;
        public float Layer
        {
            get
            {
                return (layer * 200) - 100;
            }
            set
            {
                layer = ((-value + 100) / 200);
            }
        }

        public ProgressBarComponent() : base()
        {
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Visible)
            {
                if (ResizableRectangleSprite == null || !ResizableRectangleSprite.IsLoaded)
                {
                    LoadDefault();
                }
                ResizableRectangleSprite.Position = Owner.AbsolutePosition;
                ResizableRectangleSprite.Angle = Owner.AbsoluteRotation;
                ResizableRectangleSprite.Bounds = Bounds;
                ResizableRectangleSprite.Layer = layer;
                ResizableRectangleSprite.Color = Color;
                ResizableRectangleSprite.Draw(spriteBatch);
            }
        }

        private void RecalculateBounds()
        {
            float targetWidth = ((float)_maxBounds.Width) * _progress;
            targetWidth = MathHelper.Max(ResizableRectangleSprite.MinWidth, targetWidth);
            Bounds = new Rectangle(_maxBounds.X, _maxBounds.Y, (int)targetWidth, _maxBounds.Height);
        }

        private void LoadDefault()
        {
            var s = new ResizableRectangleSprite();
            s.LeftBorder = Game.Content.Load<Texture2D>("UI/Progress_Bar/Left_Border");
            s.RightBorder = Game.Content.Load<Texture2D>("UI/Progress_Bar/Right_Border");
            s.TopBorder = Game.Content.Load<Texture2D>("UI/Progress_Bar/Top_Border");
            s.BottomBorder = Game.Content.Load<Texture2D>("UI/Progress_Bar/Bottom_Border");
            s.CornerTopLeft = Game.Content.Load<Texture2D>("UI/Progress_Bar/Top_Left");
            s.CornerTopRight = Game.Content.Load<Texture2D>("UI/Progress_Bar/Top_Right");
            s.CornerBottomLeft = Game.Content.Load<Texture2D>("UI/Progress_Bar/Bottom_Left");
            s.CornerBottomRight = Game.Content.Load<Texture2D>("UI/Progress_Bar/Bottom_Right");
            s.BackgroundTexture = Game.Content.Load<Texture2D>("UI/Progress_Bar/Background");
            ResizableRectangleSprite = s;
        }
    }
}