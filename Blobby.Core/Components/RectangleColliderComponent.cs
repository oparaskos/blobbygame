﻿using Microsoft.Xna.Framework;
using Blobby.Core.Physics;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;

﻿namespace Blobby.Core.Components
{
    public class RectangleColliderComponent : ColliderComponent
    {
        private Rectangle startingRect = new Rectangle(-100, -100, 100, 100);
        public Rectangle Rect
        {
            get
            {
				return startingRect;
            }
            set
			{
				startingRect = value;
				if (CollisionShape != null)
                {
                    CollisionShape = RectangleToCollisionShape(startingRect);
                }
            }
        }

        public RectangleColliderComponent() 
        {
            CollisionShape = RectangleToCollisionShape(startingRect);
        }

        public Shape RectangleToCollisionShape(Rectangle rect) {
            Vertices verts = new Vertices(4);
			verts.Add(new Vector2(rect.Left, rect.Top));
			verts.Add(new Vector2(rect.Right, rect.Top));
            verts.Add(new Vector2(rect.Right, rect.Bottom));
			verts.Add(new Vector2(rect.Left, rect.Bottom));
            return new PolygonShape(verts, 1.0f);
        }
    }
}