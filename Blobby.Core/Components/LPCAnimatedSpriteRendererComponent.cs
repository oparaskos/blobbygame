﻿using System;
using System.Collections.Generic;
using Blobby.Core;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Blobby.Core.Components
{
    public class LPCAnimatedSpriteRendererComponent : BlobbyComponent
    {
        private Texture2D _spritesheet { get; set; }

        private ActionSpritesheetComponent actionsheet;
		public bool Visible { get; set; } = true;
		public long StartTime { get; set; } = 0;
		public string CurrentAction { get; set; } = SpritesheetAction.DEFAULT_ACTION_NAME;
		public float Scale { get; set; } = 1.0f;
		public float Layer { get; set; } = 0.9f;
        public int Frame { get; private set; }

		public override void Initialize()
		{
			StartTime = Environment.TickCount;
            actionsheet = Owner.GetComponent<ActionSpritesheetComponent>();
            _spritesheet = Game.Content.Load<Texture2D>(actionsheet.Spritesheet);
        }

		public override void Update(GameTime gameTime)
		{
			Frame = actionsheet.Actions[CurrentAction].GetFrame(StartTime);
		}

		public override void Draw(SpriteBatch spriteBatch)
		{
			if (Visible)
			{
                SpritesheetAction spritesheetAction = actionsheet.Actions[CurrentAction];
                Rectangle sourceRectangle = spritesheetAction.GetFrameBounds(Frame);
                Vector2 origin = new Vector2(spritesheetAction.FrameWidth / 2, spritesheetAction.FrameHeight / 2);
                spriteBatch.Draw(_spritesheet, Owner.AbsolutePosition, sourceRectangle, Color.White, Owner.AbsoluteRotation, origin, Scale, SpriteEffects.None, Layer);
			}
		}
    }
}
