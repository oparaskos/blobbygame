﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blobby.Core.Components
{
    public class ColouredRectangleComponent : BlobbyComponent
    {
        public Rectangle Bounds { get; set; } = new Rectangle(-50, -50, 50, 50);
        public Color Colour = Color.Orange;
        public int Layer { get; set; } = 10;
        private Texture2D ColouredRect;

        public override void Initialize()
        {
            ColouredRect = Game.GraphicsDevice.CreateTexture2D(10, 10);
            Color[] colour = new Color[100];
            for (int i = 0; i < 100; ++i)
                colour[i] = Colour;
            ColouredRect.SetData(colour);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            Vector2 BoundsLocation = Owner.AbsolutePosition + Bounds.Location.ToVector2();
            Rectangle renderDest = new Rectangle(
                (int) BoundsLocation.X,
                (int) BoundsLocation.Y,
                Bounds.Width,
                Bounds.Height);
            spriteBatch.Draw(ColouredRect, 
                             renderDest, 
                             renderDest, 
                             Color.White);
        }
    }
}
