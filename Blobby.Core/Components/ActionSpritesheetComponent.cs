﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using Blobby.Core;

namespace Blobby.Core.Components
{
    public class ActionSpritesheetComponent : BlobbyComponent
    {
        public string Spritesheet { get; set; }
        public Dictionary<string, SpritesheetAction> Actions = new Dictionary<string, SpritesheetAction>();
        
        public void DefineAction(string actionName, int numFrames, int duration, int rowNumber)
        {
            SpritesheetAction action = new SpritesheetAction();
            action.Duration = duration;
            action.RowNumber = rowNumber;
            action.NumFrames = numFrames;
            Actions.Add(actionName, action);
        }

    }
}