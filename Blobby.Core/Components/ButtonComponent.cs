using Blobby.Core.Physics;
using Microsoft.Xna.Framework;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework.Input;
using FarseerPhysics.Factories;

namespace Blobby.Core.Components
{
    public class ButtonComponent : PhysicsBodyComponent
	{
        public override void Initialize()
		{
            Body = BodyFactory.CreateBody(
				PhysicsSubsystem.World, Owner.AbsolutePosition,
                Owner.AbsoluteRotation, BodyType.Static, this);
            Body.IsSensor = true;
            Body.CollidesWith = Category.None;
		}

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void RegisterCollider(ColliderComponent collider)
		{
            base.RegisterCollider(collider);
            collider.Fixture.CollidesWith = Category.None;
			collider.Fixture.IsSensor = true;
		}
    }
}