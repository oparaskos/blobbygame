﻿using Microsoft.Xna.Framework;
using Blobby.Core.Physics;
using FarseerPhysics.Collision.Shapes;

﻿namespace Blobby.Core.Components
{
    public class CircleColliderComponent : ColliderComponent
    {
        private float startingRadius = 10.0f;
        public float Radius
        {
            get
            {
                if (CollisionShape == null)
                {
                    return startingRadius;
                }
                return ((CircleShape)CollisionShape).Radius;
            }
            set
			{
                if (CollisionShape == null)
                {
                    startingRadius = value;
                }
                else
                {
                    ((CircleShape)CollisionShape).Radius = value;
                }
            }
        }

        public CircleColliderComponent() 
        {
            CollisionShape = new CircleShape(startingRadius, 1);
        }
    }
}