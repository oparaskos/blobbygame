using Microsoft.Xna.Framework;
using Blobby.Core.Physics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using System;

namespace Blobby.Core.Components
{
    public class RigidBodyComponent : PhysicsBodyComponent
    {
		private Vector2 _startingVelocity = Vector2.Zero;
        public Vector2 Velocity
        {
            get
            {
                return Body?.LinearVelocity ?? _startingVelocity;
            }
            set
            {
                if (Body == null)
                {
                    _startingVelocity = value;
                }
                else
                {
                    Body.LinearVelocity = value;
                }
            }
        }
        private float _startingMass = 0.05f;
		public float Mass
        {
            get
            {
                return Body?.Mass ?? _startingMass;
            }
            set
			{
				if (Body == null)
				{
					_startingMass = value;
				}
				else
				{
					Body.Mass = value;
				}
            }
        }

		private float _startingDamping = .001f;
		public float Damping
		{
			get
			{
				return Body?.LinearDamping ?? _startingDamping;
			}
			set
			{
				if (Body == null)
				{
					_startingDamping = value;
				}
				else
				{
                    Body.LinearDamping = value;
				}
			}
		}

        private bool _startingStatic;
        public bool Static { 
            get
            {
                return Body?.IsStatic ?? _startingStatic;
            }
            set
            {
                if(Body != null)
                {
                    throw new InvalidOperationException("Cannot set a rigid body to static after initialisation");
                }
                _startingStatic = value;
            }
        }

		public override void Initialize()
		{
            Body = BodyFactory.CreateBody(
				PhysicsSubsystem.World, Owner.AbsolutePosition,
				Owner.AbsoluteRotation, BodyType.Dynamic, this);
            Body.Mass = _startingMass;
            Body.LinearVelocity = _startingVelocity;
            Body.LinearDamping = _startingDamping;
            Body.IsStatic = _startingStatic;
		}

		public override void Update(GameTime gameTime)
		{
			Owner.RelativePosition = Body.Position;
		}

        public void Impulse(Vector2 impulse)
        {
            if (Body == null)
            {
                _startingVelocity += impulse;
            }
            else
            {
                Body.ApplyLinearImpulse(impulse);
            }
        }

        public void AngularImpulse(float impulse)
        {
            Body.ApplyAngularImpulse(impulse);
        }

        public bool IsCollidingWith(RigidBodyComponent targetBody)
        {
            return Body?.ContactList != null;
        }
    }
}