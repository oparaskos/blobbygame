using System.Collections.Generic;
using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blobby.Core.Components
{
    public class SpriteRendererComponent : BlobbyComponent
    {
        private Dictionary<string, Sprite> sprites
            = new Dictionary<string, Sprite>();
        private string texture;
        private bool initialized = false;

		public float Layer { get; set; } = 50;
		public float Scale { get; set; } = 1;
        public Color Colour { get; set; } = Color.White;
		public bool Visible { get; set; } = true;
        public float Alpha { get; set; } = 1f;
		
        public Sprite ActiveSprite 
        {
            get
            {
                return GetSprite(Texture);
            }
        }

        public string Texture 
        { 
            get
            { 
                return texture; 
            } 
            set
            {
                texture = value;
                if (initialized)
                {
                    GetSprite(texture);
                }
            }
        }

        public override void Initialize()
        {
            initialized = true;
            GetSprite(texture);
        }

        private Sprite GetSprite(string t) 
        {
            if(!sprites.ContainsKey(t)) {
                sprites.Add(t, new Sprite(Owner.Game.Content.Load<Texture2D>(t)));
			}
			sprites[t].Layer = Layer;
			sprites[t].Scale = Scale;
            sprites[t].Colour = Colour;
            sprites[t].Alpha = Alpha;
            return sprites[t];
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Visible)
            {
                ActiveSprite.Draw(spriteBatch, Owner.AbsolutePosition, Owner.AbsoluteRotation);
            }
        }
    }
}