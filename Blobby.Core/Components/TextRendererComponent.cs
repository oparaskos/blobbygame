using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blobby.Core.Components
{
    public class TextRendererComponent : BlobbyComponent
    {
        public string Text { get; set; } = "Hello World!";
        public bool Visible { get; set; } = true;
        public Vector2 Offset { get; set; } = Vector2.Zero;
        public float Scale { get; set; } = 1.0f;
		private float layer = 0.0f;
		public Alignment HorizontalAlignment;
		public VerticalAlignment VerticalAlignment;
        public SpriteFont Font { get; set; }
        public float Width
        {
            get
            {
                return Font.MeasureString(Text).X * Scale;
            }
        }
        public float Height
        {
            get
            {
                return Font.MeasureString(Text).Y * Scale;
            }
        }

        public float Layer
        {
            get
            {
                return (layer * 200) - 100;
            }
            set
            {
                layer = ((-value + 100) / 200);
            }
        }

        public override void Initialize()
        {
            Font = Game.Content.Load<SpriteFont>("Fonts/Pixelated_spritefont");
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Visible)
            {
                Vector2 offset = OffsetWithAlignment();
                spriteBatch.DrawString(Font, Text, Owner.AbsolutePosition, Color.White, Owner.AbsoluteRotation, -offset, Scale, SpriteEffects.None, layer);
            }
        }

        private Vector2 OffsetWithAlignment()
		{
			float offsetX = Offset.X;

            if (HorizontalAlignment == Alignment.Center)
            {
                offsetX -= (Width / 2);
            }
            else if (HorizontalAlignment == Alignment.Right)
            {
                offsetX -= Width;
            }

			float offsetY = Offset.Y;
            if (VerticalAlignment == VerticalAlignment.Center)
            {
                offsetY -= (Height / 2);
            }
            else if (VerticalAlignment == VerticalAlignment.Bottom)
            {
                offsetX -= Width;
            }


            return new Vector2(offsetX, offsetY) / Scale;
            
        }
    }

	public enum Alignment
	{
		Left,
		Right,
		Center
	}

	public enum VerticalAlignment
	{
		Top,
		Bottom,
		Center
	}
}