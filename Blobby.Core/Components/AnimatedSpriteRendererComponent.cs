using System;
using System.Collections.Generic;

using Blobby.Core;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Blobby.Core.Components
{
    public class AnimatedSpriteRendererComponent : BlobbyComponent
    {
        public Spritesheet Spritesheet { get; set; }
        public bool Visible { get; set; } = true;
        public long StartTime { get; set; } = 0;
        public long AnimationDuration { get; set; } = 5000;
        public int CurrentFrame
        {
            get
            {
                return Spritesheet.Frame;
            }
            set
            {
                Spritesheet.Frame = value;
            }
        }

        public bool PastEnd
        {
            get
            {
                return Spritesheet.PastEnd;
            }
        }

        public AnimatedSpriteRendererComponent() : base()
        {
        }

        public override void Initialize()
        {
            StartTime = Environment.TickCount;
        }

        public override void Update(GameTime gameTime)
        {
            long now = Environment.TickCount;
            long elapsed = now - StartTime;
            long timePerFrame = AnimationDuration / Spritesheet.NumFrames;
            Spritesheet.Frame = (int) (elapsed / timePerFrame);
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            if (Visible)
            {
                Spritesheet.Draw(spriteBatch, Owner.AbsolutePosition, Owner.AbsoluteRotation);
            }
        }
    }
}