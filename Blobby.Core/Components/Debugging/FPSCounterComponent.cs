﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Blobby.Core.Components.Debugging
{
    public class FPSCounterComponent : BlobbyComponent
    {
        const int NUM_FRAMES_TO_AVERAGE_OVER = 5;
        int numFramesCounted = 0;
        double[] secondsPerLastFrames = new double[NUM_FRAMES_TO_AVERAGE_OVER];
        int currentIndex = 0;

        public override void Update(GameTime gameTime)
        {
            // Update the array of values to average over.
            double secondsPerFrame = gameTime.ElapsedGameTime.TotalSeconds;
            secondsPerLastFrames[currentIndex] = secondsPerFrame;
            numFramesCounted = Math.Min(NUM_FRAMES_TO_AVERAGE_OVER, numFramesCounted + 1);
            // Average frames per second from seconds per frame
            double avg =
                secondsPerLastFrames
                .Where((s) => s > 0)
                .Select((s) => 1 / s)
                .Aggregate(0d, (x, y) => x + y) / numFramesCounted;
            Owner.GetComponent<TextRendererComponent>()
                .Text = string.Format("~{0} fps", avg);
            // Remember to update the next index next time around.
            currentIndex = (currentIndex + 1) % NUM_FRAMES_TO_AVERAGE_OVER;
        }
    }
}
