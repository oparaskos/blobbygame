﻿using Blobby.Core.Physics;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Dynamics;
using System;
using System.Linq;
using System.Collections.Generic;

﻿namespace Blobby.Core.Components
{
    public abstract class ColliderComponent : BlobbyComponent
	{
        private PhysicsBodyComponent physicsBodyComponent = null;

		public Fixture Fixture
		{
			get;
			set;
		}

        public Shape CollisionShape
		{
			get;
			set;
		}

        public PhysicsSubsystem PhysicsSubsystem
		{
			get
			{
				return Owner.GetSubsystem<PhysicsSubsystem>();
			}
		}

		public override void Initialize()
		{
            bool found = false;
            Type physicsBodyType = typeof(PhysicsBodyComponent);
            IEnumerable<BlobbyComponent> components = Owner
                .ComponentLookup
                .Values
                .Where((component) => component.GetType().IsSubclassOf(physicsBodyType));

            foreach(BlobbyComponent component in components)
            {
                if (found)
                {
					System.Console.Error.WriteLine("More than one component of type '" + physicsBodyType.FullName + "' was added to game object");
                    break;
                }
                else
                {
                    PhysicsBodyComponent physicsComponent = (PhysicsBodyComponent) component;
                    physicsComponent.RegisterCollider(this);
                    physicsBodyComponent = physicsComponent;
                    found = true;
                }
            }
            if (!found) {
				System.Console.Error.WriteLine("Could not find physics body component to bind collider to");
			}
    	}


        protected override void Dispose(bool nativeOnly)
        {
            if (physicsBodyComponent != null)
            {
                physicsBodyComponent.UnregisterCollider(this);
            }
            base.Dispose(nativeOnly);
        }
    }
}