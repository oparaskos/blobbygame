﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Blobby.Core.Components
{
    public class SliderButtonComponent : BlobbyComponent
	{
		private ClickableComponent _button = null;
		private float _value = 0.5f;

		public delegate void OnValueChangedEventHandler(float val);
		public event OnValueChangedEventHandler OnValueChanged;

        public int UpperBound { get; set; }

        public int LowerBound { get; set; }

        public bool IsPressed { 
            get
            {
                return _button.IsPressed;
            }
        }

        public int Width
        {
            get
            {
                return UpperBound - LowerBound;
            }
        }

        public float Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = MathHelper.Clamp(value, 0.0f, 1.0f);
                Owner.RelativePosition = new Vector2(
                    MathHelper.Clamp(LowerBound + _value * Width,
                                     LowerBound,
                                     UpperBound),
					0);
				OnValueChanged?.Invoke(_value);
            }
        }

        public override void Initialize()
        {
            _button = Owner.GetComponent<ClickableComponent>();
        }

        public override void Update(GameTime gameTime)
        {
            if (IsPressed)
            {
	            int absMouseX = Mouse.GetState().Position.X - (Game.Window.ClientBounds.Width / 2);
	            int relMouseX = absMouseX - (int)Owner.Parent.AbsolutePosition.X;
	            Value = (relMouseX - LowerBound) / (float) Width;
            }
        }
    }
}
