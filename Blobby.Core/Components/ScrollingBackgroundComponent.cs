using Blobby.Core;
using Microsoft.Xna.Framework;

namespace Blobby.Core.Components
{
    public class ScrollingBackgroundComponent : BlobbyComponent
    {
        public ScrollingBackgroundComponent() : base()
        {

        }
        public override void Update(GameTime gameTime)
        {
            Sprite s = Owner.GetComponent<SpriteRendererComponent>().ActiveSprite;
            CameraComponent c = (CameraComponent)Game.ActiveCamera;
            s.SourceRectangle = new Rectangle((int)(c.Owner.AbsolutePosition.X * 1.1f), (int)(c.Owner.AbsolutePosition.Y*1.1f), 1280, 1280);
            s.Offset = Vector2.Add(-c.Owner.AbsolutePosition, new Vector2(640, 640));
        }
    }
}