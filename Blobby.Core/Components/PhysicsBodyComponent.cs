using Blobby.Core.Physics;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;

namespace Blobby.Core.Components
{
    public class PhysicsBodyComponent : BlobbyComponent
	{
        private Body _body = null;
		public Body Body {
            get
            {
                return _body;
            }
            protected set
            {
                _body = value;
            }
        }

		public bool Stationary
		{
			get
			{
				return !(Body != null
					&& Body.LinearVelocity.LengthSquared() > PhysicsSubsystem.STOPPED_SPEED);
			}
		}

		public bool Rotating
		{
			get
			{
				return Body?.AngularVelocity > PhysicsSubsystem.STOPPED_SPEED;
			}
		}

        public PhysicsSubsystem PhysicsSubsystem 
        {
            get
            {
                return Owner.GetSubsystem<PhysicsSubsystem>();   
            }
        }

        public override void Update(GameTime gameTime) {
            Body.Position = Owner.AbsolutePosition;
        }

        protected override void Dispose(bool nativeOnly)
        {
            if (Body != null)
            {
                PhysicsSubsystem.World.RemoveBody(Body);
                Body = null;
            }
            base.Dispose(nativeOnly);
		}

        public virtual void RegisterCollider(ColliderComponent collider)
		{
            if (Body != null)
            {
                collider.Fixture = Body.CreateFixture(collider.CollisionShape, collider);
            }
		}

		public virtual void UnregisterCollider(ColliderComponent collider)
		{
            // If this has been destroyed before the colliders then Body may be null.
            if (Body != null)
            {
                Body.DestroyFixture(collider.Fixture);
            }
		}
    }
}