using System;
using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace Blobby.Core.Components
{
    public class AudioEmitterComponent : BlobbyComponent
    {
        SoundEffectInstance soundEffectInstance;
        private bool initialised = false;
        private bool isLooping = false;
        public bool IsLooping
        {
            get
            {
                if (soundEffectInstance != null)
                {
                    return soundEffectInstance.IsLooped;
                }
                else
                {
                    return isLooping;
                }
            }
            set
            {
                if (soundEffectInstance != null)
                {
                    soundEffectInstance.IsLooped = value;
                }
                else
                {
                    isLooping = value;
                }
            }
        }

        private float volume = 1.0f;
        public float Volume
        {
            get
            {
                if (soundEffectInstance != null)
                {
                    return soundEffectInstance.Volume;
                }
                else
                {
                    return volume;
                }
            }
            set
            {
                if (soundEffectInstance != null)
                {
                    soundEffectInstance.Volume = value;
                }
                else
                {
                    volume = value;
                }
            }
        }
        private bool is3D;
        public bool PlayOnStart { get; set; }
        public bool Is3D
        {
            get
            {
                return is3D;
            }
            set
            {
                if (initialised)
                {
                    throw new InvalidOperationException("You cannot set Is3d after initialisation");
                }
                is3D = value;
            }
        }

        public override void LoadContent()
        {
            SoundEffect soundEffect = Game.Content
                .Load<SoundEffect>("Audio/Ambience/AmbientElectronicLoop_02");
            soundEffectInstance = soundEffect?.CreateInstance();
        }

        public override void Initialize()
        {
            if (soundEffectInstance != null)
            {
                soundEffectInstance.Volume = volume;
                soundEffectInstance.IsLooped = isLooping;
                if (Is3D)
                {
                    ApplyPositionalSound();
                }
                if (PlayOnStart)
                {
                    Play();
                }
            }
        }
        
        public void Play()
        {
            if (soundEffectInstance != null)
            {
                soundEffectInstance.Play();
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (Is3D)
            {
                ApplyPositionalSound();
            }
        }

        protected override void Dispose(bool nativeOnly)
        {
            if (soundEffectInstance != null)
            {
                soundEffectInstance.Stop();
                soundEffectInstance.Dispose();
            }
            base.Dispose(nativeOnly);
        }

        private void ApplyPositionalSound()
        {
            if (soundEffectInstance != null)
            {
                var emitter = new AudioEmitter();
                emitter.Position = new Vector3(Owner.AbsolutePosition, 0);
                var listener = new AudioListener();
                listener.Position = new Vector3(((CameraComponent)Game.ActiveCamera).Owner.AbsolutePosition, 0);
                soundEffectInstance.Apply3D(listener, emitter);
            }
        }
    }
}