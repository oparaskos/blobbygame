using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Blobby.Core;

namespace Blobby.Core.Components
{
    public class CameraComponent : BlobbyComponent, ICameraComponent
    {
        private AudioListener _audioListener = null;
        public AudioListener AudioListener
        {
            get
            {
                if(_audioListener == null) {
                    _audioListener = new AudioListener();
                }
                return _audioListener;
            }
        }
        public Rectangle Bounds
        {
            get
            {
                return Game.Window.ClientBounds;
            }
        }
        public float Zoom { get; set; } = 1f;

        public bool Active
        {
            get
            {
                return Game != null
                    && Game.ActiveCamera == this;
            }
            set
            {
                if (Game == null)
                {
                    throw new InvalidOperationException("Cannot set Active Camera before Initialize completed");
                }

                if (value)
                {
                    Game.ActiveCamera = this;
                }
                else
                {
                    Game.ActiveCamera = null;
                }
            }
        }

        public Matrix Transform
        {
            get
            {
                return Matrix.CreateTranslation(-Owner.AbsolutePosition.X, -Owner.AbsolutePosition.Y, 0)
                    * Matrix.CreateRotationZ(-Owner.AbsoluteRotation)
                    * Matrix.CreateScale(Zoom)
                    * Matrix.CreateTranslation(new Vector3(Bounds.Width * 0.5f, Bounds.Height * 0.5f, 0));
            }
        }

        public CameraComponent() : base() { }

        public override void Update(GameTime gameTime) {
            AudioListener.Position = new Vector3(Owner.AbsolutePosition, 0);
        }
    }
}