﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace Blobby.Core
{
    public class DefaultGraphicsDevice : IGraphicsDevice
    {
        public GraphicsDevice BaseGraphicsDevice { get; private set; }
        public Viewport Viewport
        {
            get
            {
                return BaseGraphicsDevice.Viewport;
            } 
            set
            {
                BaseGraphicsDevice.Viewport = value;
            }
        }

        public DefaultGraphicsDevice(GraphicsDevice graphicsDevice)
        {
            BaseGraphicsDevice = graphicsDevice;
        }

        public void Clear(Color color)
        {
            BaseGraphicsDevice.Clear(color);
        }

        public Texture2D CreateTexture2D(int v1, int v2)
        {
            return new Texture2D(BaseGraphicsDevice, v1, v2);
        }
    }
}
