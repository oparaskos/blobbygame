﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace Blobby.Core
{
    public class DefaultInputManager : IInputManager, IConfigurableKeyboardInput, IConfigurableGamepadInput
    {
        Dictionary<string, Button> buttonMappings = new Dictionary<string, Button>();

        private Button RegisterButtonMapping(string inputName)
        {
            if (!buttonMappings.ContainsKey(inputName))
            {
                buttonMappings.Add(inputName, new Button());
            }
            return buttonMappings[inputName];
        }

        public void RegisterGamepadButton(string inputName, Buttons button)
        {
            RegisterButtonMapping(inputName).gamepadButtons.Add(button);
        }

        public void RegisterKeyboardKey(string inputName, Keys key)
        {
            RegisterButtonMapping(inputName).keyboardKeys.Add(key);
        }

        public bool GetButton(string inputName)
        {
            return buttonMappings[inputName].Pressed;
        }

        public class Button
        {
            public List<Keys> keyboardKeys = new List<Keys>();
            public List<Buttons> gamepadButtons = new List<Buttons>();

            /// <summary>
            /// Button is down if any of its registered keys are down.
            /// </summary>
            public bool Pressed {
                get
                {
                    foreach (Keys keyboardKey in keyboardKeys)
                    {
                        if (Keyboard.GetState().IsKeyDown(keyboardKey))
                        {
                            return true;
                        }
                    }

                    foreach (Buttons gamepadButton in gamepadButtons)
                    {
                        if (GamePad.GetState(0).IsButtonDown(gamepadButton))
                        {
                            return true;
                        }
                    }

                    return false;
                }
            }
        }

        public void RegisterKeyboardKey(string inputName, params Keys[] keys)
        {
            foreach(Keys key in keys)
            {
                RegisterKeyboardKey(inputName, key);
            }
        }

        public void RegisterGamepadButton(string inputName, params Buttons[] buttons)
        {
            foreach(Buttons button in buttons)
            {
                RegisterGamepadButton(inputName, button);
            }
        }
    }
}
