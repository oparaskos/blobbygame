# Readme

[![Build Status](https://img.shields.io/appveyor/ci/oparaskos/blobbygame.svg)](https://ci.appveyor.com/project/oparaskos/blobbygame)
[![Build Stats](https://buildstats.info/appveyor/chart/oparaskos/blobbygame?branch=master)](https://ci.appveyor.com/project/oparaskos/blobbygame)
# Ideas

## Multiplayer

### Server Structure Overview

These ramblings are just thoughts, will likely change before and during implementation

At the top level, a load balancer balancing between several micro-instances which connect to a high availability database tracking the location and velocity of players spacecraft.

When the player changes grid-square they open a connection to another load-balanced server which manages that square, again tracking the location of players in the gridsquare but also managing AI players, resources and economy.

During a battle (or when there is a convoy of multiple players in viewing range of oneanother)

* The player opens a connection to a dedicated server spun up for this battle.
* As more players join the load on the server may reach a limit, at which point we need to prevent new players from joining
 * Ingame canon can explain this (Ships' engines emit some kind of radiation which makes proximity dangerous, at a threshold all players in proximity to the radiation take damage)

