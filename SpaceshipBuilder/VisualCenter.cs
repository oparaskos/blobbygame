﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Linq;

namespace SpaceshipBuilder
{
    public class VisualCenter
    {
        private const double COLOR_DIFF_WEIGHT_EXPO = 0.333;
        private const int ROUNDS = 100;

        public static Point FromImage(Texture2D image)
        {
            int w = image.Width;
            int h = image.Height;
            Color[] rgbMatrix = new Color[w * h];
            image.GetData(rgbMatrix);
            float visualLeft = 0.5f;
            float visualTop = 0.5f;
            visualLeft = RecursiveGetCoord(rgbMatrix, w, h, visualLeft, visualTop, 'X', 1 / ROUNDS).X;
            visualLeft = RecursiveGetCoord(rgbMatrix, w, h, visualLeft, visualTop, 'X', -1 / ROUNDS).X;
            visualTop = RecursiveGetCoord(rgbMatrix, w, h, visualLeft, visualTop, 'Y', 1 / ROUNDS).Y;
            visualTop = RecursiveGetCoord(rgbMatrix, w, h, visualLeft, visualTop, 'Y', -1 / ROUNDS).Y;
            return new Point((int)(visualLeft * w), (int)(visualTop * h));
        }

        private static Vector2 RecursiveGetCoord(Color[] rgbMatrix, int width, int height, float visualLeft, float visualTop, char currentAxis, int stepSize)
        {
            Color bgColor = rgbMatrix[0];
            float visualLeftToApply = visualLeft;
            float visualTopToApply = visualTop;
            int maxDiff =
                Math.Max(bgColor.R, (int)255 - bgColor.R) +
                Math.Max(bgColor.G, (int)255 - bgColor.G) +
                Math.Max(bgColor.B, (int)255 - bgColor.B);
            double maxDistance = new Vector2(width, height).Length();
            float newVisualLeft = visualLeft;
            float newVisualTop = visualTop;

            if (currentAxis == 'X')
            {
                newVisualLeft += stepSize;
            }
            else
            {
                newVisualTop += stepSize;
            }

            double oldCenterIntensity = GetCenterIntensity(rgbMatrix, width, height, visualLeft, visualTop, bgColor, maxDiff, maxDistance);
            double newCenterIntensity = GetCenterIntensity(rgbMatrix, width, height, newVisualLeft, newVisualTop, bgColor, maxDiff, maxDistance);

            while (newCenterIntensity > oldCenterIntensity)
            {
                visualLeftToApply = newVisualLeft;
                visualTopToApply = newVisualTop;

                if (currentAxis == 'X')
                {
                    newVisualLeft += stepSize;
                }
                else
                {
                    newVisualTop += stepSize;
                }
                oldCenterIntensity = newCenterIntensity;
                newCenterIntensity = GetCenterIntensity(rgbMatrix, width, height, newVisualLeft, newVisualTop, bgColor, maxDiff, maxDistance);
            }
            return new Vector2(visualLeftToApply, visualTopToApply);
        }

        private static int GetCenterIntensity(Color[] rgbMatrix, int width, int height, float visualLeft, float visualTop, Color bgColor, int maxDiff, double maxDistance)
        {
            int centerCol = (int)(height * visualTop);
            int centerRow = (int)(width * visualLeft);
            Point centerPoint = new Point(centerCol, centerRow);
            int resCol = 0;
            for (int rowIdx = 0; rowIdx < width; ++rowIdx)
            {
                for (int colIdx = 0; colIdx < width; ++colIdx)
                {
                    Color col = rgbMatrix[rowIdx * width + colIdx];
                    int cellColorDiff = RgbDiff(bgColor, col, maxDiff);

                    if (cellColorDiff == 0)
                    {
                        return resCol;
                    }

                    double cellDistance = (centerPoint.ToVector2() - new Vector2(rowIdx, colIdx)).Length();
                    int cellColorWeight = (int)(cellColorDiff * Math.Pow(1 - cellDistance / maxDistance, 0.5) * 1000);
                    resCol += cellColorWeight;
                }
            }
            return resCol;
        }

		private static int RgbDiff(Color baseColor, Color testColor, double maxDiff)
		{
		    if (testColor.A == 0) {
		        return 0;
		    }

		    double diff =
                Math.Abs(baseColor.R - testColor.R) +
		        Math.Abs(baseColor.G - testColor.G) +
		        Math.Abs(baseColor.B - testColor.B);
		    return (int) Math.Round(Math.Pow(diff / maxDiff, COLOR_DIFF_WEIGHT_EXPO) * (testColor.A / 255) * 1000);
		}
    }
}



























