﻿using System;
using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Moq;

namespace Blobby.MyGame.Specs.MockGame
{
    public class PhantomGame : IBlobbyGame
    {
        public Mock<IContentManager> MockContentManager = new Mock<IContentManager>();
        public Mock<GameWindow> MockGameWindow = new Mock<GameWindow>();

        public event EventHandler<EventArgs> EnabledChanged;
        public event EventHandler<EventArgs> UpdateOrderChanged;

        public bool ExitHasBeenCalled { get; private set; } = false;
        public ICameraComponent ActiveCamera { get; set; }
        public IContentManager Content { get; set; } = null;
        public IGraphicsDevice GraphicsDevice { get; set; } = new MockGraphicsDevice();
        public GameWindow Window { get; set; } = null;
        public IInputManager Input { get; internal set; }
        public bool Enabled { get; } = true;
        public int UpdateOrder { get; } = 0;
        public GameObject GameObject { get; set; }

        public PhantomGame()
        {
            Input = new MockInputManager();
            Content = MockContentManager.Object;
            Window = MockGameWindow.Object;
            MockContentManager.Setup(x => x.Load<SoundEffect>(It.IsAny<string>())).Returns<SoundEffect>(null);
        }

        public void Exit()
        {
            ExitHasBeenCalled = true;
        }

        public void Update(GameTime gameTime)
        {
            GameObject?.Update(gameTime);
        }
    }
}
