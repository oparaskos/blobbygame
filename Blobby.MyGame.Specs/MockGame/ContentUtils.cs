﻿using Microsoft.Xna.Framework.Audio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blobby.MyGame.Specs.MockGame
{
    public static class ContentUtils
    {
        public static SoundEffect EmptySoundEffect()
        {
            // TODO: Need to wrap sound effect loading since it tries to contact the sound drivers at start which wont be loaded in tests.
            int bufferlen = 1024*8;
            byte[] buffer = new byte[bufferlen];
            var silence = new SoundEffect(buffer, 0, bufferlen, bufferlen, AudioChannels.Mono, 0, 1);
            silence.Name = "Silence";
            return silence;
        }
    }
}
