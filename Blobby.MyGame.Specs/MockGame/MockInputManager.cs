﻿using Blobby.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blobby.MyGame.Specs.MockGame
{
    class MockInputManager : IInputManager
    {
        Dictionary<string, bool> keystates = new Dictionary<string, bool>();

        public bool GetButton(string inputName)
        {
            return keystates.ContainsKey(inputName) && keystates[inputName];
        }

        public void SetButton(string inputName, bool pressed)
        {
            keystates[inputName] = pressed;
        }
    }
}
