﻿using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blobby.MyGame.Specs.MockGame
{
    internal class MockGraphicsDevice : IGraphicsDevice
    {
        public Viewport Viewport { get; set; } = new Viewport(new Rectangle(0, 0, 100, 100));

        public void Clear(Color black)
        {/* Nothing to do. */}

        public Texture2D CreateTexture2D(int v1, int v2)
        {
            return new Texture2D(null, 0, 0);
        }
    }
}