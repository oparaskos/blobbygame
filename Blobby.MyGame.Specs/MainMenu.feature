﻿Feature: Main Menu
	In order to access game options, as well as the game
	As a player
	I want to click a button to to indicate my intentions

Scenario: Starting the game
	Given The player is on the 'Main Menu' screen
	When The player clicks on the 'Start Game Button'
	Then The 'Main Menu' screen should be hidden
	And The 'Main Game' should have been loaded
