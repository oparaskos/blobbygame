﻿Feature: Deris Field
	In order to find interesting loot and a source of cover in space
	As a player
	I want to be able to fly into a debris field

Background: Given a running game
	Given A Game World
	  And The Game World contains a player spaceship
	  And The Game World contains a debris field
	  And The game has started

Scenario: Entering the debris field
	Given I am piloting a spaceship
	  And the ship is about to enter a debris field
	 When I enter the debris field
	 Then An ominous sound effect is played

Scenario: Scavange from floating debris
	Given I am piloting a spaceship
	  And I the spaceship is adjacent to a piece of debris
	  And The debris contains some useful element
	 When I select the debris
	  And I select to extract the valuable content from the debris
	 Then the result should be 120 on the screen
