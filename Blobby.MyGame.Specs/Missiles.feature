﻿Feature: Players can fire Missiles
    As a player I want to shoot missiles at other spaceships
    Such that I can defend my spaceship
    And I can loot the contents of enemy spaceships
    And I can level-up.
	
Background:
	Given A Game World
	  And The Game World contains a player spaceship
	  And The Game World contains a target spaceship
	  And The target spaceship is 100 units away from the player spaceship
	  And The game has started

# Test the test is set up correctly -- always double check your measurement tools
Scenario: Spaceship is in front of the target
	Given I am piloting a spaceship
	When The next update call to the object happens with a deltaTime of 1 second
	Then The spaceship will be facing the target
	 And The spaceship will be ≅100 units the target

Scenario: Missile hits its target
	Given I am piloting a spaceship
      And A missile has been fired
     When The missile collides with its target
     Then The missile will explode

Scenario: Missile stops and detonates before reaching its target
	Given I am piloting a spaceship
      And A missile has been fired
      And The missile does not have enough momentum to reach the obstacle
     When The missile comes to a stop
     Then The missile will explode

Scenario: An exploding missile damages nearby objects
	Given I am piloting a spaceship
      And A missile has been fired
      And The missile has come to a stop
      And The missile is within 300 units of an object
      And The object can take damage
     When The missile explodes
     Then The objects in range will be damaged
      And The damage will be proportional to the distance to the center of the explosion.

Scenario: Explosion will cause more damage than available shield power
	Given I am piloting a spaceship
      And A missile has been fired
      And The missile has exploded
      And The explosion is within range of a spaceship
      And The spaceship has shields charged to 1%
     When The missile explodes
     Then The shield's health will take damage
      And The spaceship's health will remain at its previous level

Scenario: Missiles damage shields ahead of spaceship health
	Given I am piloting a spaceship
      And A missile has been fired
      And The missile will explode
      And The explosion is within range of a spaceship
      And The spaceship has available shields
     When The missile explodes
     Then The shield's health will take damage

Scenario: Missiles can penetrate depleted shields
	Given I am piloting a spaceship
      And A missile has been fired
      And The missile will explode
      And The explosion is within range of a spaceship
      And The spaceship has depleted shields
     When The missile explodes
     Then The spaceship's health will take damage
