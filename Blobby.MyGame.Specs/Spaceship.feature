﻿Feature: Piloting a Spaceship
	In order to get from place to place in game
	As a player
	I want to fly a spaceship

Background:
	Given A Game World
	  And The Game World contains a player spaceship
	  And The game has started
	
Scenario: Flying Forward
	Given I am piloting a spaceship
	  And the spaceship is pointing 0 degrees
	 When I press the 'Forward' button for 2 frame
	 Then The spaceship velocity should increase
	  And The spaceship should be moving in the direction it is pointing
	  And The spaceship's thrusters should be firing
	  
Scenario: Flying Backwards
	Given I am piloting a spaceship
	 When I press the 'Backward' button for 2 frame
	 Then The spaceship velocity should increase
	  And The spaceship should be moving opposite to the direction it is pointing
	  
Scenario: Turning Left
	Given I am piloting a spaceship
	 When I press the 'Left' button for 2 frame
	 Then The spaceship should rotate counter clockwise

Scenario: Turning Right
	Given I am piloting a spaceship
	 When I press the 'Right' button for 1 frame
	 Then The spaceship should rotate clockwise
