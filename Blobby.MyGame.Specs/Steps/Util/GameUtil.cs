﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace Blobby.MyGame.Specs.Steps.Util
{
    class GameUtil
    {
        public static void SimulateTicks(int numTicks, double secondsPerTick = 0.1)
        {
            TimeSpan elapsedTime;
            elapsedTime = InitialiseElapsedTime();
            IUpdateable rootGameObject = ContextUtil.GetLeastSpecificUpdatable();
            for (int i = 0; i < numTicks; ++i)
            {
                elapsedTime += TimeSpan.FromSeconds(secondsPerTick);
                GameTime gameTime = new GameTime(elapsedTime, TimeSpan.FromSeconds(secondsPerTick));
                rootGameObject.Update(gameTime);
            }
            // Keep track of the elapsed game time, based on the number of simulated ticks.
            ScenarioContext.Current.Set<TimeSpan>(elapsedTime, "elapsedTime");
        }

        // TODO: I think this is duplicated code.
        public static TimeSpan InitialiseElapsedTime()
        {
            TimeSpan elapsedTime;
            if (ScenarioContext.Current.ContainsKey("elapsedTime"))
            {
                elapsedTime = ScenarioContext.Current.Get<TimeSpan>("elapsedTime");
            }
            else
            {
                elapsedTime = new TimeSpan(0, 0, 0);
            }

            return elapsedTime;
        }
    }
}
