﻿using Blobby.Core;
using Blobby.MyGame.Specs.MockGame;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace Blobby.MyGame.Specs.Steps.Util
{
    public static class ContextUtil
    {
        /// <summary>
        /// Look for a context Game, rootgameobject or gameObject in that order
        /// </summary>
        /// <returns>The least specific match, throws if none</returns>
        public static IUpdateable GetLeastSpecificUpdatable()
        {
            PhantomGame game = null;
            if (ScenarioContext.Current.TryGetValue<PhantomGame>(out game))
            {
                return game;
            }
            else
            {
                return GetLeastSpecificGameObject();
            }
        }

        public static GameObject GetLeastSpecificGameObject()
        {
            RootGameObject root = null;
            if (ScenarioContext.Current.TryGetValue<RootGameObject>(out root))
            {
                return root;
            }
            else
            {
                return ScenarioContext.Current.Get<GameObject>();
            }
        }
    }
}
