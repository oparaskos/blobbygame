﻿using Blobby.Core;
using Blobby.Core.Physics;
using Blobby.Core.Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blobby.MyGame.Specs.Steps
{
    public class GameWorldSteps
    {
        public static RootGameObject CreateWorld(IBlobbyGame game, params GameObject[] children)
        {
            RootGameObject worldRoot = new RootGameObject();
            worldRoot.AddSubsystem<SteamworksSubsystem>();
            worldRoot.AddSubsystem<PhysicsSubsystem>();
            worldRoot.SetGame(game);
            foreach (GameObject child in children)
            {
                worldRoot.AddChild(child);
            }
            return worldRoot;
        }
    }
}
