﻿using Blobby.Core;
using Blobby.Core.Components;
using Blobby.MyGame.Specs.MockGame;
using NUnit.Framework;
using System;
using System.Linq;
using TechTalk.SpecFlow;
using Microsoft.Xna.Framework;
using Blobby.MyGame.Components;
using Blobby.Core.Physics;
using Blobby.MyGame.Factory;
using Blobby.MyGame.Specs.Steps.Util;

namespace Blobby.MyGame.Specs.Steps
{
    [Binding]
    public class SpaceshipSteps
    {
        [Given(@"A Game World")]
        public void GivenAGameWorld()
        {
            PhantomGame game = new PhantomGame();
            RootGameObject gameObject = GameWorldSteps.CreateWorld(game);
            game.GameObject = gameObject;
            ScenarioContext.Current.Set<PhantomGame>(game);
            ScenarioContext.Current.Set<RootGameObject>(gameObject);
        }

        [Given(@"The Game World contains a target spaceship")]
        public void GivenTheGameWorldContainsATargetSpaceship()
        {
            SpaceshipFactory shipFactory = new SpaceshipFactory();
            GameObject targetShip = shipFactory.CreateSpaceship(Vector2.Zero);
            targetShip.Name = "Target Spaceship";
            AddSpaceshipToScene(targetShip);
        }
        
        [Given(@"The Game World contains a player spaceship")]
        public void GivenTheGameWorldContainsAPlayerSpaceship()
        {
            SpaceshipFactory shipFactory = new SpaceshipFactory();
            GameObject playerShip = shipFactory.CreateSpaceship(Vector2.Zero);
            // A player spaceship is one pilotable by the player.
            playerShip.Name = "Player Spaceship";
            playerShip.AddComponent<MyPlayerControllerComponent>();
            AddSpaceshipToScene(playerShip);
        }

        private static void AddSpaceshipToScene(GameObject playerShip)
        {
            RootGameObject root = ScenarioContext.Current.Get<RootGameObject>();
            root.AddChild(playerShip);
            ScenarioContext.Current.Set<GameObject>(playerShip, playerShip.Name);
        }

        /// <summary>
        /// Put the target spaceship in front of the player
        /// </summary>
        /// <param name="distanceToTarget">desired distance to the target spaceship</param>
        [Given(@"The target spaceship is (.*) units away from the player spaceship")]
        public void GivenTheTargetSpaceshipIsNUnitsAwayFromThePlayerSpaceship(Decimal distanceToTarget)
        {
            GameObject player = ScenarioContext.Current.Get<GameObject>("Player Spaceship");
            GameObject target = ScenarioContext.Current.Get<GameObject>("Target Spaceship");
            Vector2 vectorToTarget = new Vector2(
                (float)Math.Cos(player.AbsoluteRotation),
                (float)Math.Sin(player.AbsoluteRotation));
            vectorToTarget.Normalize();
            vectorToTarget *= (float)distanceToTarget;
            target.RelativePosition = player.AbsolutePosition + vectorToTarget;
        }

        [Given(@"The game has started")]
        public void GivenTheGameHasStarted()
        {
            GameObject root = ContextUtil.GetLeastSpecificGameObject();
            root.Initialize();
        }

        [Given(@"I am piloting a spaceship")]
        public void GivenIAmPilotingASpaceship()
        {
            // Player is piloting a spaceship when the default game first starts,
            //   just assert that is the case just in case something changes.
            GameObject rootGameObject = ContextUtil.GetLeastSpecificGameObject();
            MyPlayerControllerComponent playerShipController = rootGameObject.FindComponentOnChild<MyPlayerControllerComponent>();
            Assert.NotNull(playerShipController);
            GameObject shipObject = playerShipController.Owner;
            Assert.NotNull(shipObject);
            // Check the player controller is attached to a spaceship
            Assert.IsTrue(shipObject.HasComponent<MySpaceshipControllerComponent>());
            ScenarioContext.Current.Set<GameObject>(shipObject, "Player Ship");
        }

        [Given(@"the spaceship is pointing (.*) degrees")]
        public void GivenTheSpaceshipIsPointingDegrees(Decimal angle)
        {
            float rads = MathHelper.ToRadians((float)angle);
            ScenarioContext.Current.Get<GameObject>("Player Ship").RelativeRotation = rads;
        }

        [When(@"I press the '(.*)' button for (.*) frame")]
        public void WhenIPressTheButtonForFrame(string buttonName, int numFrames)
        {
            PhantomGame game = ScenarioContext.Current.Get<PhantomGame>();
            // With our mock input manager we can simulate keypresses
            ((MockInputManager)game.Input).SetButton(buttonName, true);
            GameUtil.SimulateTicks(numFrames, 1);
        }
        
        [Then(@"The spaceship velocity should increase")]
        public void ThenTheSpaceshipVelocityShouldIncrease()
        {
            GameObject playerShip = ScenarioContext.Current.Get<GameObject>("Player Ship");
            var physics = playerShip.FindComponentOnChild<RigidBodyComponent>();
            Assert.Greater(physics.Velocity.LengthSquared(), PhysicsSubsystem.STOPPED_SPEED);
        }
        
        [Then(@"The spaceship should be moving in the direction it is pointing")]
        public void ThenTheSpaceshipShouldBeMovingInTheDirectionItIsPointing()
        {
            GameObject playerShip = ScenarioContext.Current.Get<GameObject>("Player Ship");
            float vectorAngle = GetShipVelocityAngle(playerShip);
            Assert.AreEqual(playerShip.AbsoluteRotation, vectorAngle, 0.01);
        }

        private static float GetShipVelocityAngle(GameObject playerShip)
        {
            var physics = playerShip.FindComponentOnChild<RigidBodyComponent>();
            float vectorAngle = (float)(-Vector2.UnitY).Angle(physics.Velocity);
            return vectorAngle;
        }

        [Then(@"The spaceship should be moving opposite to the direction it is pointing")]
        public void ThenTheSpaceshipShouldBeMovingOppositeToTheDirectionItIsPointing()
        {
            GameObject playerShip = ScenarioContext.Current.Get<GameObject>("Player Ship");
            float vectorAngle = GetShipVelocityAngle(playerShip);
            // Forwards is the ship angle, +PI is backwards.
            Assert.AreEqual(playerShip.RelativeRotation + Math.PI, vectorAngle, 0.01);
        }

        [Then(@"The spaceship's thrusters should be firing")]
        public void ThenTheSpaceshipSThrustersShouldBeFiring()
        {
            Assert.True(true);
        }

        [Then(@"The spaceship should rotate counter clockwise")]
        public void ThenTheSpaceshipShouldRotateCCW()
        {
            GameObject playerShip = ScenarioContext.Current.Get<GameObject>("Player Ship");
            Assert.Less(playerShip.RelativeRotation, -0.01);
        }

        [Then(@"The spaceship should rotate clockwise")]
        public void ThenTheSpaceshipShouldRotateClockwise()
        {
            GameObject playerShip = ScenarioContext.Current.Get<GameObject>("Player Ship");
            Assert.Greater(playerShip.RelativeRotation, 0.01);
        }
    }
}
