﻿using Blobby.Core;
using Blobby.Core.Components;
using Blobby.Core.Components.Debugging;
using Blobby.MyGame.Specs.MockGame;
using Blobby.MyGame.Specs.Steps.Util;
using Microsoft.Xna.Framework;
using NUnit.Framework;
using System;
using System.Text.RegularExpressions;
using TechTalk.SpecFlow;

namespace Blobby.MyGame.Specs.Steps
{
    [Binding]
    public class FPSCounterSteps
    {
        [Given(@"A FPS Counter Object")]
        public void GivenAFPSCounterObject()
        {
            GameObject fpsCounter = new GameObject("FPS Counter");
            fpsCounter.AddComponent<FPSCounterComponent>();
            fpsCounter.AddComponent<TextRendererComponent>();
            ScenarioContext.Current.Set(fpsCounter);
        }
        
        [Given(@"The last (.*) update calls to the object had a delta time of (.*) seconds")]
        public void GivenTheLastUpdateCallsHadADeltaTimeOfSeconds(int numUpdateCalls, Decimal frameDuration)
        {
            GameObject fpsCounter = ScenarioContext.Current.Get<GameObject>();
            TimeSpan elapsedTime = TimeSpan.FromSeconds((double) frameDuration);
            for (int i = 0; i < numUpdateCalls; ++i)
            {
                TimeSpan totalTime = TimeSpan.FromSeconds((double) (frameDuration + (frameDuration * i)));
                ScenarioContext.Current.Set(totalTime, "GameTime");
                GameTime frameTime = new GameTime(totalTime, elapsedTime);
                fpsCounter.Update(frameTime);
            }
        }

        [When(@"The next update call to the object happens with a deltaTime of (.*) second")]
        public void WhenTheNextUpdateCallHappensWithADeltaTimeOfSecond(Decimal frameDuration)
        {
            TimeSpan frameTime = TimeSpan.FromSeconds((double)frameDuration);
            TimeSpan gameTimeBeforeFrame = TimeSpan.Zero;
            // If we had some game time before now add to the existing game time.
            if (ScenarioContext.Current.ContainsKey("GameTime"))
            {
                gameTimeBeforeFrame = ScenarioContext.Current.Get<TimeSpan>("GameTime");
            }
            TimeSpan totalTime = gameTimeBeforeFrame.Add(frameTime);
            ScenarioContext.Current.Set<TimeSpan>(gameTimeBeforeFrame, "GameTime");
            GameTime gameTime = new GameTime(totalTime, frameTime);
            IUpdateable fpsCounter = ContextUtil.GetLeastSpecificUpdatable();
            fpsCounter.Update(gameTime);
        }
        
        [Then(@"the result should be ≅(.*) seconds on the screen")]
        public void ThenTheResultShouldBeSecondsOnTheScreen(Decimal approximateSeconds)
        {
            Regex findNumber = new Regex(@"^[^\d]*(\d+\.?\d+)[^\d]*$");
            GameObject fpsCounter = ScenarioContext.Current.Get<GameObject>();
            string onScreen = fpsCounter.GetComponent<TextRendererComponent>().Text;
            string number = findNumber.Match(onScreen).Groups[1].Value;
            Decimal actual = Decimal.Parse(number);
            Assert.AreEqual((double) approximateSeconds, (double) actual, 0.2d);
        }
    }
}
