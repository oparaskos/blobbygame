﻿using Blobby.Core;
using Blobby.Core.Components;
using Blobby.MyGame.Components;
using Blobby.MyGame.Specs.MockGame;
using Blobby.MyGame.Specs.Steps.Util;
using Microsoft.Xna.Framework;
using NUnit.Framework;
using System;
using System.Diagnostics;
using TechTalk.SpecFlow;

namespace Blobby.MyGame.Specs.Steps
{
    [Binding]
    public class MissilesSteps
    {
        [Then(@"The spaceship will be facing the target")]
        public void ThenTheSpaceshipWillBeFacingTheTarget()
        {
            float tolerance = 0.01f;
            GameObject player = ScenarioContext.Current.Get<GameObject>("Player Spaceship");
            GameObject target = ScenarioContext.Current.Get<GameObject>("Target Spaceship");
            Vector2 expectedVectorToTarget = new Vector2((float)Math.Cos(player.AbsoluteRotation), (float)Math.Sin(player.AbsoluteRotation));
            Vector2 actualVectorToTarget = target.AbsolutePosition - player.AbsolutePosition;
            Assert.AreEqual(
                Vector2.Dot(expectedVectorToTarget, actualVectorToTarget),
                actualVectorToTarget.Length(),
                tolerance);
        }

        [Then(@"The spaceship will be ≅(.*) units the target")]
        public void ThenTheSpaceshipWillBeWithinNUnitsOfTheTarget(Decimal numUnits)
        {
            GameObject player = ScenarioContext.Current.Get<GameObject>("Player Spaceship");
            GameObject target = ScenarioContext.Current.Get<GameObject>("Target Spaceship");
            Assert.Less((player.AbsolutePosition - target.AbsolutePosition).Length(), (float)numUnits + 10);
            Assert.Greater((player.AbsolutePosition - target.AbsolutePosition).Length(), (float)numUnits - 10);
        }

        [Given(@"A missile has been fired")]
        public void GivenAMissileHasBeenFired()
        {
            GameObject player = ScenarioContext.Current.Get<GameObject>("Player Spaceship");
            var ship = player.GetComponent<MySpaceshipControllerComponent>();
            GameObject missile = ship.FireTorpedo();
            Assert.That(missile.Initialised);
            ScenarioContext.Current.Set<GameObject>(missile, "Missile");
        }
        
        [When(@"The missile collides with its target")]
        public void WhenTheMissileCollidesWithItsTarget()
        {
            GameObject missile = ScenarioContext.Current.Get<GameObject>("Missile");
            GameObject target = ScenarioContext.Current.Get<GameObject>("Target Spaceship");
            WaitForCollisionBetween(missile, target);
        }

        [Then(@"The missile will explode")]
        public void ThenTheMissileWillExplode()
        {
            // TODO: not a good way to check for explosion.
            Assert.IsTrue(ScenarioContext.Current.Get<GameObject>("Missile").IsDisposed);
        }

        private static void WaitForCollisionBetween(GameObject missile, GameObject target)
        {
            RigidBodyComponent missileBody = missile.GetComponent<RigidBodyComponent>();
            RigidBodyComponent targetBody = target.GetComponent<RigidBodyComponent>();
            float timeout = 1000;
            Stopwatch s = new Stopwatch();
            s.Start();
            do
            {
                if(s.ElapsedMilliseconds > timeout)
                {
                    throw new TimeoutException("Timed out while waiting for collision");
                }
                GameUtil.SimulateTicks(1);
                // TODO: This is waiting for an explosion causing a dispose instead of a collision
                // The reason is because it is difficult to intercept a collision before it happens (i.e. will collide next frame)
            } while (!missileBody.IsDisposed);
            s.Stop();
        }
    }
}
