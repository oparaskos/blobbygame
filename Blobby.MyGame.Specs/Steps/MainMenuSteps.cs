﻿using Blobby.Core;
using Blobby.Core.Components;
using Blobby.MyGame;
using Blobby.MyGame.Specs.MockGame;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using TechTalk.SpecFlow;
using Blobby.MyGame.Factory;

namespace Blobby.MyGame.Specs
{
    [Binding]
    public class MainMenuSteps
    {
        public void InitGame()
        {
            IBlobbyGame game = new PhantomGame();
            ScenarioContext.Current.Set<IBlobbyGame>(game);
            WorldFactory wf = new WorldFactory();
            ScenarioContext.Current.Set<WorldFactory>(wf);
            RootGameObject gameObject = wf.CreateWorld(game);
            ScenarioContext.Current.Set<RootGameObject>(gameObject);
        }

        [Given(@"The player is on the '(.*)' screen")]
        public void GivenThePlayerIsOnTheScreen(string screenName)
        {
            InitGame();
            GameObject screen = FindFirstObjectWithName(screenName);
            Assert.IsNotNull(screen);
            ScenarioContext.Current.Set(screen, "currentScreen");
        }
        
        [When(@"The player clicks(?: on)?(?: the)? '(.*)'(?: button)?")]
        public void WhenThePlayerClicksOnTheButton(string clickableName)
        {
            var obj = ScenarioContext.Current.Get<GameObject>("currentScreen");
            var matchingObjects = obj.FindChildrenByName(clickableName);
            Assert.AreEqual(1, matchingObjects.Count());
            var matchingObject = matchingObjects.First();
            Assert.IsTrue(matchingObject.HasComponent<ClickableComponent>());
            matchingObject.GetComponent<ClickableComponent>().DoClick();
        }
        
        [Then(@"The '(.*)' screen should be hidden")]
        public void ThenTheScreenShouldBeHidden(string screenName)
        {
            Assert.IsFalse(FindFirstObjectWithName(screenName).Enabled);
        }
        
        [Then(@"The '(.*)'(?: object)? should have been loaded")]
        public void ThenTheObjectShouldHaveBeenLoaded(string objectName)
        {
            Assert.IsNotNull(FindFirstObjectWithName(objectName));
        }

        private GameObject FindFirstObjectWithName(string expectedName)
        {
            var rootGameObject = ScenarioContext.Current.Get<RootGameObject>();
            var matchingObjects = rootGameObject.FindChildrenByName(expectedName);
            Assert.AreEqual(1, matchingObjects.Count());
            return matchingObjects.First();
        }
    }
}
