﻿Feature: FPSCounter
	In order to know the running speed of the game
	As a game developer
	I want to be given the number of frames per second on screen

Scenario: Frames per second should be averaged over 5 frames
	Given A FPS Counter Object
	And The last 4 update calls to the object had a delta time of 0.5 seconds
	When The next update call to the object happens with a deltaTime of 1 second
	Then the result should be ≅1.66 seconds on the screen
	
Scenario: Frames per second should be averaged over n frames if there have only been n < 5 frames
	Given A FPS Counter Object
	And The last 2 update calls to the object had a delta time of 0.9 seconds
	When The next update call to the object happens with a deltaTime of 1 second
	Then the result should be ≅1.07 seconds on the screen
