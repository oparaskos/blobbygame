﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blobby.Graphics
{
    public class TextureData
    {
        public class Pixel
        {
            byte r;
            byte g;
            byte b;
            byte a;
            public Pixel(byte r, byte g, byte b, byte a)
            {
                this.r = r;
                this.g = g;
                this.b = b;
                this.a = a;
            }
        }

        public Pixel[] pixels;
    }
}
