﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Blobby.Graphics.Algorithm.MarchingSquares
{
    public static class MarchingSquaresAlgorithm
    {
        private static readonly byte tolerance = Byte.MaxValue / 2;

        /// <remarks>
        /// Based on http://www.emanueleferonato.com/2013/03/01/using-marching-squares-algorithm-to-trace-the-contour-of-an-image/ 
        /// </remarks>
        public static List<Point> MarchingSquares(this Bitmap bitmapData)
        {
            List<Point> contourVector = new List<Point>();
            // getting the starting pixel
            Point? startPixelPoint = GetStartingPixel(bitmapData);
            // if we found a starting pixel we can begin
            if (startPixelPoint != null)
            {
                Point startPoint = startPixelPoint.Value;
                contourVector.Add(startPoint);
                // pX and pY are the coordinates of the starting point
                int pX = startPoint.X;
                int pY = startPoint.Y;
                // stepX and stepY can be -1, 0 or 1 and represent the step in pixels to reach
                // next contour point
                int stepX = 0;
                int stepY = 0;
                // we also need to save the previous step, that's why we use prevX and prevY
                int prevX = 0;
                int prevY = 0;
                // closedLoop will be true once we traced the full contour
                bool closedLoop = false;
                while (!closedLoop)
                {
                    // the core of the script is getting the 2x2 square value of each pixel
                    int squareValue = GetSquareValue(bitmapData, pX, pY);
                    switch (squareValue)
                    {
                        /* going UP with these cases:

                        +---+---+   +---+---+   +---+---+
                        | 1 |   |   | 1 |   |   | 1 |   |
                        +---+---+   +---+---+   +---+---+
                        |   |   |   | 4 |   |   | 4 | 8 |
                        +---+---+  	+---+---+  	+---+---+

                        */
                        case 1:
                        case 5:
                        case 13:
                            stepX = 0;
                            stepY = -1;
                            break;
                        /* going DOWN with these cases:

                        +---+---+   +---+---+   +---+---+
                        |   |   |   |   | 2 |   | 1 | 2 |
                        +---+---+   +---+---+   +---+---+
                        |   | 8 |   |   | 8 |   |   | 8 |
                        +---+---+  	+---+---+  	+---+---+

                        */
                        case 8:
                        case 10:
                        case 11:
                            stepX = 0;
                            stepY = 1;
                            break;
                        /* going LEFT with these cases:

                        +---+---+   +---+---+   +---+---+
                        |   |   |   |   |   |   |   | 2 |
                        +---+---+   +---+---+   +---+---+
                        | 4 |   |   | 4 | 8 |   | 4 | 8 |
                        +---+---+  	+---+---+  	+---+---+

                        */
                        case 4:
                        case 12:
                        case 14:
                            stepX = -1;
                            stepY = 0;
                            break;
                        /* going RIGHT with these cases:

                        +---+---+   +---+---+   +---+---+
                        |   | 2 |   | 1 | 2 |   | 1 | 2 |
                        +---+---+   +---+---+   +---+---+
                        |   |   |   |   |   |   | 4 |   |
                        +---+---+  	+---+---+  	+---+---+

                        */
                        case 2:
                        case 3:
                        case 7:
                            stepX = 1;
                            stepY = 0;
                            break;
                        case 6:
                            /* special saddle point case 1:
							
							+---+---+ 
							|   | 2 | 
							+---+---+
							| 4 |   |
							+---+---+
							
							going LEFT if coming from UP
							else going RIGHT 
							
							*/
                            if (prevX == 0 && prevY == -1)
                            {
                                stepX = -1;
                                stepY = 0;
                            }
                            else
                            {
                                stepX = 1;
                                stepY = 0;
                            }
                            break;
                        case 9:
                            /* special saddle point case 2:

                                +---+---+ 
                                | 1 |   | 
                                +---+---+
                                |   | 8 |
                                +---+---+

                                going UP if coming from RIGHT
                                else going DOWN 

                                */
                            if (prevX == 1 && prevY == 0)
                            {
                                stepX = 0;
                                stepY = -1;
                            }
                            else
                            {
                                stepX = 0;
                                stepY = 1;
                            }
                            break;
                    }
                    // moving onto next point
                    pX += stepX;
                    pY += stepY;
                    // saving contour point
                    contourVector.Add(new Point(pX, pY));
                    prevX = stepX;
                    prevY = stepY;
                    // if we returned to the first point visited, the loop has finished
                    if (pX == startPoint.X && pY == startPoint.Y)
                    {
                        closedLoop = true;
                    }
                }
            }
            return contourVector;
        }

        private static Point? GetStartingPixel(Bitmap bitmapData)
        {
            // finding the starting pixel is a matter of brute force, we need to scan
            // the image pixel by pixel until we find a non-transparent pixel
            Point offsetPoint = new Point(0, 0);
            for (int i = 0; i < bitmapData.Height; i++)
            {
                for (int j = 0; j < bitmapData.Width; j++)
                {
                    offsetPoint.X = j;
                    offsetPoint.Y = i;
                    if (AlphaValue(bitmapData, offsetPoint) > tolerance)
                    {
                        return offsetPoint;
                    }
                }
            }
            return null;
        }

        public static byte AlphaValue(Bitmap bitmapData, Point offsetPoint)
        {
            GraphicsUnit pixels = GraphicsUnit.Pixel;
            if (bitmapData.GetBounds(ref pixels).Contains(offsetPoint.X, offsetPoint.Y))
            {
                return bitmapData.GetPixel(offsetPoint.X, offsetPoint.Y).A;
            }
            return 0;
        }

        private static int GetSquareValue(Bitmap bitmapData, int pX, int pY)
        {
            /*
			
			checking the 2x2 pixel grid, assigning these values to each pixel, if not transparent
			
			+---+---+
			| 1 | 2 |
			+---+---+
			| 4 | 8 | <- current pixel (pX,pY)
			+---+---+
			
			*/
            int squareValue = 0;
            // checking upper left pixel
            if (AlphaValue(bitmapData, new Point(pX - 1, pY - 1)) >= tolerance)
            {
                squareValue += 1;
            }
            // checking upper pixel
            if (AlphaValue(bitmapData, new Point(pX, pY - 1)) > tolerance)
            {
                squareValue += 2;
            }
            // checking left pixel
            if (AlphaValue(bitmapData, new Point(pX - 1, pY)) > tolerance)
            {
                squareValue += 4;
            }
            // checking the pixel itself
            if (AlphaValue(bitmapData, new Point(pX, pY)) > tolerance)
            {
                squareValue += 8;
            }
            return squareValue;
        }
    }
}
