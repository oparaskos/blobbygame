﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blobby.Graphics.Algorithm.VertexLoopSimplify
{
    public static class VertexLoopSimplifyAlgorithm
    {
        /// <summary>
        /// Very naive
        /// </summary>
        /// <param name="points">list of points to reduce</param>
        /// <param name="threshold">threshold by which to reduce them</param>
        /// <returns>the reduced list of points</returns>
        public static List<Point> Simplify(this List<Point> points, double threshold = Math.PI/5)
        {
            List<Point> reducedPoints = new List<Point>();
            reducedPoints.Add(points.First());
            for (int i = 1; i < points.Count; ++i)
            {
                Point prevPoint = reducedPoints.Last();
                Point thisPoint = points.ElementAt(i);
                if (i + 1 >= points.Count)
                {
                    reducedPoints.Add(points.ElementAt(i));
                }
                else
                {
                    Point nextPoint = points.ElementAt(i + 1);
                    double anglePrevNext = Math.Atan2(prevPoint.Y - nextPoint.Y, prevPoint.X - nextPoint.X);
                    double anglePrevThis = anglePrevNext - Math.Atan2(prevPoint.Y - thisPoint.Y, prevPoint.X - thisPoint.X);
                    double angleThisNext = anglePrevNext - Math.Atan2(thisPoint.Y - nextPoint.Y, thisPoint.X - nextPoint.X);
                    if (Math.Abs(angleThisNext) > threshold || Math.Abs(anglePrevThis) > threshold)
                    {
                        reducedPoints.Add(thisPoint);
                    }
                }
            }
            return reducedPoints;
        }
    }
}
