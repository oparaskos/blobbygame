using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;

namespace Blobby.Core
{

    class SceneLoader
    {
        public GameObject LoadScene(string filename)
        {
            using (StreamReader r = new StreamReader(filename))
            {
                string json = r.ReadToEnd();
                return ReadJsonGameObject(JsonConvert.DeserializeObject<JsonGameObject>(json), "root");
            }
        }

        private GameObject ReadJsonGameObject(JsonGameObject jsonGameObject, string name)
        {
            GameObject gameObject = new GameObject();
            // gameObject.Name = name; // So that later i can reference other objects by name.
            gameObject.Position = new Vector2(jsonGameObject.Position[0], jsonGameObject.Position[1]);
            gameObject.Angle = jsonGameObject.Angle;
            ReadGameObjectComponents(jsonGameObject, gameObject);
            ReadGameObjectChildren(jsonGameObject, gameObject);
            return gameObject;
        }

        private void ReadGameObjectChildren(JsonGameObject jsonGameObject, GameObject gameObject)
        {
            if (jsonGameObject.Children != null)
            {
                foreach (string childName in jsonGameObject.Children.Keys)
                {
                    JsonGameObject child = jsonGameObject.Children[childName];
                    gameObject.AddChild(ReadJsonGameObject(child, childName));
                }

            }
        }

        private void ReadGameObjectComponents(JsonGameObject jsonGameObject, GameObject gameObject)
        {
            if (jsonGameObject.Components != null)
            {
                foreach (JsonComponent component in jsonGameObject.Components)
                {
                    Assembly asm = this.GetType().Assembly;
                    if (component.AssemblyName != null)
                    {
                        asm = Assembly.LoadFrom(component.AssemblyName);
                    }
                    Type type = asm.GetType(component.TypeName);
                    if (type.IsInstanceOfType(typeof(BlobbyComponent)))
                    {
                        gameObject.AddComponent(type);
                    }
                }
            }
        }

        private class JsonComponent
        {
            public string AssemblyName { get; set; }
            public string TypeName { get; set; }
            public Dictionary<string, string> Properties { get; set; }
        }

        private class JsonGameObject
        {
            public float[] Position { get; set; }
            public float Angle { get; set; }
            public List<JsonComponent> Components { get; set; }
            public Dictionary<string, JsonGameObject> Children { get; set; }
        }
    }
}