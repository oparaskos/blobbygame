﻿using System.Collections.Generic;
using Blobby.MyGame.Components;
using Microsoft.Xna.Framework;
using Blobby.Core;

namespace Blobby.MyGame.AI
{
    internal class BehaviourList : AIBehaviour
    {
        private List<AIBehaviour> behaviourList;

        public BehaviourList(List<AIBehaviour> behaviourList)
        {
            this.behaviourList = behaviourList;
        }

        public override void Update(GameTime gameTime)
        {
            MyAIHumanoidControllerComponent ai = Owner.GetComponent<MyAIHumanoidControllerComponent>();
            foreach (AIBehaviour task in behaviourList) {
                task.Completed = false;
                ai.QueueTask(task);
            }
            ai.StartNextTask();
        }
    }
}