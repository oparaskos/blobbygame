﻿using Blobby.MyGame.Components;
using Microsoft.Xna.Framework;
using Blobby.Core;

namespace Blobby.MyGame.AI
{
    internal class ConversationBehaviour : AIBehaviour
    {
        private MyHumanoidControllerComponent characterToTalkTo;

        public override float Radius
        {
            get
            {
                return 30;
            }
        }

        public MyHumanoidControllerComponent Humanoid
        {
            get
            {
                return Owner.GetComponent<MyHumanoidControllerComponent>();
            }
        }

        public Conversation Conversation { get; set; }
        private bool hasJoined = false;

        public ConversationBehaviour(MyHumanoidControllerComponent characterToTalkTo)
        {
            this.characterToTalkTo = characterToTalkTo;
            Location = characterToTalkTo.Owner.AbsolutePosition;
        }

        public override void Update(GameTime gameTime)
        {
            if(!hasJoined)
            {
                Conversation.Join(Humanoid);
            }
            Completed = Conversation.Completed;
        }
    }
}