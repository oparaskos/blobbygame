﻿using System;
using Blobby.MyGame.Components;
using Microsoft.Xna.Framework;

namespace Blobby.MyGame.AI
{
    public class Conversation
    {
        public bool Completed;

        public delegate void HandleSpeak(Message message);
        public event HandleSpeak OnSpeak;

        private Message lastMessage = null;

        public void Update(GameTime gameTime)
        {
            // Handle update for canned conversations.
        }

        public void Join(MyHumanoidControllerComponent characterToTalkTo)
        {
            OnSpeak += characterToTalkTo.OnSpokenTo;
            if(lastMessage != null)
            {
                characterToTalkTo.OnSpokenTo(lastMessage);
            }
        }

        public void Say(MyHumanoidControllerComponent messageSender, string messageBody)
        {
            Message message = new Message();
            message.From = messageSender;
            message.Body = messageBody;
            message.conversation = this;
            OnSpeak(message);
            lastMessage = message;
        }

        public class Message
        {
            public MyHumanoidControllerComponent From { get; set; }
            public string Body { get; set; }
            public Conversation conversation { get; set; }

            public void Reply(MyHumanoidControllerComponent from, string body)
            {
                conversation.Say(from, body);
            }
        }
    }
}