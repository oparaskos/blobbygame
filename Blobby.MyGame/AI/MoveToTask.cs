﻿using Blobby.MyGame.Components;
using Microsoft.Xna.Framework;

namespace Blobby.MyGame.AI
{
    internal class MoveToTask : AIBehaviour
    {
        
        public MoveToTask(Vector2 vector2)
        {
            Location = vector2;
        }

        public override void Update(GameTime gameTime)
        {
            Completed = (Owner.AbsolutePosition - Location.Value).Length() < Radius;
        }

        public override string ToString()
        {
            return "MoveToTask " + Location;
        }
    }
}