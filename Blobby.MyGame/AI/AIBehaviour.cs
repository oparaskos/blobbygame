﻿using System;
using Blobby.Core;
using Microsoft.Xna.Framework;

namespace Blobby.MyGame.AI
{
    public abstract class AIBehaviour : IUpdateable
    {

        public event EventHandler<EventArgs> CompletedChanged;
        public event EventHandler<EventArgs> LocationChanged;
        public event EventHandler<EventArgs> OwnerChanged;
        public event EventHandler<EventArgs> EnabledChanged;
        public event EventHandler<EventArgs> UpdateOrderChanged;

        private bool _completed = false;
        private Vector2? _location = null;
        private GameObject _owner = null;
        private bool _enabled = true;
        private int _updateOrder = 0;

        public virtual bool Completed
        {
            get
            {
                return _completed;
            }
            set
            {
                _completed = value;
                CompletedChanged?.Invoke(this, null);
            }
        }

        public virtual Vector2? Location
        {
            get
            {
                return _location;
            }
            set
            {
                _location = value;
                LocationChanged?.Invoke(this, null);
            }
        }

        public GameObject Owner
        {
            get
            {
                return _owner;
            }
            set
            {
                _owner = value;
                OwnerChanged?.Invoke(this, null);
            }
        }

        public virtual bool Enabled
        {
            get
            {
                return _enabled;
            }
            set
            {
                _enabled = value;
                EnabledChanged?.Invoke(this, null);
            }
        }

        public virtual int UpdateOrder
        {
            get
            {
                return _updateOrder;
            }
            set
            {
                _updateOrder = value;
                UpdateOrderChanged?.Invoke(this, null);
            }
        }

        public virtual float Radius
        {
            get
            {
                return 10;
            }
        }

        public abstract void Update(GameTime gameTime);
    }
}