using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Blobby.Core;
using Blobby.Core.Components;
using Blobby.Core.Steamworks;
using Blobby.MyGame.Factory;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Runtime.Serialization;
using System.IO;

namespace Blobby.MyGame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class GameApplication : Game, IBlobbyGame
    {
        public ICameraComponent ActiveCamera { get; set; }
        public new IContentManager Content { get; private set; }
        public new IGraphicsDevice GraphicsDevice { get; private set; }
        public IInputManager Input { get; private set; }

        public bool Enabled { get; } = true;
        public int UpdateOrder { get; } = 0;

        public event EventHandler<EventArgs> EnabledChanged;
        public event EventHandler<EventArgs> UpdateOrderChanged;

        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;

        private GameObject world;
        private WorldFactory worldFactory;

        public GameApplication()
        {
            graphics = new GraphicsDeviceManager(this);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            Input = InitDefaultKeyboardLayout();
            GraphicsDevice = new DefaultGraphicsDevice(base.GraphicsDevice);
            Content = new DefaultContentManager(base.Content)
            {
                RootDirectory = "Content"
            };
            spriteBatch = new SpriteBatch(graphics.GraphicsDevice);
            worldFactory = new WorldFactory();
            base.Initialize();
        }

        private IInputManager InitDefaultKeyboardLayout()
        {
            DefaultInputManager input = new DefaultInputManager();
            // Quit the game
            input.RegisterGamepadButton("Exit", Buttons.Back);
            input.RegisterKeyboardKey("Exit", Keys.Escape);
            // Go forwards
            input.RegisterGamepadButton("Forward", Buttons.DPadUp);
            input.RegisterKeyboardKey("Forward", Keys.W, Keys.Up);
            // Backwards
            input.RegisterGamepadButton("Backward", Buttons.DPadDown);
            input.RegisterKeyboardKey("Backward", Keys.S, Keys.Down);
            // Turn left
            input.RegisterGamepadButton("Left", Buttons.DPadLeft);
            input.RegisterKeyboardKey("Left", Keys.A, Keys.Left);
            // Turn right
            input.RegisterGamepadButton("Right", Buttons.DPadRight);
            input.RegisterKeyboardKey("Right", Keys.D, Keys.Right);
            // Fire Torpedos
            input.RegisterGamepadButton("Fire", Buttons.RightTrigger);
            input.RegisterKeyboardKey("Fire", Keys.Space);
            // Fire phasers
            input.RegisterGamepadButton("AltFire", Buttons.RightShoulder);
            input.RegisterKeyboardKey("AltFire", Keys.E);
            // Write the world to a .json file.
            input.RegisterKeyboardKey("SNAPSHOT", Keys.P);
            return input;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            world = worldFactory.CreateWorld(this);
            CameraComponent camera = world.FindComponentOnChild<CameraComponent>();
            if (camera == null)
            {
                throw new NotSupportedException("No Camera Found");
            }
            else
            {
                camera.Active = true;
            }
            world.Initialize();
            base.LoadContent();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            base.UnloadContent();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            world.Update(gameTime);
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin(
                transformMatrix: ActiveCamera?.Transform,
                samplerState: SamplerState.PointWrap,
                sortMode: SpriteSortMode.BackToFront,
                blendState: BlendState.AlphaBlend);
            world.Draw(spriteBatch);
            spriteBatch.End();
            base.Draw(gameTime);
        }

        public static void Main(String[] args)
        {
            SteamworksSubsystem.PreInitialize(args);
            using (GameApplication game = new GameApplication())
            {
                game.Run();
            }
        }

        void IUpdateable.Update(GameTime gameTime)
        {
            Update(gameTime);
        }
    }
}