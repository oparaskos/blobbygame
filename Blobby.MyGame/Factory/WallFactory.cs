﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Blobby.Core;
using Blobby.Core.Components;

namespace Blobby.MyGame.Factory
{
    public class WallFactory
    {
        public WallFactory()
        {
        }

		public GameObject CreateWallLoop(List<Vector2> vertices, bool closedLoop = true)
		{
			GameObject wallLoop = new GameObject("Wall Loop");
			Vector2? lastPoint = null;
			foreach (Vector2 vertex in vertices)
			{
				if (lastPoint.HasValue)
				{
					wallLoop.AddChild(CreateWall(lastPoint.Value, vertex));
				}
				lastPoint = vertex;
			}
			if (closedLoop && lastPoint.HasValue)
			{
				wallLoop.AddChild(CreateWall(lastPoint.Value, vertices[0]));
			}
			return wallLoop;
		}

		public GameObject CreateWall(Vector2 startPoint, Vector2 endPoint)
		{
			GameObject container = new GameObject("Tmp_Container");
			GameObject start = new GameObject("Tmp_Start");
			var a = start.AddComponent<SpriteRendererComponent>();
			a.Texture = "UI\\Button\\Square";
			a.Layer = 70;
			start.RelativePosition = startPoint;

			GameObject end = new GameObject("Tmp_End");
			var b = end.AddComponent<SpriteRendererComponent>();
			b.Texture = "UI\\Button\\Square";
			b.Layer = 70;
			end.RelativePosition = endPoint;

			GameObject wall = new GameObject("Wall");
			wall.RelativePosition = endPoint + ((endPoint - startPoint) / 2);
			int width = (int)(startPoint - endPoint).Length();
			var c = wall.AddComponent<LineRendererComponent>();
			c.Width = width;
			c.Layer = 60;
			wall.RelativePosition = startPoint;
			wall.RelativeRotation = (float)Vector2.UnitX.Angle(endPoint - startPoint);
			wall.AddComponent<RigidBodyComponent>().Static = true;
			var r = wall.AddComponent<RectangleColliderComponent>();
			r.Rect = new Rectangle(0, 0, width, 5);

			container.AddChild(start);
			container.AddChild(end);
			container.AddChild(wall);
			return container;
		}
    }
}
