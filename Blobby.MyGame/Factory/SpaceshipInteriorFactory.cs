﻿using System;
using System.Collections.Generic;
using Blobby.Core;
using Blobby.Core.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Blobby.MyGame.Factory
{
    public class SpaceshipInteriorFactory
    {
        readonly WallFactory wallFactory;

        public SpaceshipInteriorFactory(WallFactory wallFactory)
        {
            this.wallFactory = wallFactory;
        }

        public GameObject CreateSpaceship()
        {
            GameObject spaceship = new GameObject("Spaceship");
            List<Vector2> vertices = CreateShipWalls(spaceship);
            spaceship.AddChild(CreateFloor());
            spaceship.AddChild(CreateConsole());
            return spaceship;
        }

        private GameObject CreateConsole()
        {
            GameObject console = new GameObject("Console");
            console.RelativePosition = new Vector2(0, 20);
            console.AddComponent<SpriteRendererComponent>().Texture = "Graphics\\Objects\\console_w";
            console.AddComponent<RigidBodyComponent>().Static = true;
            console.AddComponent<CircleColliderComponent>().Radius = 20;
            console.AddComponent<ClickableComponent>().OnClick += (object sender, EventArgs args) => System.Console.WriteLine("Console Clicked!"); ;
            return console;
        }

        private List<Vector2> CreateShipWalls(GameObject worldRoot)
		{
            List<Vector2> vertices = new List<Vector2>();
			// TODO: Load from a file
			vertices.Add(new Vector2(210.8125f - 250, 42.26836f - 340) * 2);
			vertices.Add(new Vector2(196.34375f - 250, 104.95586f - 340) * 2);
			vertices.Add(new Vector2(190.21875f - 250, 153.04961f - 340) * 2);
			vertices.Add(new Vector2(181.34375f - 250, 153.04961f - 340) * 2);
			vertices.Add(new Vector2(181.34375f - 250, 117.92461f - 340) * 2);
			vertices.Add(new Vector2(163.625f - 250, 118.26836f - 340) * 2);
			vertices.Add(new Vector2(127.5f - 250, 161.54961f - 340) * 2);
			vertices.Add(new Vector2(93.75f - 250, 232.45586f - 340) * 2);
			vertices.Add(new Vector2(78.75f - 250, 302.01836f - 340) * 2);
			vertices.Add(new Vector2(78.0625f - 250, 378.01836f - 340) * 2);
			vertices.Add(new Vector2(68.1875f - 250, 378.01836f - 340) * 2);
			vertices.Add(new Vector2(68.53125f - 250, 317.70586f - 340) * 2);
			vertices.Add(new Vector2(43.625f - 250, 318.70586f - 340) * 2);
			vertices.Add(new Vector2(21.46875f - 250, 331.33086f - 340) * 2);
			vertices.Add(new Vector2(5.4375f - 250, 353.14336f - 340) * 2);
			vertices.Add(new Vector2(0.34375f - 250, 378.70586f - 340) * 2);
			vertices.Add(new Vector2(0.0f - 250, 528.7059f - 340) * 2);
			vertices.Add(new Vector2(7.15625f - 250, 543.3621f - 340) * 2);
			vertices.Add(new Vector2(23.1875f - 250, 551.5496f - 340) * 2);
			vertices.Add(new Vector2(68.53125f - 250, 552.2371f - 340) * 2);
			vertices.Add(new Vector2(68.53125f - 250, 508.2684f - 340) * 2);
			vertices.Add(new Vector2(121.6875f - 250, 508.6121f - 340) * 2);
			vertices.Add(new Vector2(121.34375f - 250, 535.1746f - 340) * 2);
			vertices.Add(new Vector2(129.1875f - 250, 542.6746f - 340) * 2);
			vertices.Add(new Vector2(172.5f - 250, 542.6746f - 340) * 2);
			vertices.Add(new Vector2(181.34375f - 250, 535.1746f - 340) * 2);
			vertices.Add(new Vector2(180.6875f - 250, 516.4246f - 340) * 2);
			vertices.Add(new Vector2(199.4375f - 250, 516.4246f - 340) * 2);
			vertices.Add(new Vector2(199.4375f - 250, 490.17461f - 340) * 2);
			vertices.Add(new Vector2(207.59375f - 250, 490.17461f - 340) * 2);
			vertices.Add(new Vector2(207.9375f - 250, 508.6121f - 340) * 2);
			vertices.Add(new Vector2(212.375f - 250, 517.7996f - 340) * 2);
			vertices.Add(new Vector2(224.09375f - 250, 525.4871f - 340) * 2);
			vertices.Add(new Vector2(260.5625f - 250, 525.7684f - 340) * 2);
			vertices.Add(new Vector2(272.49995f - 250, 517.9559f - 340) * 2);
			vertices.Add(new Vector2(276.93745f - 250, 508.7372f - 340) * 2);
			vertices.Add(new Vector2(277.2812f - 250, 490.33091f - 340) * 2);
			vertices.Add(new Vector2(285.43745f - 250, 490.33091f - 340) * 2);
			vertices.Add(new Vector2(285.43745f - 250, 516.5809f - 340) * 2);
			vertices.Add(new Vector2(304.18745f - 250, 516.5809f - 340) * 2);
			vertices.Add(new Vector2(303.5312f - 250, 535.3309f - 340) * 2);
			vertices.Add(new Vector2(312.37495f - 250, 542.8309f - 340) * 2);
			vertices.Add(new Vector2(355.68745f - 250, 542.8309f - 340) * 2);
			vertices.Add(new Vector2(363.5312f - 250, 535.3309f - 340) * 2);
			vertices.Add(new Vector2(363.18745f - 250, 508.7372f - 340) * 2);
			vertices.Add(new Vector2(416.3437f - 250, 508.3934f - 340) * 2);
			vertices.Add(new Vector2(416.3437f - 250, 552.3622f - 340) * 2);
			vertices.Add(new Vector2(461.68745f - 250, 551.7059f - 340) * 2);
			vertices.Add(new Vector2(477.7187f - 250, 543.5184f - 340) * 2);
			vertices.Add(new Vector2(484.87495f - 250, 528.8622f - 340) * 2);
			vertices.Add(new Vector2(484.5312f - 250, 378.86216f - 340) * 2);
			vertices.Add(new Vector2(479.43745f - 250, 353.29966f - 340) * 2);
			vertices.Add(new Vector2(463.40625f - 250, 331.45586f - 340) * 2);
			vertices.Add(new Vector2(441.25f - 250, 318.86211f - 340) * 2);
			vertices.Add(new Vector2(416.34375f - 250, 317.83086f - 340) * 2);
			vertices.Add(new Vector2(416.6875f - 250, 378.17461f - 340) * 2);
			vertices.Add(new Vector2(406.8125f - 250, 378.17461f - 340) * 2);
			vertices.Add(new Vector2(406.125f - 250, 302.14336f - 340) * 2);
			vertices.Add(new Vector2(391.125f - 250, 232.61211f - 340) * 2);
			vertices.Add(new Vector2(357.375f - 250, 161.70586f - 340) * 2);
			vertices.Add(new Vector2(321.25f - 250, 118.39336f - 340) * 2);
			vertices.Add(new Vector2(303.53125f - 250, 118.04961f - 340) * 2);
			vertices.Add(new Vector2(303.53125f - 250, 153.17461f - 340) * 2);
			vertices.Add(new Vector2(294.65625f - 250, 153.17461f - 340) * 2);
			vertices.Add(new Vector2(288.53125f - 250, 105.11211f - 340) * 2);
			vertices.Add(new Vector2(276.42014f - 250, 42.79799f - 340) * 2);
			worldRoot.AddChild(wallFactory.CreateWallLoop(vertices));
			return vertices;
		}

		public GameObject CreateFloor()
		{
			GameObject floor = new GameObject("Floor");
			var s = floor.AddComponent<SpriteRendererComponent>();
			s.Texture = "Graphics\\ship_floor";
			s.Layer = 1;
			s.Scale = 2.0f;
			floor.RelativePosition = new Vector2(-20, -92.5f);
			return floor;
		}

	}
}
