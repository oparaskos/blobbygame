﻿﻿﻿using Blobby.Core;
using Microsoft.Xna.Framework;
using Blobby.Core.Components;
using Blobby.Core.Physics;

namespace Blobby.MyGame.Factory
{
    public class TopDownGameFactory : IGameFactory
	{
		private PlayerFactory playerFactory;
        private WallFactory wallFactory;
        private SpaceshipInteriorFactory spaceshipInteriorFactory;
        private MouseFactory mouseFactory;
        private CameraFactory cameraFactory;

		public TopDownGameFactory()
		{
			playerFactory = new PlayerFactory();
            wallFactory = new WallFactory();
            spaceshipInteriorFactory = new SpaceshipInteriorFactory(wallFactory);
            mouseFactory = new MouseFactory();
            cameraFactory = new CameraFactory(mouseFactory);
		}

		public GameObject CreateGame(IBlobbyGame game)
        {
            GameObject worldRoot = new GameObject();
            worldRoot.AddComponent<ScrollingBackgroundComponent>();
            SpriteRendererComponent sp = worldRoot.AddComponent<SpriteRendererComponent>();
            sp.Texture = "Graphics\\space1";
            sp.Layer = -100;
            // Create an NPC.
            worldRoot.AddChild(playerFactory.CreateNPC(new Vector2(20, 20)));
            // Create the player character
            GameObject player = playerFactory.CreatePlayer(Vector2.Zero);
			worldRoot.AddChild(player);
			worldRoot.AddChild(cameraFactory.CreateFollowingCamera(game, player));
			worldRoot.AddChild(spaceshipInteriorFactory.CreateSpaceship());
            return worldRoot;
        }
    }
}
