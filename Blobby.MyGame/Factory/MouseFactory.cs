﻿using System;
using Blobby.Core;
using Blobby.Core.Components;
using Blobby.MyGame.Components;
using Microsoft.Xna.Framework;

namespace Blobby.MyGame.Factory
{
    public class MouseFactory
    {
        public MouseFactory()
        {
		}

		public GameObject CreateMouse()
		{
			GameObject mouse = new GameObject();
			mouse.AddComponent<MyMouseCursorComponent>();
			TextRendererComponent trc = mouse.AddComponent<TextRendererComponent>();
			trc.Offset = new Vector2(10, 5);
			mouse.AddComponent<MyShipPositionComponent>();
			SpriteRendererComponent src = mouse.AddComponent<SpriteRendererComponent>();
			src.Texture = "UI\\Pointer";
			src.Layer = 100;
			return mouse;
		}
    }
}
