﻿using System;
using Blobby.Core;
using Blobby.Core.Components;

namespace Blobby.MyGame.Factory
{
    internal class StarbaseFactory
    {
        public StarbaseFactory()
        {
        }

        internal GameObject CreateStarbase(IBlobbyGame game)
        {
            GameObject starbase = new GameObject("Starbase");
            starbase.AddComponent<RigidBodyComponent>();
            starbase.AddComponent<CircleColliderComponent>().Radius = 400;
            SpriteRendererComponent sp = starbase.AddComponent<SpriteRendererComponent>();
            sp.Texture = "Graphics\\starbase";
            sp.Layer = 50;
            return starbase;
        }
    }
}