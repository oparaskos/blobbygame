using Blobby.Core;
using Microsoft.Xna.Framework;
using Blobby.Core.Components;
using Blobby.MyGame.Components;

namespace Blobby.MyGame.Factory
{
    class LaserBeamFactory
    {
        public GameObject CreateLaserBeam()
        {
            GameObject beam = new GameObject("Beam");
            beam.RelativePosition = new Vector2(0, -520);
            beam.AddComponent<TriggerComponent>();
            beam.AddComponent<RectangleColliderComponent>().Rect = new Rectangle(-10, 10, 10, 1000);
            beam.AddComponent<MyBeamControllerComponent>();
            SpriteRendererComponent spriteRenderer
                = beam.AddComponent<SpriteRendererComponent>();
            spriteRenderer.Visible = false;
            spriteRenderer.Texture = "Graphics\\beam";
            return beam;
        }
    }
}