﻿﻿using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Blobby.Core.Components;
using Blobby.MyGame.Components;
using Blobby.Core.Physics;

namespace Blobby.MyGame.Factory
{
    public class MainGameFactory : IGameFactory
	{
		private SpaceshipFactory spaceshipFactory;
		private PlanetFactory planetFactory;
        private StarbaseFactory starbaseFactory;

        public MainGameFactory()
		{
			spaceshipFactory = new SpaceshipFactory();
			planetFactory = new PlanetFactory();
            starbaseFactory = new StarbaseFactory();
        }

		public GameObject CreateGame(IBlobbyGame game)
		{
			PhysicsSubsystem physics = new PhysicsSubsystem();
			GameObject worldRoot = new GameObject("Main Game");
			worldRoot.AddComponent<ScrollingBackgroundComponent>();
			SpriteRendererComponent sp = worldRoot.AddComponent<SpriteRendererComponent>();
			sp.Texture = "Graphics\\space1";
			sp.Layer = -100;
			// Create the spaceship
			GameObject playerShip = spaceshipFactory.CreateSpaceship(Vector2.Zero);
            playerShip.Name = "Player Spaceship";
            playerShip.AddComponent<MyPlayerControllerComponent>();
            worldRoot.AddChild(playerShip);
			// Create the spaceship
			GameObject enemyShip = spaceshipFactory.CreateSpaceship(new Vector2(100, 0));
            enemyShip.AddComponent<MyEnemyControllerComponent>().Following = playerShip;
            worldRoot.AddChild(enemyShip);
			worldRoot.AddChild(CreateCamera(game, playerShip));
            GameObject planet = planetFactory.CreatePlanet(game);
            worldRoot.AddChild(planet);
            GameObject starbase = starbaseFactory.CreateStarbase(game);
            starbase.RelativePosition = new Vector2(1024, 1024);
            worldRoot.AddChild(starbase);
            planet.AddChild(CreateSoundSource(game));
			return worldRoot;
		}
        
		public GameObject CreateMouse(IBlobbyGame game)
		{
			GameObject mouse = new GameObject();
			mouse.AddComponent<MyMouseCursorComponent>();
			TextRendererComponent trc = mouse.AddComponent<TextRendererComponent>();
			trc.Offset = new Vector2(10, 5);
			mouse.AddComponent<MyShipPositionComponent>();
			SpriteRendererComponent src = mouse.AddComponent<SpriteRendererComponent>();
			src.Texture = "UI\\Pointer";
			src.Layer = 100;
			return mouse;
		}

		public GameObject CreateSoundSource(IBlobbyGame game)
		{
			GameObject soundSource = new GameObject();
			AudioEmitterComponent aec = soundSource.AddComponent<AudioEmitterComponent>();
			aec.PlayOnStart = true;
			return soundSource;
		}

		public GameObject CreateCamera(IBlobbyGame game, GameObject spaceship)
		{
			GameObject cameraObject = new GameObject();
			cameraObject.AddComponent<FollowingCameraComponent>().Target = spaceship;
			cameraObject.AddComponent<CameraComponent>();
			cameraObject.AddChild(CreatePositionIndicator());
			cameraObject.AddChild(CreateHealthIndicator());
            cameraObject.AddChild(CreateMouse(game));
            cameraObject.AddChild(CreateSpeedIndicator(spaceship));
            cameraObject.AddChild(CreateEnergyIndicator(spaceship));
            return cameraObject;
		}

        public GameObject CreateHealthIndicator()
        {
            GameObject display = new GameObject();
            display.RelativePosition = new Vector2(250, 180);
            display.AddComponent<DialogWindowComponent>()
                .Bounds = new Rectangle(-10, -9, 150, 25);
            display.AddComponent<TextRendererComponent>();
            display.AddComponent<MyShipShieldHealthComponent>();
            return display;
        }

        public GameObject CreatePositionIndicator()
		{
			GameObject display = new GameObject();
			display.RelativePosition = new Vector2(250, 210);
			display.AddComponent<DialogWindowComponent>()
				.Bounds = new Rectangle(-10, -9, 150, 25);
			display.AddComponent<TextRendererComponent>();
			display.AddComponent<MyShipPositionComponent>();
			return display;
		}

		public GameObject CreateSpeedIndicator(GameObject spaceship)
		{
			GameObject display = new GameObject();
			TextRendererComponent label = display.AddComponent<TextRendererComponent>();
			label.Text = "Speed";
			label.Offset = new Vector2(-50, -7);
			display.RelativePosition = new Vector2(-330, 230);
			display.AddComponent<MySpeedToProgressBarComponent>().Target = spaceship;
			display.AddComponent<ProgressBarComponent>()
				.MaxBounds = new Rectangle(-10, -9, 150, 10);
			return display;
		}

        public GameObject CreateEnergyIndicator(GameObject spaceship)
        {
            GameObject display = new GameObject();
            TextRendererComponent label = display.AddComponent<TextRendererComponent>();
            label.Text = "Energy";
            label.Offset = new Vector2(-50, -7);
            display.RelativePosition = new Vector2(-330, 210);
            display.AddComponent<MyEnergyProgressBarComponent>().Target = spaceship;
            ProgressBarComponent progress = display.AddComponent<ProgressBarComponent>();
            progress.MaxBounds = new Rectangle(-10, -9, 150, 10);
            progress.Color = Color.Orange;
            return display;
        }
    }
}
