﻿using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Blobby.Core.Components;
using Blobby.MyGame.Components;
using Blobby.Core.Physics;

namespace Blobby.MyGame.Factory
{
    public class ButtonFactory
	{

		public GameObject CreateButton(string text)
		{
			GameObject display = new GameObject(text + " Button");
			int w = 250;
			int h = 50;
			Rectangle bounds = new Rectangle(-(w / 2), -(h / 2), w, h);
			display.AddComponent<DialogWindowComponent>()
				   .Bounds = bounds;
			TextRendererComponent t = display.AddComponent<TextRendererComponent>();
			t.VerticalAlignment = VerticalAlignment.Center;
			t.HorizontalAlignment = Alignment.Center;
			t.Text = text;
			t.Scale = 3;
			display.AddComponent<ButtonComponent>();
			display.AddComponent<ClickableComponent>();
			display.AddComponent<RectangleColliderComponent>().Rect = bounds;
			return display;
		}

		public GameObject CreateColouredRectangleButton(string text, Rectangle? bounds)
		{
			GameObject display = new GameObject(text + " Button");
			int w = 250;
			int h = 50;
            if (bounds == null)
            {
                bounds = new Rectangle(-(w / 2), -(h / 2), w, h);
            }
			display.AddComponent<ColouredRectangleComponent>()
                   .Bounds = bounds.Value;
			TextRendererComponent t = display.AddComponent<TextRendererComponent>();
			t.VerticalAlignment = VerticalAlignment.Center;
			t.HorizontalAlignment = Alignment.Center;
			t.Text = text;
			t.Scale = 3;
			display.AddComponent<ButtonComponent>();
            display.AddComponent<RectangleColliderComponent>().Rect = bounds.Value;
			return display;
		}
    }
}
