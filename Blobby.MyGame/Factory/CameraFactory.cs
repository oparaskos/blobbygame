﻿using System;
using Microsoft.Xna.Framework;
using Blobby.Core;
using Blobby.Core.Components;

namespace Blobby.MyGame.Factory
{
    public class CameraFactory
    {
        readonly MouseFactory mouseFactory;

        public CameraFactory(MouseFactory mouseFactory)
        {
            this.mouseFactory = mouseFactory;
        }

		public GameObject CreateFollowingCamera(IBlobbyGame game, GameObject player)
		{
			GameObject cameraObject = new GameObject();
			cameraObject.AddComponent<FollowingCameraComponent>().Target = player;
			cameraObject.AddComponent<CameraComponent>();
			cameraObject.AddChild(mouseFactory.CreateMouse());
			return cameraObject;
		}
    }
}
