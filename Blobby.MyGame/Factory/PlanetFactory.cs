using System;
using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Blobby.Core.Components;
using Blobby.MyGame.Components;

namespace Blobby.MyGame.Factory
{
    class PlanetFactory
    {
        private Random rnd = new Random();

        public GameObject CreatePlanet(IBlobbyGame game)
        {
            GameObject planet = new GameObject();
            planet.RelativePosition = new Vector2(-1000,0);//rnd.Next(-100, 100), rnd.Next(-100, 100));
            SpriteRendererComponent sc = planet.AddComponent<SpriteRendererComponent>();
            int planettype =  rnd.Next(1, 2);
            sc.Texture = "Graphics/Planets/" + planettype;
            sc.Layer = -90;
			sc.Scale = 3f;
			planet.AddComponent<TriggerComponent>();
            planet.AddComponent<MyOrbitControllerComponent>();
            planet.AddComponent<CircleColliderComponent>().Radius = 400;
            return planet;
        }
    }
}