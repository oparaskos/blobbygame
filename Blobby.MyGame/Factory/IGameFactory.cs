﻿using Blobby.Core;

namespace Blobby.MyGame.Factory
{
    public interface IGameFactory
    {
        GameObject CreateGame(IBlobbyGame game);
    }
}