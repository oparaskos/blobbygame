using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Blobby.Core.Components;
using Blobby.MyGame.Components;

namespace Blobby.MyGame.Factory
{
    class TorpedoFactory
    {
        public GameObject CreateTorpedo(Vector2 position, Vector2 velocity, IBlobbyGame game)
        {
            GameObject spaceship = new GameObject("Torpedo");
            spaceship.RelativePosition = position;
            spaceship.AddComponent<RigidBodyComponent>().Velocity = velocity;
            spaceship.AddComponent<CircleColliderComponent>().Radius = 3;
            spaceship.AddComponent<MyTorpedoControllerComponent>();
            SpriteRendererComponent spriteRenderer
                = spaceship.AddComponent<SpriteRendererComponent>();
            spriteRenderer.Texture = "Graphics/Torpedo";
            return spaceship;
        }
    }
}