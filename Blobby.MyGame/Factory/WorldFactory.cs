using Blobby.Core;
using Blobby.Core.Components;
using Blobby.Core.Physics;
using Blobby.Core.Steamworks;

namespace Blobby.MyGame.Factory
{
    public class WorldFactory
	{
		IBlobbyGame game;
        RootGameObject worldRoot;
        GameObject menuRoot;

        TopDownGameFactory topDownGameFactory;
		MainGameFactory mainGameFactory;
		ShipCreatorGameFactory shipCreatorGameFactory;
		MainMenuFactory mainMenuFactory;

        public WorldFactory()
		{
            topDownGameFactory = new TopDownGameFactory();
			mainGameFactory = new MainGameFactory();
			mainMenuFactory = new MainMenuFactory();
            shipCreatorGameFactory = new ShipCreatorGameFactory();
        }

        public RootGameObject CreateWorld(IBlobbyGame game)
		{
			this.game = game;
			worldRoot = new RootGameObject();
            worldRoot.AddSubsystem<SteamworksSubsystem>();
			worldRoot.AddSubsystem<PhysicsSubsystem>();
            worldRoot.AddComponent<ObjectDumpComponent>();
            worldRoot.SetGame(game);
            menuRoot = mainMenuFactory
                .CreateMenu(game);
            worldRoot.AddChild(menuRoot);
            return worldRoot;
		}
    }
}
