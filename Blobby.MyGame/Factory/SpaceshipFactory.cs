using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Blobby.Core.Components;
using Blobby.MyGame.Components;
using Blobby.Core.Physics;
using System;

namespace Blobby.MyGame.Factory
{
    public class SpaceshipFactory
    {
        private LaserBeamFactory laserBeamFactory = new LaserBeamFactory();

        public GameObject CreateSpaceship(Vector2 position)
        {
			GameObject spaceship = new GameObject("Spaceship");
            spaceship.RelativePosition = position;
			spaceship.AddComponent<RigidBodyComponent>();
            spaceship.AddComponent<CircleColliderComponent>().Radius = 40;
            spaceship.AddComponent<MySpaceshipControllerComponent>();
            spaceship.AddComponent<MyEnergySourceComponent>();
            GameObject beam = laserBeamFactory.CreateLaserBeam();
            spaceship.AddChild(beam);
            spaceship.AddChild(CreateShield("Graphics\\spr_shield"));
            spaceship.AddChild(CreateHull("Graphics\\spaceship"));
            spaceship.AddChild(CreateThruster("Graphics\\thruster_boost", "Graphics\\thruster_boost", true));
            spaceship.AddChild(CreateThruster("Graphics\\thruster_boost", "Graphics\\thruster_boost", false));
            spaceship.AddChild(CreateHealthIndicator());
            return spaceship;
        }

        private GameObject CreateHealthIndicator()
        {
            GameObject obj = new GameObject("Health Indicator");
            obj.RelativePosition = new Vector2(0, -50);
            obj.AddComponent<MyHealthIndicatorComponent>();
            ProgressBarComponent bar = obj.AddComponent<ProgressBarComponent>();
            bar.ResizableRectangleSprite = CreateHealthBarBackgroundSprite();
            bar.MaxBounds = new Rectangle(-45, 0, 90, 10);
            return obj;
        }

        private ResizableRectangleSprite CreateHealthBarBackgroundSprite()
        {
            ResizableRectangleSprite  sprite = new ResizableRectangleSprite();
            // TODO: retrun an implementation of IResizableRectangleSprite which takes 1 image
            return sprite;
        }

        private GameObject CreateHull(string texture) {
            GameObject obj = new GameObject("Spaceship Hull");
            SpriteRendererComponent sp = obj.AddComponent<SpriteRendererComponent>();
            sp.Texture = texture;
            sp.Layer = 55;
            return obj;
		}

		private GameObject CreateThruster(string thrusterTexture, string thrusterTextureOff, bool isLeft)
		{
			GameObject thruster = new GameObject("Thruster");
			SpriteRendererComponent sp = thruster.AddComponent<SpriteRendererComponent>();
			sp.Texture = thrusterTextureOff;
			sp.Layer = 50;
			if (isLeft)
			{
				thruster.RelativePosition = new Vector2(-10, 33);
			}
			else
			{
				thruster.RelativePosition = new Vector2(10, 33);
			}
			return thruster;
		}

        private GameObject CreateShield(string shieldTexture) {
			GameObject shield = new GameObject("Shield");
			shield.AddComponent<MyShieldControllerComponent>();
            SpriteRendererComponent sp = shield.AddComponent<SpriteRendererComponent>();
			sp.Texture = shieldTexture;
            sp.Colour = Color.Green;
			sp.Layer = 60;
			sp.Scale = 0.09f;
            sp.Alpha = 0.1f;
            return shield;
        }
    }
}