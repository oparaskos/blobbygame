using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Blobby.Core.Components;
using Blobby.MyGame.Components;
using Blobby.Core.Physics;

namespace Blobby.MyGame.Factory
{
    class ExplosionFactory
    {
        public PhysicsSubsystem Physics { get; set; }
        public GameObject CreateExplosion(IBlobbyGame game, Vector2 position)
        {
            GameObject explosion = new GameObject("Explosion");
            explosion.RelativePosition = position;
            explosion.AddComponent<MyExplosionControllerComponent>();
            AnimatedSpriteRendererComponent spriteRenderer
                = explosion.AddComponent<AnimatedSpriteRendererComponent>();
            spriteRenderer.AnimationDuration = 500;
            spriteRenderer.Spritesheet = new Spritesheet(game.Content.Load<Texture2D>("Graphics/explosion"), numFrames: 6);
            return explosion;
        }
    }
}