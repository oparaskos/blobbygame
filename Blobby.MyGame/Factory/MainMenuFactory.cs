﻿﻿using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Blobby.Core.Components;
using Blobby.MyGame.Components;
using Blobby.Core.Physics;

namespace Blobby.MyGame.Factory
{
    public class MainMenuFactory
    {
        ButtonFactory buttonFactory;

        public MainMenuFactory()
        {
            buttonFactory = new ButtonFactory();
        }

        public GameObject CreateMenu(
			IBlobbyGame game)
        {
            GameObject gameRoot = new GameObject("Game");
            var mainMenu = gameRoot.AddComponent<MyMainMenuComponent>();
            gameRoot.AddComponent<CameraComponent>();

            GameObject menuRoot = new GameObject("Main Menu");
            menuRoot.AddChild(CreateStartGameButton(game, mainMenu.OnStartGame));
            menuRoot.AddChild(CreateSecondButton(game, mainMenu.OnSecondButton));
            menuRoot.AddChild(CreateStartShipCreatorButton(game, mainMenu.OnStartShipCreator));
            menuRoot.AddChild(CreateQuitGameButton(game, mainMenu.OnQuit));
            menuRoot.AddChild(CreateUserNameText(game));
            menuRoot.AddChild(CreateMouse(game));

            gameRoot.AddChild(menuRoot);
            return gameRoot;
        }

        public GameObject CreateMouse(IBlobbyGame game)
        {
            GameObject mouse = new GameObject("Mouse");
            mouse.AddComponent<MyMouseCursorComponent>();
            SpriteRendererComponent src = mouse.AddComponent<SpriteRendererComponent>();
            src.Texture = "UI\\Pointer";
            src.Layer = 100;
            return mouse;
		}

		public GameObject CreateStartGameButton(IBlobbyGame game, ClickableComponent.OnClickEventHandler onClick)
		{
			GameObject display = buttonFactory.CreateButton("Start Game");
			display.GetComponent<ClickableComponent>().OnClick += onClick;
			display.RelativePosition = new Vector2(0, -90);
			return display;
		}

		public GameObject CreateSecondButton(IBlobbyGame game, ClickableComponent.OnClickEventHandler onClick)
		{
			GameObject display = buttonFactory.CreateButton("Button II");
			display.GetComponent<ClickableComponent>().OnClick += onClick;
			display.RelativePosition = new Vector2(0, -30);
			return display;
		}

		public GameObject CreateStartShipCreatorButton(IBlobbyGame game, ClickableComponent.OnClickEventHandler onClick)
		{
			GameObject display = buttonFactory.CreateButton("Ship Creator");
			display.GetComponent<ClickableComponent>().OnClick += onClick;
			display.RelativePosition = new Vector2(0, 30);
			return display;
        }

        public GameObject CreateQuitGameButton(IBlobbyGame game, ClickableComponent.OnClickEventHandler onClick)
        {
            GameObject display = buttonFactory.CreateButton("Quit Game");
            display.GetComponent<ClickableComponent>().OnClick += onClick;
            display.RelativePosition = new Vector2(0, 90);
            return display;
        }

        public GameObject CreateUserNameText(IBlobbyGame game)
        {
            GameObject username = new GameObject("UserName");
            username.AddComponent<TextRendererComponent>().Text = "Not logged-in to Steam";
            username.AddComponent<MyUsernameTextComponent>();
            username.RelativePosition = new Vector2((game.GraphicsDevice.Viewport.Width/ 2) - 100, (-game.GraphicsDevice.Viewport.Height /2) + 20);
            return username;
        }
    }
}
