﻿using System;
using Blobby.Core;
using Blobby.Core.Components;
using Microsoft.Xna.Framework;

namespace Blobby.MyGame.Factory
{
    public class SliderFactory
    {
        public SliderFactory()
        {
        }

        int buttonWidth = 18;
        int labelWidth = 20;

        public GameObject CreateSlider(
            SliderButtonComponent.OnValueChangedEventHandler onValueChanged,
            int sliderWidth = 90)
        {
			GameObject slider = new GameObject("Slider");
            GameObject l = CreateLabel(sliderWidth, onValueChanged);
            SliderButtonComponent s = l.GetComponent<SliderButtonComponent>();
			slider.AddChild(l);
            slider.AddChild(CreateDecrementButton(sliderWidth, s));
            slider.AddChild(CreateIncrementButton(sliderWidth, s));
            slider.AddChild(CreateSliderBar(sliderWidth));
            return slider;
		}

        public GameObject CreateSliderBar(int sliderWidth)
		{
			int left = -sliderWidth / 2;
			left += buttonWidth / 2;
            GameObject sliderBar = new GameObject("Slider Bar");
            LineRendererComponent line = sliderBar.AddComponent<LineRendererComponent>();
            line.Width = sliderWidth - buttonWidth;
            sliderBar.RelativePosition = new Vector2(left, 0);
            return sliderBar;
        }

        public GameObject CreateLabel(
            int sliderWidth, 
            SliderButtonComponent.OnValueChangedEventHandler onValueChanged)
        {
            int left = -sliderWidth / 2;
            left += buttonWidth / 2;
			left += labelWidth / 2;
            int right = left + sliderWidth - buttonWidth - labelWidth;
			GameObject spriteButton = new GameObject("SpriteButton");
			spriteButton.RelativePosition = new Vector2(0, 0);
            spriteButton.AddComponent<ClickableComponent>();
            spriteButton.AddComponent<ButtonComponent>();
            SliderButtonComponent sliderButton
				= spriteButton.AddComponent<SliderButtonComponent>();
			sliderButton.LowerBound = left;
			sliderButton.UpperBound = right;
			sliderButton.Value = 0.75f;
            sliderButton.OnValueChanged += onValueChanged;
			RectangleColliderComponent rect 
                = spriteButton.AddComponent<RectangleColliderComponent>();
            rect.Rect = new Rectangle(-buttonWidth / 2, -buttonWidth / 2, buttonWidth, buttonWidth);
            SpriteRendererComponent spriteRendererComponent
                = spriteButton.AddComponent<SpriteRendererComponent>();
            spriteRendererComponent.Texture = "UI/Button/HalfButton_Mid";
            spriteRendererComponent.Layer = 100;
			return spriteButton;
        }

        public GameObject CreateDecrementButton(int sliderWidth, SliderButtonComponent l)
        {
			GameObject btn = CreateSpriteButton("UI/Button/HalfButton_Left", (object sender, EventArgs args) =>
			{
                l.Value -= 0.1f;
			});
			btn.RelativePosition = new Vector2(-sliderWidth/2, 0);
			return btn;
        }

		public GameObject CreateIncrementButton(int sliderWidth, SliderButtonComponent l)
		{
            GameObject btn = CreateSpriteButton("UI/Button/HalfButton_Right", (object sender, EventArgs args) => 
            {
				l.Value += 0.1f;
            });
            btn.RelativePosition = new Vector2(sliderWidth/2, 0);
            return btn;
        }

		public GameObject CreateSpriteButton(string texture, ClickableComponent.OnClickEventHandler onClick)
		{
            GameObject spriteButton = new GameObject("SpriteButton");
            SpriteRendererComponent spriteRendererComponent = spriteButton.AddComponent<SpriteRendererComponent>();
            spriteRendererComponent.Texture = texture;
			spriteRendererComponent.Layer = 100;
            spriteButton.AddComponent<ButtonComponent>();
			spriteButton.AddComponent<ClickableComponent>().OnClick += onClick;
			RectangleColliderComponent rect
				= spriteButton.AddComponent<RectangleColliderComponent>();
            rect.Rect = new Rectangle(-buttonWidth/2, -buttonWidth / 2, buttonWidth, buttonWidth);
            return spriteButton;
        }
    }
}
