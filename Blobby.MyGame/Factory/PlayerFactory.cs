﻿using System;
using Microsoft.Xna.Framework;
using Blobby.Core;
using Blobby.MyGame.Components;
using Blobby.MyGame.AI;
using Blobby.Core.Components;
using System.Collections.Generic;

namespace Blobby.MyGame.Factory
{
    public class PlayerFactory
    {
        public PlayerFactory()
        {
        }

        public GameObject CreatePlayer(Vector2 position)
        {
            GameObject player = new GameObject("Player");
            player.AddComponent<ClickableComponent>();
            player.AddChild(CreateChatBox());
            player.AddComponent<RigidBodyComponent>();
            player.AddComponent<CircleColliderComponent>();
            player.AddComponent<MyPlayerHumanoidControllerComponent>();
            player.AddComponent<MyHumanoidControllerComponent>();
            LPCAnimatedSpriteRendererComponent s = player.AddComponent<LPCAnimatedSpriteRendererComponent>();
            s.Layer = 0.1f;
            s.CurrentAction = "death";
            ActionSpritesheetComponent a = player.AddComponent<ActionSpritesheetComponent>();
            LoadLPCSpritesheet(a, "Graphics\\Characters\\Example1\\spritesheet");
            return player;
        }

        public GameObject CreateNPC(Vector2 position)
        {
            GameObject player = new GameObject("Non-Player Character");
            player.AddComponent<ClickableComponent>();
            player.AddComponent<RigidBodyComponent>();
            player.AddComponent<CircleColliderComponent>();
            player.AddChild(CreateChatBox());
            MyAIHumanoidControllerComponent ai =player.AddComponent<MyAIHumanoidControllerComponent>();
            List<AIBehaviour> behaviourList = new List<AIBehaviour>();
            behaviourList.Add(new MoveToTask(new Vector2(130, -130)));
            behaviourList.Add(new MoveToTask(new Vector2(130, 130)));
            behaviourList.Add(new MoveToTask(new Vector2(-130, 130)));
            behaviourList.Add(new MoveToTask(new Vector2(-130, -130)));
            behaviourList.Add(new BehaviourList(behaviourList));
            ai.QueueTask(new BehaviourList(behaviourList));
            MyHumanoidControllerComponent pawn = player.AddComponent<MyHumanoidControllerComponent>();
            pawn.WalkSpeed = 1000;
            LPCAnimatedSpriteRendererComponent s = player.AddComponent<LPCAnimatedSpriteRendererComponent>();
            s.Layer = 0.1f;
            s.CurrentAction = "death";
            ActionSpritesheetComponent a = player.AddComponent<ActionSpritesheetComponent>();
            LoadLPCSpritesheet(a, "Graphics\\Characters\\Example2\\spritesheet");
            return player;
        }

        public GameObject CreateChatBox()
        {
            GameObject player = new GameObject("Chat");
            player.RelativePosition = new Vector2(0, -30);
            player.AddComponent<MyChatControllerComponent>();
            // TODO: lines should be added on the fly and faded out over time.
            player.AddComponent<TextRendererComponent>();
            return player;
        }

        public void LoadLPCSpritesheet(ActionSpritesheetComponent s, string sheet)
        {
			s.Spritesheet = sheet;
            // Stopped Animation
			s.DefineAction("stopped_n", 1, 1000, 0);
			s.DefineAction("stopped_w", 1, 1000, 1);
			s.DefineAction("stopped_s", 1, 1000, 2);
			s.DefineAction("stopped_e", 1, 1000, 3);
			// Walk Animation
			s.DefineAction("walk_n", 9, 1000, 8);
			s.DefineAction("walk_w", 9, 1000, 9);
			s.DefineAction("walk_s", 9, 1000, 10);
			s.DefineAction("walk_e", 9, 1000, 11);
			// Death
			s.DefineAction("death", 6, 300, 20);
			s.DefineAction("dead", 1, 1000, 20);
        }
    }
}
