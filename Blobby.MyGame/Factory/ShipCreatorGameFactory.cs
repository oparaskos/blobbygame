﻿using Blobby.Core;
using Microsoft.Xna.Framework;
using Blobby.Core.Components;
using Blobby.MyGame.Components;

namespace Blobby.MyGame.Factory
{
    public class ShipCreatorGameFactory : IGameFactory
    {
        public Color[,] colours = new Color[3,5];
        ButtonFactory buttonFactory;
        public ShipCreatorGameFactory() {
			buttonFactory = new ButtonFactory();
			colours[0, 0] = Color.White;
			colours[1, 0] = Color.Black;
			colours[2, 0] = Color.Transparent;
            int cnt = (colours.Length/3 - 2);
			for (int i = 0; i < cnt; ++i)
            {
                byte intensity = (byte)(((float)i / 9.0f) * 255);
                colours[0,cnt- i] = new Color(intensity, 0 ,0);
                colours[1,cnt - i] = new Color(0, intensity, 0);
                colours[2,cnt - i] = new Color(0, 0, intensity);
            }
        }

        public GameObject CreateGame(IBlobbyGame game)
        {
			GameObject gameObject = new GameObject("Ship Creator");
            GameObject canvas = CreateCanvas(game);
			gameObject.AddChild(canvas);
            MyCanvasComponent c = canvas.GetComponent<MyCanvasComponent>();
            gameObject.AddChild(CreateDialog(game, c));
			gameObject.AddChild(CreateMouse(game));
			gameObject.AddComponent<CameraComponent>();
			gameObject.AddComponent<AutoScrollingBackgroundComponent>();
			SpriteRendererComponent sp = gameObject.AddComponent<SpriteRendererComponent>();
			sp.Texture = "Graphics\\space1";
			sp.Layer = -100;
            return gameObject;
		}

		public GameObject CreateMouse(IBlobbyGame game)
		{
			GameObject mouse = new GameObject();
			mouse.AddComponent<MyMouseCursorComponent>();
			SpriteRendererComponent src = mouse.AddComponent<SpriteRendererComponent>();
			src.Texture = "UI\\Pointer";
			src.Layer = 100;
			return mouse;
		}

        public GameObject CreateDialog(IBlobbyGame game, MyCanvasComponent canvas) {
            GameObject pallette = new GameObject("Dialog Window")
            {
                RelativePosition = new Vector2((game.Window.ClientBounds.Width / 2) - 60, 0)
            };
            Rectangle bounds = new Rectangle(-58, -((game.Window.ClientBounds.Height / 2) - 5), 114, game.Window.ClientBounds.Height - 10);
            DialogWindowComponent dialog = pallette.AddComponent<DialogWindowComponent>();
			dialog.Bounds = bounds;
            GameObject p = CreatePalette(game, canvas);
            pallette.AddChild(p);
            pallette.AddChild(CreatePanel(game));
            SliderFactory sliderFactory = new SliderFactory();
            ColorPaletteComponent pallete = p.GetComponent<ColorPaletteComponent>();
            GameObject slider = sliderFactory.CreateSlider((val) =>
            {
                pallete.Value = val;
            });
            slider.RelativePosition = new Vector2(0, -((game.Window.ClientBounds.Height / 2) - 90));
            pallette.AddChild(slider);
            return pallette;
		}

        public GameObject CreatePanel(IBlobbyGame game)
        {
            GameObject panel = new GameObject("Panel 1");
            UIPanelComponent panelComponent = panel.AddComponent<UIPanelComponent>();
            panelComponent.Bounds = new Rectangle(-50,-((game.Window.ClientBounds.Height / 2) - 105), 100, game.Window.ClientBounds.Height - 120);
            return panel;
        }

        public GameObject CreatePalette(IBlobbyGame game, MyCanvasComponent canvas)
        {
            GameObject pallette = new GameObject("Colour Pallete")
            {
                RelativePosition = new Vector2(0, -(game.Window.ClientBounds.Height / 2) + 45.0f)
            };
            ColorPaletteComponent p = pallette.AddComponent<ColorPaletteComponent>();
            p.OnColorPicked += (Color c) =>
            {
                canvas.BrushColor = c;
            };
            return pallette;
        }

        private GameObject CreateCanvas(IBlobbyGame game)
        {
            GameObject canvas = new GameObject("Ship Canvas");
            canvas.AddComponent<MyCanvasComponent>();
            canvas.AddComponent<MyVisualCenterComponent>();
            return canvas;
        }
    }
}