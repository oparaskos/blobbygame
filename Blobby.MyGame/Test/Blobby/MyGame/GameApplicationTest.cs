using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Blobby.Core;
using Blobby.Core.Components;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace Blobby.MyGame.Test
{
    [TestFixture]
    public class GameApplicationTest
	{
		[Test]
		public void TestInitialise()
		{
			GameApplication app = new GameApplication();
			Assert.That(app.Content.RootDirectory, Is.EqualTo("Content"));
		}
		[Test]
		public void TestInitialize()
		{
			GameApplication app = new GameApplication();
            app.Initialize();
			Assert.That(app.Content.RootDirectory, Is.EqualTo("Content"));
		}

    }
}