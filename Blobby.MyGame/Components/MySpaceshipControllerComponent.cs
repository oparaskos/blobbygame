﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Blobby.Core;
using Blobby.Core.Components;
using Blobby.Core.Physics;
using Blobby.MyGame.Factory;
using System.Linq;

namespace Blobby.MyGame.Components
{
    public class MySpaceshipControllerComponent : BlobbyComponent
    {
        private const string THRUSTER_ON = "Graphics\\thruster_boost";
        private const string THRUSTER_OFF = "Graphics\\thruster";
        private bool? thrusterState = null;
        public float TorpedoSpeed { get; set; } = 400;
        public float TurnSpeed { get; set; } = (float) Math.PI;
        public float LookingAtThreshold { get; set; } = 0.3f;

        private TorpedoFactory torpedoFactory;
        private RigidBodyComponent physicsBody;

        private MyBeamControllerComponent phaserBeamController = null;

        public bool IsFiringPhasers
        {
            get
            {
                // TODO: Talk to a phaser beam controller component instead so it can track its own energy levels and damage.
                return phaserBeamController != null && phaserBeamController.Firing;
            }
        }

        public MySpaceshipControllerComponent() : base()
        {
            torpedoFactory = new TorpedoFactory();
        }

        public override void Initialize()
        {
            base.Initialize();
            physicsBody = Owner.GetComponent<RigidBodyComponent>();
        }

        public override void Update(GameTime gameTime)
        {
            // Thrusters should be on if the ship is moving in the direction it is looking
            float similarity = Vector2.Dot(GetLookVector(), physicsBody.Velocity);
            bool thrustersOn = (!physicsBody.Stationary && similarity > 0.1f);
            ToggleThrusters(thrustersOn);
            phaserBeamController = Owner.FindComponentOnChild<MyBeamControllerComponent>();
        }

        public Vector2 GetLookVector()
        {
            return Vector2.Transform(-Vector2.UnitY, Matrix.CreateRotationZ(Owner.AbsoluteRotation));
        }

        public Vector2 GetPortVector()
        {
            return Vector2.Transform(Vector2.UnitX, Matrix.CreateRotationZ(Owner.AbsoluteRotation));
        }

        public void MoveToward(GameObject other, float deltaTime) { MoveToward(other.AbsolutePosition, deltaTime); }
        public void MoveToward(Vector2 location, float deltaTime)
        {
            LookAt(location, deltaTime);
            if(IsLookingAt(location))
            {
                ApplyThrust(5000, deltaTime);
            }
        }

        public void ApplyThrust(float amount, float deltaTime)
        {
            if (Math.Abs(amount * deltaTime) > PhysicsSubsystem.STOPPED_SPEED)
            {
                physicsBody.Impulse(GetLookVector() * (amount * deltaTime));
            }
        }

        public void LookAt(GameObject other, float deltaTime) { LookAt(other.AbsolutePosition, deltaTime); }
        public void LookAt(Vector2 location, float deltaTime)
        {
            Vector2 targetVector = location - Owner.AbsolutePosition;
            float dot = Vector2.Dot(GetPortVector(), targetVector);
            float maxTurnSpeed = TurnSpeed * deltaTime;
            float impulseAmount = MathHelper.Clamp(dot * maxTurnSpeed, -maxTurnSpeed, maxTurnSpeed);
            if (Math.Abs(impulseAmount) > 0.03f)
            {
               Owner.RelativeRotation += impulseAmount;
            }
        }

        public float GetLookAt(GameObject other) { return GetLookAt(other.AbsolutePosition);  }
        public float GetLookAt(Vector2 location)
        {
            return (float)Math.Atan2(location.Y - Owner.AbsolutePosition.Y, location.X - Owner.AbsolutePosition.X);
        }

        public bool IsLookingAt(GameObject other) { return IsLookingAt(other.AbsolutePosition); }
        public bool IsLookingAt(Vector2 location)
        {
            Vector2 targetVector = location - Owner.AbsolutePosition;
            return Vector2.Dot(GetLookVector(), targetVector) / targetVector.Length() > 0.5f;
        }

        public GameObject FireTorpedo()
        {
            Vector2 torpedoDirection = Vector2.Normalize(new Vector2((float)Math.Sin(Owner.AbsoluteRotation), -(float)Math.Cos(Owner.AbsoluteRotation)));
            Vector2 torpedoVelocity = physicsBody.Velocity + (torpedoDirection * TorpedoSpeed);
            Vector2 cannonLocation = Vector2.Add(Owner.AbsolutePosition, Vector2.Multiply(torpedoDirection, 45));
            GameObject torpedo = torpedoFactory.CreateTorpedo(cannonLocation, torpedoVelocity, Game);
            Owner.Parent.AddChild(torpedo);
            torpedo.Initialize();
            return torpedo;
        }

        public void FirePhasers()
        {
            if (phaserBeamController != null)
            {
                phaserBeamController.Firing = true;
            }
        }

        public void StopFiringPhasers()
        {
            if (phaserBeamController != null)
            {
                phaserBeamController.Firing = false;
            }
        }

        private void ToggleThrusters(bool newThrusterState)
        {
            if (thrusterState == null || thrusterState != newThrusterState)
            {
                IEnumerable<GameObject> thrusters = Owner.FindChildrenByName("Thruster");
                foreach (GameObject thruster in thrusters)
                {
                    if (newThrusterState)
                    {
                        thruster.GetComponent<SpriteRendererComponent>().Texture = THRUSTER_ON;
                    }
                    else
                    {
                        thruster.GetComponent<SpriteRendererComponent>().Texture = THRUSTER_OFF;
                    }
                }
                thrusterState = newThrusterState;
            }
        }
    }
}
