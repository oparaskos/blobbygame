﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Blobby.Core;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Blobby.MyGame.Components
{
    public class MyCanvasComponent : BlobbyComponent
	{
        int brushSize = 3;
		const int width = 1000;
		const int height = 1000;
        public Texture2D Texture { get; set; }
        public Color[] Data { get; set; } = new Color[width * height];
        public Color BrushColor = Color.Blue;

        public MyCanvasComponent()
        {
            for (int pixel = 0; pixel < Data.Length; pixel++)
            {
                Data[pixel] = Color.Transparent;
            }
        }

        public override void Initialize()
        {
            Texture = Game.GraphicsDevice.CreateTexture2D(width, height);
            Texture.SetData(Data);
        }

		public override void Update(GameTime gameTime)
		{

            Vector2 absoluteMousePosition = new Vector2(
                Mouse.GetState().Position.X - (Game.Window.ClientBounds.Width / 2),
                Mouse.GetState().Position.Y - (Game.Window.ClientBounds.Height / 2));
            brushSize = MathHelper.Clamp(Mouse.GetState().ScrollWheelValue/100, 1, 15);
			ButtonState leftButton = Mouse.GetState().LeftButton;
			ButtonState rightButton = Mouse.GetState().RightButton;
			if (leftButton == ButtonState.Pressed)
			{
                Vector2 canvasPosition = absoluteMousePosition + new Vector2(500, 500);
                Paint(canvasPosition, BrushColor, 2.0f, true, brushSize, brushSize);
			}
			else if (rightButton == ButtonState.Pressed)
			{
				Vector2 canvasPosition = absoluteMousePosition + new Vector2(500, 500);
                Paint(canvasPosition, Color.Transparent, 3.0f, true, brushSize, brushSize);
			}
        }

        private void Paint(Vector2 canvasPosition, Color color,
                           float threshold = 0.01f, bool hardEdge = false,
                           int brushWidth = 4, int brushHeight = 4)
        {
            
            if (canvasPosition.X > 0 && canvasPosition.Y > 0
                && canvasPosition.X < width && canvasPosition.Y < height)
            {
                int x = (int)Math.Floor(canvasPosition.X/3);
                int y = (int)Math.Floor(canvasPosition.Y/3);
                for (int dx = Math.Max(0, x - (brushWidth/2)); dx < Math.Min(width, x + (brushWidth/2)); ++dx)
                {
                    for (int dy = Math.Max(0, y - (brushHeight/2)); dy < Math.Min(height, y + (brushHeight / 2)); ++dy)
                    {
                        int index = dy * width + dx;
						int absDx = Math.Abs(dx - x);
						int absDy = Math.Abs(dy - y);

                        float distance = (absDx * absDx) + (absDy * absDy);
                        if(distance < threshold) {
                            Data[index] = color;
                        }
                        else if(!hardEdge)
                        {
							Color c = Data[index];
                            Color target = color * (1 / distance);
							Data[index] = addColors(c, target);
                        }
                    }
                }
            }
        }

        private static Color addColors(Color lhs, Color rhs)
		{
            int r = MathHelper.Clamp(lhs.R + rhs.R, Byte.MinValue, Byte.MaxValue);
			int g = MathHelper.Clamp(lhs.G + rhs.G, Byte.MinValue, Byte.MaxValue);
			int b = MathHelper.Clamp(lhs.B + rhs.B, Byte.MinValue, Byte.MaxValue);
			int a = MathHelper.Clamp(lhs.A + rhs.A, Byte.MinValue, Byte.MaxValue);
            return new Color(r, g, b, a);
        }

        public override void Draw(SpriteBatch spriteBatch)
		{
			Texture.SetData(Data);
            spriteBatch.Draw(
                Texture,
                new Vector2(-500, -500),
                color: Color.White,
                scale: new Vector2(3.0f)
            );
        }
    }
}
