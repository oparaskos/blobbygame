﻿using Blobby.Core.Components;
using Blobby.Core;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
// using SpaceshipBuilder;

namespace Blobby.MyGame.Components
{
    public class MyVisualCenterComponent : BlobbyComponent
    {
        ButtonState lastState = ButtonState.Released;

        public override void Update(GameTime gameTime)
        {
            ButtonState newState = Mouse.GetState().LeftButton;
			if(lastState != newState && newState == ButtonState.Released) 
            {
                Texture2D img = Owner.GetComponent<MyCanvasComponent>().Texture;
                //var visualCenter = VisualCenter.FromImage(img).ToVector2();
                //Owner.Position = visualCenter;
                //System.Console.WriteLine(visualCenter);
            }
			lastState = newState;
        }
    }
}
