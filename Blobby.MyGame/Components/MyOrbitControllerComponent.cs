﻿using Blobby.Core;
using Microsoft.Xna.Framework;
using Blobby.Core.Components;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using System.Collections.Generic;
using System;

namespace Blobby.MyGame.Components
{
    public class MyOrbitControllerComponent : BlobbyComponent
    {
        List<GameObject> affectedObjects = new List<GameObject>();

        public override void Initialize()
		{
            TriggerComponent c = Owner.FindComponentOnParent<TriggerComponent>();
			c.OnCollision += this.OnCollision;
			c.OnSeparation += this.OnSeperation;
		}

        public override void Update(GameTime gameTime) {
            foreach (GameObject affected in affectedObjects) {
                double angle = Vector2.UnitX.Angle(Owner.AbsolutePosition - affected.AbsolutePosition);
                angle = angle + (Math.PI / 2) - 0.3;
                Vector2 impulseDirection = Vector2.Normalize(
                    new Vector2(
                        (float)Math.Cos(angle),
                        (float)Math.Sin(angle)
                    )
                );
                affected.GetComponent<RigidBodyComponent>().Impulse(impulseDirection * gameTime.ElapsedGameTime.Milliseconds);
			}
        }

		public virtual bool OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
		{
			GameObject other = ((BlobbyComponent)fixtureB.UserData).Owner;
            affectedObjects.Add(other);
            System.Console.WriteLine("Collision Enter");
            return true;
		}

		public virtual void OnSeperation(Fixture fixtureA, Fixture fixtureB)
		{
			GameObject other = ((BlobbyComponent)fixtureB.UserData).Owner;
            affectedObjects.Remove(other);
            System.Console.WriteLine("Seperation");
		}
	}
}
