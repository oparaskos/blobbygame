﻿using Blobby.Core;
using Blobby.Core.Components;

namespace Blobby.MyGame.Components
{
    public class MyChatControllerComponent : BlobbyComponent
    {
        // Todo, have multiple lines, fade old lines out.
        private TextRendererComponent Text;

        public override void Initialize()
        {
            base.Initialize();
            Text = Owner.GetComponent<TextRendererComponent>();
            Text.Text = "";
        }

        public void ShowChatText(string v)
        {
            Text.Text = v;
        }
    }
}