using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Blobby.Core.Components;
using Blobby.Core;
using System.Collections.Generic;
using FarseerPhysics.Collision;
using FarseerPhysics.Dynamics;
using Blobby.Core.Physics;
using System;

namespace Blobby.MyGame.Components
{
    class MyExplosionControllerComponent : BlobbyComponent
    {

        public float BlastRadius { get; set; } = 400.0f;
        public float BlastPower { get; set; } = 40000.0f;

		public PhysicsSubsystem PhysicsSubsystem
		{
			get
			{
				return Owner.GetSubsystem<PhysicsSubsystem>();
			}
		}

        public override void Update(GameTime gameTime)
        {
            AnimatedSpriteRendererComponent animatedSpriteRendererComponent
                = Owner.GetComponent<AnimatedSpriteRendererComponent>();
            if (animatedSpriteRendererComponent.PastEnd)
            {
                Explode();
                Owner.Destroy();
            }
        }

        private void Explode()
        {
			AABB aabb;
            aabb.LowerBound = Owner.AbsolutePosition - new Vector2(BlastRadius, BlastRadius);
            aabb.UpperBound = Owner.AbsolutePosition + new Vector2(BlastRadius, BlastRadius);
            List<Fixture> fixtures = PhysicsSubsystem.World.QueryAABB(ref aabb);
            foreach (Fixture f in fixtures)
			{
                Body body = f.Body;
                GameObject bodyOwner = ((BlobbyComponent) body.UserData).Owner;
                if ((bodyOwner.AbsolutePosition - Owner.AbsolutePosition).Length() >= BlastRadius)
                {
                    continue;
                }
                ApplyBlastImpulse(body, bodyOwner);
			}   
        }

        void ApplyBlastImpulse(Body body, GameObject entity)
		{
            Vector2 blastDir = entity.AbsolutePosition - Owner.AbsolutePosition;
            float distance = blastDir.Length();
            if (distance < 0.001 && distance > -0.001)
            {
                return;
            }
			float invDistance = 1 / distance;
			float impulseMag = BlastPower * invDistance * invDistance;
            body.ApplyLinearImpulse(impulseMag * blastDir, entity.AbsolutePosition);
            MyShieldControllerComponent shield = entity.FindComponentOnChild<MyShieldControllerComponent>();
            if (shield != null)
            {
                shield.TakeDamage((int)impulseMag);
            }
		}
    }
}