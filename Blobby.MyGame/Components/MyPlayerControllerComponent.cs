using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using Blobby.Core;
using Blobby.Core.Components;
using Blobby.Core.Physics;

namespace Blobby.MyGame.Components
{
    public class MyPlayerControllerComponent : BlobbyComponent
    {
        private MySpaceshipControllerComponent ship;
        public int FireCooldown { get; set; } = 500;
        public bool CanFire { get; set; } = true;
        private int lastFireTime = 0;

        public override void Initialize()
        {
            base.Initialize();
            ship = Owner.GetComponent<MySpaceshipControllerComponent>();
        }

        public override void Update(GameTime gameTime)
        {
            float rotationVelocity = 4.5f;
            float delta = (float)gameTime.ElapsedGameTime.TotalSeconds;
            int now = Environment.TickCount;

            if (Game.Input.GetButton("AltFire"))
            {
                ship.FirePhasers();
            }
            else if(ship.IsFiringPhasers)
            {
                ship.StopFiringPhasers();
            }
            
            if (Game.Input.GetButton("Fire"))
            {
                if (CanFire || (now - lastFireTime) > FireCooldown)
                {
                    CanFire = false;
                    lastFireTime = now;
                    ship.FireTorpedo();
                }
            } else {
                CanFire = true;
            }

            if (Game.Input.GetButton("Right"))
            {
                Owner.RelativeRotation += delta * rotationVelocity;
            }
            if (Game.Input.GetButton("Left"))
            {
                Owner.RelativeRotation -= delta * rotationVelocity;
            }

            float velocity = 5000;
            if (Game.Input.GetButton("Forward"))
            {
                ship.ApplyThrust(velocity, delta);
            }
            if (Game.Input.GetButton("Backward"))
            {
                ship.ApplyThrust(-1 * velocity, delta);
            }
        }
    }
}