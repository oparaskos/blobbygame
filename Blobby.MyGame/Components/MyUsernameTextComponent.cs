﻿using Blobby.Core;
using Blobby.Core.Components;
using Blobby.Core.Steamworks;

namespace Blobby.MyGame.Components
{
    public class MyUsernameTextComponent : BlobbyComponent
    {
        private SteamworksSubsystem steamworksSubsystem;

        public override void Initialize()
        {
            base.Initialize();
            steamworksSubsystem = Owner.GetSubsystem<SteamworksSubsystem>();
            if (steamworksSubsystem.Enabled && steamworksSubsystem.IsSteamRunning)
            {
                Owner.GetComponent<TextRendererComponent>().Text = steamworksSubsystem.SteamworksClient.Username;
            }
        }
    }
}