﻿using Microsoft.Xna.Framework;
using System;
using System.Linq;
using Blobby.Core;
using Blobby.Core.Components;
using Blobby.Core.Physics;
using Blobby.MyGame.AI;

namespace Blobby.MyGame.Components
{
    public class MyHumanoidControllerComponent : BlobbyComponent
    {
        public string LookDirection { get; set; } = "n";
        public float WalkSpeed { get; set; } = 5000;
        public Vector2 WalkDirection { get; set; } = Vector2.Zero;
        private RigidBodyComponent Physics { get; set; }
        private Vector2? MoveToTarget { get; set; } = null;

        public PhysicsSubsystem PhysicsSubsystem
        {
            get
            {
                return Owner.GetSubsystem<PhysicsSubsystem>();
            }
        }

        public override void Initialize()
        {
            base.Initialize();
            Physics = Owner.GetComponent<RigidBodyComponent>();
        }

        public override void Update(GameTime gameTime)
        {
            if (MoveToTarget.HasValue)
            {
                WalkDirection = Vector2.Normalize(MoveToTarget.Value - Owner.AbsolutePosition);
            }
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            Vector2 impulseVector = WalkDirection * WalkSpeed * deltaTime;
            bool isMoving = WalkDirection.LengthSquared() > PhysicsSubsystem.STOPPED_SPEED;
            ComputeDamping(Physics, isMoving);
            ComputeLookDirection(Physics.Velocity);
            if (Math.Abs(impulseVector.LengthSquared()) > PhysicsSubsystem.STOPPED_SPEED)
            {
                Physics.Impulse(impulseVector);
                State_Walking();
            }
            else
            {
                State_Idle();
            }
            WalkDirection = Vector2.Zero;
        }

        public void OnSpokenTo(Conversation.Message message)
        {
            if (message.From != this && !message.conversation.Completed) {
                message.conversation.Completed = true;
                Say("Bye!", message.conversation);
            }
        }

        internal void Say(string v, Conversation conversation)
        {
            ShowChatText(v);
            conversation.Say(this, v);
        }

        public void ShowChatText(string v)
        {
            MyChatControllerComponent text = Owner.FindComponentOnChild<MyChatControllerComponent>();
            text.ShowChatText(v);
        }

        public bool IsAtLocation(GameObject other, float radius = 0) { return IsAtLocation(other.AbsolutePosition, radius); }
        public bool IsAtLocation(Vector2 location, float radius = 0)
        {
            return null != PhysicsSubsystem.QueryGameObjectsAtLocation(location, radius)
                .Where(g => g == Owner)
                .FirstOrDefault();
        }

        internal void PerformAction(string action)
        {
            var sprite = Owner.GetComponent<LPCAnimatedSpriteRendererComponent>();
            sprite.CurrentAction = action + '_' + LookDirection;
        }
        
        private void ComputeDamping(RigidBodyComponent physics, bool isMoving)
        {
            if (isMoving)
            {
                physics.Damping = 0.001f;
            }
            else
            {
                // Once the player stops moving simulate very frictiony feet.
                physics.Damping = 0.01f;
            }
        }

        public void StopMoving()
        {
            MoveToTarget = null;
        }

        public String ComputeLookDirection(Vector2 velocity)
        {
            // If we stop look in the same direction as just before we stopped.
            if (Math.Abs(velocity.LengthSquared()) < PhysicsSubsystem.STOPPED_SPEED)
                return LookDirection;

            if (Math.Abs(velocity.X) > Math.Abs(velocity.Y))
            {
                if (velocity.X > 0)
                {
                    LookDirection = "e";
                }
                else
                {
                    LookDirection = "w";
                }
            }
            else
            {
                if (velocity.Y > 0)
                {
                    LookDirection = "s";
                }
                else
                {
                    LookDirection = "n";
                }
            }
            return LookDirection;
        }

        public void MoveTo(GameObject other) { MoveTo(other.AbsolutePosition); }
        public void MoveTo(Vector2 location)
        {
            MoveToTarget = location;
        }

        private void State_Walking()
        {
            PerformAction("walk");
        }

        private void State_Idle()
        {
            PerformAction("stopped");
        }
    }
}
