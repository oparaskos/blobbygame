﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using Blobby.Core;
using Blobby.Core.Components;
using Blobby.Core.Physics;
using Blobby.MyGame.AI;

namespace Blobby.MyGame.Components
{
	public class MyPlayerHumanoidControllerComponent : BlobbyComponent
    {
        private MyHumanoidControllerComponent Humanoid { get; set; }

        public override void Initialize()
        {
            base.Initialize();
            Humanoid = Owner.GetComponent<MyHumanoidControllerComponent>();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            
            KeyboardState keyState = Keyboard.GetState();
            Vector2 moveDirection = Vector2.Zero;
            if (Game.Input.GetButton("Right"))
            {
                moveDirection += Vector2.UnitX;
            }
            if (Game.Input.GetButton("Left"))
            {
                moveDirection -= Vector2.UnitX;
            }
            if (Game.Input.GetButton("Forward"))
            {
                moveDirection -= Vector2.UnitY;
            }
            if (Game.Input.GetButton("Backward"))
            {
                moveDirection += Vector2.UnitY;
            }
            Humanoid.WalkDirection = moveDirection;
        }

        internal void Beckon(MyAIHumanoidControllerComponent npc)
        {
            // Put "Hey!" into a text renderer above the players' head.
            Conversation conversation = new Conversation();
            conversation.Join(Humanoid);
            Humanoid.Say("Hey!", conversation);
            // Add a task on the other character to move to me and start a conversation.
            ConversationBehaviour conversationBehaviour = new ConversationBehaviour(Humanoid);
            conversationBehaviour.Conversation = conversation;
            npc.CurrentTask = conversationBehaviour;
        }
    }
}
