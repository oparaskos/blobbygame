﻿using System;
using System.Collections.Generic;
using System.Linq;
using Blobby.Core;
using Microsoft.Xna.Framework;
using Blobby.Core.Components;
using Blobby.MyGame.AI;

namespace Blobby.MyGame.Components
{
    class MyAIHumanoidControllerComponent : BlobbyComponent
    {
        private List<AIBehaviour> ToDoList { get; set; }
        private MyHumanoidControllerComponent Humanoid { get; set; }
        private ClickableComponent Clickable { get; set; }

        // TODO: redo this.
        public AIBehaviour CurrentTask
        {
            get
            {
                if(ToDoList.Count() == 0)
                {
                    return null;
                }
                return ToDoList.ElementAt(0);
            }

            set
            {
                ClearTasks();
                if (value != null)
                {
                    Console.WriteLine("Setting task " + value);
                    QueueTask(value);
                }
            }
        }

        public AIBehaviour NextTask
        {
            get
            {
                return ToDoList.ElementAt(1);
            }

            set
            {
                if (value != null)
                {
                    AIBehaviour currentTask = CurrentTask;
                    ClearTasks();
                    if (currentTask != null)
                    {
                        ToDoList.Add(currentTask);
                    }
                    ToDoList.Add(value);
                }
            }
        }

        public MyAIHumanoidControllerComponent()
        {
            ToDoList = new List<AIBehaviour>();
        }

        public override void Initialize()
        {
            base.Initialize();
            Humanoid = Owner.GetComponent<MyHumanoidControllerComponent>();
            Clickable = Owner.GetComponent<ClickableComponent>();
            Clickable.OnClick += OnClick;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (CurrentTask != null && CurrentTask.Completed)
            {
                Console.WriteLine("Task completed " + CurrentTask);
                // Go to the next task.
                StartNextTask();
            }
            if (CurrentTask == null)
            {
                // If there is no task to perform return.
                Humanoid.PerformAction("idle");
                return;
            }
            // Go to the next task or perform it.
            if (CurrentTask.Location.HasValue && (CurrentTask.Location.Value - Humanoid.Owner.AbsolutePosition).Length() > CurrentTask.Radius)
            {
                Humanoid.MoveTo(CurrentTask.Location.Value);
            }
            else
            {
                Humanoid.StopMoving();
                CurrentTask.Update(gameTime);
            }
        }

        public void QueueTask(AIBehaviour task)
        {
            task.Owner = Owner;
            ToDoList.Add(task);
        }

        public void ClearTasks()
        {
            ToDoList.Clear();
        }

        public void StartNextTask()
        {
            ToDoList.RemoveAt(0);
            Console.WriteLine("Started task " + CurrentTask);
        }

        // When the player clicks an AI character they are begin beckoned.
        private void OnClick(object sender, EventArgs args)
        {
            Owner.Root.FindComponentOnChild<MyPlayerHumanoidControllerComponent>()
                .Beckon(this);
        }
    }
}
