﻿using Blobby.Core;
using Blobby.Core.Components;
using Microsoft.Xna.Framework;

namespace Blobby.MyGame.Components
{
    public class MyEnergyProgressBarComponent : BlobbyComponent
    {
        public GameObject Target { get; set; }

        private MyEnergySourceComponent energySourceController;

        public override void Initialize()
        {
            energySourceController = Target.FindComponentOnChild<MyEnergySourceComponent>();
        }

        public override void Update(GameTime gameTime)
        {
            Owner.GetComponent<ProgressBarComponent>().Progress = energySourceController.AvailableEnergy / energySourceController.EnergyCapacity;
        }

    }
}