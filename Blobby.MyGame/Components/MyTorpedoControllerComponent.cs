using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Blobby.Core;
using Blobby.Core.Components;
using System;
using Blobby.Core.Physics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using Blobby.MyGame.Factory;

namespace Blobby.MyGame.Components
{
    class MyTorpedoControllerComponent : BlobbyComponent
    {
        private float _explodeSpeedSquared = PhysicsSubsystem.STOPPED_SPEED;
        public float ExplodeSpeed
        {
            get { return (float)Math.Sqrt(_explodeSpeedSquared); }
            set { _explodeSpeedSquared = value * value; }
        }

        public ExplosionFactory explosionFactory;

        public MyTorpedoControllerComponent() : base()
        {
            explosionFactory = new ExplosionFactory();
		}

        public override void Initialize()
        {
            RigidBodyComponent c = Owner.FindComponentOnParent<RigidBodyComponent>();
            c.Body.OnCollision += this.OnCollision;
        }

        public override void Update(GameTime gameTime)
        {
            RigidBodyComponent physics = Owner.GetComponent<RigidBodyComponent>();
            if (physics.Velocity.LengthSquared() < _explodeSpeedSquared)
            {
                Explode();
            }
        }

        public void Explode()
        {
            GameObject explosion = explosionFactory.CreateExplosion(Game, Owner.AbsolutePosition);
            Owner.Parent.AddChild(explosion);
            Owner.Destroy();
		}

		public virtual bool OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
		{
            Explode();
			return true;
		}
    }
}