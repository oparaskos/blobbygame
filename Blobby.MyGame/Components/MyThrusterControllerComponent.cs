using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Blobby.Core;
using Blobby.Core.Components;

namespace Blobby.MyGame.Components
{
    class MyThrusterControllerComponent : BlobbyComponent
	{
		private const string THRUSTER_ON = "Graphics\\thruster_boost";
		private const string THRUSTER_OFF = "Graphics\\thruster";

        public override void Update(GameTime gameTime)
        {
            KeyboardState keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(Keys.W) || keyState.IsKeyDown(Keys.Up))
            {
                Owner.GetComponent<SpriteRendererComponent>().Texture = THRUSTER_ON;
			}
			else
			{
				Owner.GetComponent<SpriteRendererComponent>().Texture = THRUSTER_OFF;
            }
        }

    }
}