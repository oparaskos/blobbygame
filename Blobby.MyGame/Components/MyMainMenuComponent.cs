﻿using System;
using System.Linq;
using Blobby.Core;
using Blobby.Core.Components;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Blobby.MyGame.Factory;

namespace Blobby.MyGame
{
    public class MyMainMenuComponent : BlobbyComponent
    {
        private TopDownGameFactory topDownGameFactory;
        private MainGameFactory mainGameFactory;
        private ShipCreatorGameFactory shipCreatorGameFactory;
        private MainMenuFactory mainMenuFactory;
        private GameObject gameRoot;
        private bool _escapeIsDown;

        public bool Running { get; set; } = false;

        public MyMainMenuComponent()
        {
            topDownGameFactory = new TopDownGameFactory();
            mainGameFactory = new MainGameFactory();
            mainMenuFactory = new MainMenuFactory();
            shipCreatorGameFactory = new ShipCreatorGameFactory();
        }

        internal void OnStartGame(object sender, EventArgs args)
        {
            StartGame(mainGameFactory);
        }

        internal void OnSecondButton(object sender, EventArgs args)
        {
            StartGame(topDownGameFactory);
        }

        internal void OnStartShipCreator(object sender, EventArgs args)
        {
            StartGame(shipCreatorGameFactory);
        }

        internal void StartGame(IGameFactory gameFactory)
        {
            // Stop any running game before starting the new one.
            if (Running)
            {
                StopGame();
            }
            gameRoot = gameFactory.CreateGame(Game);
            Owner.AddChild(gameRoot);
            Owner.FindChildrenByName("Main Menu").First().Enabled = false;
            gameRoot.Initialize();
            gameRoot.FindComponentOnChild<CameraComponent>().Active = true;
            Running = true;
        }

        internal void StopGame()
        {
            Owner.FindChildrenByName("Main Menu").First().Enabled = true;
            Owner.GetComponent<CameraComponent>().Active = true;
            gameRoot.Destroy();
            Running = false;
        }

        internal void OnQuit(object sender, EventArgs args)
        {
            Game.Exit();
        }

        internal void OnEscapePressed()
        {
            if (Running)
            {
                StopGame();
            }
            else
            {
                OnQuit(this,null);
            }
        }
   
        public override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (Game.Input.GetButton("Exit"))
            {
                _escapeIsDown = true;
                Console.WriteLine("Exit key is down - Exit()");
            }

            if(_escapeIsDown)
            {
                if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Released
                    && Keyboard.GetState().IsKeyUp(Keys.Escape))
                {
                    _escapeIsDown = false;
                    OnEscapePressed();
                }
            }
        }
    }
}