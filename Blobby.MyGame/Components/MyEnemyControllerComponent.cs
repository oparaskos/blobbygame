using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using Blobby.Core;
using Blobby.Core.Components;
using Blobby.Core.Physics;

namespace Blobby.MyGame.Components
{
    class MyEnemyControllerComponent : BlobbyComponent
    {
        private MySpaceshipControllerComponent ship;
        public float FollowDistance = 200f;

        public override void Initialize()
        {
            base.Initialize();
            ship = Owner.GetComponent<MySpaceshipControllerComponent>();
        }

        public GameObject Following { get; set; }

        public override void Update(GameTime gameTime)
        {
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            if(Math.Abs((Owner.AbsolutePosition - Following.AbsolutePosition).Length()) > FollowDistance) {
                ship.MoveToward(Following, deltaTime);
            } else
            {
                ship.LookAt(Following, deltaTime);
            }
        }
    }
}