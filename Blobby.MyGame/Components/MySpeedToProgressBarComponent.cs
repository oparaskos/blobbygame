using Microsoft.Xna.Framework;
using Blobby.Core;
using Blobby.Core.Components;

namespace Blobby.MyGame.Components
{
    class MySpeedToProgressBarComponent : BlobbyComponent
    {

        public float MaxSpeed
        {
            get;
            set;
        } = 10;
        public GameObject Target { get; set; }
        public MySpeedToProgressBarComponent() : base() { }

        public override void Update(GameTime gameTime) {
            RigidBodyComponent physicsComponent = Target.GetComponent<RigidBodyComponent>();
            Owner.GetComponent<ProgressBarComponent>().Progress = physicsComponent.Velocity.Length() / MaxSpeed;
        }

    }
}