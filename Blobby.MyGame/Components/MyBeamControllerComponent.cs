﻿using Blobby.Core;
using Blobby.Core.Components;
using Microsoft.Xna.Framework;
using System;

namespace Blobby.MyGame.Components
{
    public class MyBeamControllerComponent : BlobbyComponent, IEnergyConsumer
    {
        private MyEnergySourceComponent shipEnergyComponent;

        public float DepletionRate { get; set; } = 50f;

        public bool Firing
        {
            get
            {
                return _firing;
            }
            set
            {
                _firing = value;
                Owner.GetComponent<SpriteRendererComponent>().Visible = _firing;
            }
        }


        private bool _firing = false;

        public override void Initialize()
        {
            base.Initialize();
            shipEnergyComponent = Owner.FindComponentOnParent<MyEnergySourceComponent>();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            float Energy = shipEnergyComponent.AvailableEnergy;
            if(Firing)
            {
                shipEnergyComponent.AddConsumer(this);
                if(Energy <= 0.01)
                {
                    // Dont fire if the ship is out of energy
                    Firing = false;
                }
            }
            else
            {
                shipEnergyComponent.RemoveConsumer(this);
            }
        }
    }
}