﻿using Blobby.Core;
using Blobby.Core.Components;
using Microsoft.Xna.Framework;

namespace Blobby.MyGame.Components
{
    public class MyHealthIndicatorComponent : BlobbyComponent
    {
        private ProgressBarComponent progressBarComponent;
        private MyShieldControllerComponent healthComponent;

        public override void Initialize()
        {
            base.Initialize();
            progressBarComponent = Owner.GetComponent<ProgressBarComponent>();
            healthComponent = Owner.Parent.FindComponentOnChild<MyShieldControllerComponent>();
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            progressBarComponent.Progress = healthComponent.Health / (float) healthComponent.MaxHealth;
        }
    }
}