﻿using Blobby.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Blobby.Core.Util;

namespace Blobby.MyGame.Components
{
    /// <summary>
    /// Component to represent total ship energy for a given ship
    /// </summary>
    class MyEnergySourceComponent : BlobbyComponent
    {
        public float EnergyCapacity { get; set; } = 100f;
        public float AvailableEnergy { get; set; } = 60f;

        // Recharge rate while there are no consumers.
        public float EnergyRechargeRate { get; set; } = 20f;

        private float depletionRate = 0;
        private float productionRate = 0;
        private ICollection<IEnergyConsumer> consumers = new HashSet<IEnergyConsumer>();
        private ICollection<IEnergyProducer> producers = new HashSet<IEnergyProducer>();

        public void AddConsumer(IEnergyConsumer o)
        {
            if (o.DepletionRate < 0)
            {
                throw new ArgumentException("Depletion rate should be 0, for recharge use AddProducer");
            }
            if (!consumers.Contains(o))
            {
                consumers.Add(o);
                depletionRate += o.DepletionRate;
            }
        }

        public void RemoveConsumer(IEnergyConsumer o)
        {
            if (consumers.Remove(o))
            {
                depletionRate -= o.DepletionRate;
            }
        }

        public void AddProducer(IEnergyProducer o)
        {
            if (o.ProductionRate < 0)
            {
                throw new ArgumentException("Production rate should be positive, for discharge use AddConsumer");
            }
            if (!producers.Contains(o))
            {
                producers.Add(o);
                productionRate += o.ProductionRate;
            }
        }

        public void RemoveProducer(IEnergyProducer o)
        {
            if (producers.Remove(o))
            {
                productionRate -= o.ProductionRate;
            }
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;
            if (!consumers.IsEmpty() || !producers.IsEmpty())
            {
                float deltaE = (-depletionRate + productionRate) * deltaTime;
                AvailableEnergy = MathHelper.Clamp(AvailableEnergy + deltaE, 0, EnergyCapacity);
            }
            else
            {
                // If there are no producers or consumers then go up at a slow rate to the capacity.
                AvailableEnergy = MathHelper.Clamp(AvailableEnergy + EnergyRechargeRate * deltaTime, 0, EnergyCapacity);
            }
        }
    }

    public interface IEnergyProducer
    {
        float ProductionRate { get; }
    }

    public interface IEnergyConsumer
    {
        float DepletionRate { get; }
    }
}
