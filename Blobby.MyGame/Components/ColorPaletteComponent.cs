﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Blobby.Core;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace Blobby.MyGame.Components
{
    public class ColorPaletteComponent : BlobbyComponent
	{
        public delegate void OnColourPickedEventHandler(Color color);
        public event OnColourPickedEventHandler OnColorPicked;

		const int width = 100;
		const int height = 60;
        Texture2D textureData;
		public Color[] Data { get; set; } = new Color[width * height];
		public float _value = 1.0f;
        public bool dirty = false;
        public float Value
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
                dirty = true;
            }
        }

        public override void Initialize()
        {
			textureData = Game.GraphicsDevice.CreateTexture2D(width, height);
            GenerateGrid();
        }

        private void GenerateGrid()
        {
			for (int x = 0; x < width; ++x)
			{
				for (int y = 0; y < height; ++y)
				{
					Data[y * width + x] = new HueSaturationValue(((float)x / width) * 360.0f, (float)y / height, Value).ToColor();
				}
			}
			textureData.SetData(Data);
        }

		public override void Update(GameTime gameTime)
		{
            if(dirty) 
            {
                dirty = false;
                GenerateGrid();
            }
			Vector2 absoluteMousePosition = new Vector2(
				Mouse.GetState().Position.X - (Game.Window.ClientBounds.Width / 2),
				Mouse.GetState().Position.Y - (Game.Window.ClientBounds.Height / 2))
                - Owner.AbsolutePosition;
			ButtonState leftButton = Mouse.GetState().LeftButton;
            if (leftButton == ButtonState.Pressed)
            {
                Vector2 canvasPosition = absoluteMousePosition + new Vector2((width / 2), (height / 2));
                int x = (int)Math.Floor(canvasPosition.X);
				int y = (int)Math.Floor(canvasPosition.Y);

                if (x > 0 && y > 0
                    && x < width && y < height)
                {
                    int index = y * width + x;
                    OnColorPicked?.Invoke(Data[index]);
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
		{
			textureData.SetData(Data);
            spriteBatch.Draw(textureData,
                             Owner.AbsolutePosition + new Vector2(-(width/2), -(height/2)),
                             Color.White);
        }
    }
}
