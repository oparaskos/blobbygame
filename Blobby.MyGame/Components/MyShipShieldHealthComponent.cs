using Microsoft.Xna.Framework;
using Blobby.Core;
using Blobby.Core.Components;
using System.Linq;

namespace Blobby.MyGame.Components
{
    class MyShipShieldHealthComponent : BlobbyComponent
    {
        MyShieldControllerComponent shieldHealth = null;
        public MyShipShieldHealthComponent() : base() { }

        public override void Update(GameTime gameTime)
        {
            if (shieldHealth == null)
            {
                shieldHealth = Owner.Root.FindChildrenByName("Player Spaceship").First()
                    .FindComponentOnChild<MyShieldControllerComponent>();
            }
            Owner.GetComponent<TextRendererComponent>()
                .Text = "Health: " + shieldHealth.Health.ToString() + "/" + shieldHealth.MaxHealth.ToString();
        }

    }
}