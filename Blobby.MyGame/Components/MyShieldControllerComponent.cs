﻿using Blobby.Core;
using Microsoft.Xna.Framework;
using Blobby.Core.Physics;
using Blobby.Core.Components;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using Blobby.Core.MathUtil;
using System;

namespace Blobby.MyGame.Components
{
    public class MyShieldControllerComponent : BlobbyComponent
    {

        public double hitTime = 0;
        public double effectDuration = 3000;
        public int collisionCount = 0;
        private SpriteRendererComponent shieldSprite;

        public int MaxHealth { get; set; } = 100;
        public int Health { get; set; } = 100;

        public override void Initialize()
		{
			shieldSprite = Owner.GetComponent<SpriteRendererComponent>();
            RigidBodyComponent c = Owner.FindComponentOnParent<RigidBodyComponent>();
            c.Body.OnCollision += this.OnCollision;
            c.Body.OnSeparation += this.OnSeperation;
        }

        public override void Update(GameTime gameTime)
        {
            double elapsedSinceLastHit = TimeUtil.GetMillis() - hitTime;
            float alpha = Blobby.Core.MathUtil.MathHelper.Lerp(
                0.9f, 0.1f,
                Microsoft.Xna.Framework.MathHelper.Clamp((float) ((double) elapsedSinceLastHit / (double)effectDuration), 0, 1));
            if (collisionCount > 0)
            {
                alpha = ((float)MaxHealth / Health);
            }
            shieldSprite.Alpha = alpha;

		}

		public virtual bool OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
		{
            ++collisionCount;
			return true;
		}

		public virtual void OnSeperation(Fixture fixtureA, Fixture fixtureB)
		{
            --collisionCount;
			hitTime = TimeUtil.GetMillis();
		}

        public void TakeDamage(int damage)
        {
            Health -= damage;
        }
    }
}
