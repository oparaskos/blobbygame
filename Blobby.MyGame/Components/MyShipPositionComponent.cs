using Microsoft.Xna.Framework;
using Blobby.Core;
using Blobby.Core.Components;

namespace Blobby.MyGame.Components
{
    class MyShipPositionComponent : BlobbyComponent
    {
        public MyShipPositionComponent() : base() { }

        public override void Update(GameTime gameTime)
        {
            Owner.GetComponent<TextRendererComponent>()
                .Text = Owner.AbsolutePosition.ToString();
        }

    }
}