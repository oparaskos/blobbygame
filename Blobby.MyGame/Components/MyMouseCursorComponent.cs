using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using Blobby.Core;
using Blobby.Core.Physics;
using Blobby.Core.Components;
using System.Linq;

namespace Blobby.MyGame.Components
{
    class MyMouseCursorComponent : BlobbyComponent
    {
        private ButtonState lastLeftButtonState;

        private GameObject cameraObject;
        private SpriteRendererComponent mouseSprite;

        public PhysicsSubsystem PhysicsSubsystem
        {
            get
            {
                return Owner.GetSubsystem<PhysicsSubsystem>();
            }
        }

        public Vector2 OnScreenCoordinates
        {
            get
            {
                Vector2 absoluteMousePosition = new Vector2(
                    Mouse.GetState().Position.X - (Game.Window.ClientBounds.Width / 2),
                    Mouse.GetState().Position.Y - (Game.Window.ClientBounds.Height / 2));
                return absoluteMousePosition;
            }
        }

        public MyMouseCursorComponent() : base()
        {
            lastLeftButtonState = Mouse.GetState().LeftButton;
        }

        public override void Initialize()
        {
            cameraObject = ((CameraComponent)Owner.Game.ActiveCamera)?.Owner;
            mouseSprite = Owner.GetComponent<SpriteRendererComponent>();
        }

        public override void Update(GameTime gameTime)
        {
            Owner.RelativePosition = OnScreenCoordinates;
            var mouseHasMoved = true;
            if(mouseHasMoved) {
                CheckHovering();
            }
            ButtonState leftButton = Mouse.GetState().LeftButton;
            if (lastLeftButtonState == ButtonState.Pressed)
            {
                if (leftButton == ButtonState.Released)
                {
                    ClickFinished();
                    lastLeftButtonState = leftButton;
                }
            }
            else
            {
                if (leftButton == ButtonState.Pressed)
                {
                    OnClick();
                    lastLeftButtonState = leftButton;
                }
            }
        }

        public void CheckHovering()
        {

            GameObject physicsObject = PhysicsSubsystem
                .QueryGameObjectsAtLocation(Owner.AbsolutePosition)
                .FirstOrDefault();
            if (physicsObject != null)
            {
                if (physicsObject.HasComponent<ClickableComponent>())
                {
                    mouseSprite.Texture = physicsObject.GetComponent<ClickableComponent>().Cursor;
                }
            } else {
                mouseSprite.Texture = "UI\\Pointer";
            }
        }

        public void OnClick()
        {
            GameObject physicsObject = PhysicsSubsystem
                .QueryGameObjectsAtLocation(Owner.AbsolutePosition)
                .FirstOrDefault();
            if (physicsObject != null)
            {
                if(physicsObject.HasComponent<ClickableComponent>())
                {
                    physicsObject.GetComponent<ClickableComponent>().DoClick();
                }
                else if (cameraObject != null && cameraObject.HasComponent<FollowingCameraComponent>())
                {
                    FollowingCameraComponent followingCamera = cameraObject
                        .GetComponent<FollowingCameraComponent>();
                    followingCamera.Target = physicsObject;
                    followingCamera.Focusing = !followingCamera.Focusing;
                }
            }
        }

        public void ClickFinished()
        {
        }
    }
}