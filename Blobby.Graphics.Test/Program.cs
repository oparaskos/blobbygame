﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Drawing;
using Blobby.Graphics.Algorithm.MarchingSquares;
using Blobby.Graphics.Algorithm.VertexLoopSimplify;
using TriangleNet.Geometry;
using TriangleNet.IO;
using TriangleNet.Meshing;

namespace Blobby.Graphics.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            Image img = LoadFile();
            GraphicsUnit u = GraphicsUnit.Pixel;
            RectangleF rect = img.GetBounds(ref u);
            List<System.Drawing.Point> outline = OutlineObject(img);
            ICollection<TriangleNet.Topology.Triangle> triangles = Triangulate(img, outline);
            MeshToSvg(rect, outline, triangles);
        }

        private static ICollection<TriangleNet.Topology.Triangle> Triangulate(Image img, List<System.Drawing.Point> outline)
        {
            Polygon polygon = new Polygon();
            polygon.Add(new Contour(outline.Select((p) => new Vertex(p.X, p.Y)), 1, false));
            var options = new ConstraintOptions() { ConformingDelaunay = true };
            var quality = new QualityOptions() { MinimumAngle = 25 };

            // Triangulate the polygon
            IMesh m = polygon.Triangulate(options, quality);
            var triangles = m.Triangles;
            return triangles;
        }

        private static List<System.Drawing.Point> OutlineObject(Image img)
        {
            Bitmap map = new Bitmap(img);
            List<System.Drawing.Point> points = map.MarchingSquares();
            points = points.Simplify();
            return points;
        }

        private static Image LoadFile()
        {
            string fileName = "file.png";
            using (Stream BitmapStream = System.IO.File.Open(fileName, System.IO.FileMode.Open))
            {
                return Image.FromStream(BitmapStream);
            }
        }

        private static void MeshToSvg(RectangleF rect, List<System.Drawing.Point> outline, ICollection<TriangleNet.Topology.Triangle> triangles)
        {
            Console.WriteLine("<svg height=\"" + rect.Height + "\" width=\"" + rect.Width + "\">");
            OutlineToSvg(outline);
            TrianglesToSvg(triangles);
            Console.WriteLine("</svg>");
        }

        private static void OutlineToSvg(List<System.Drawing.Point> points)
        {
            Console.Write("\t\t<path d=\"");
            System.Drawing.Point startPoint = points.ElementAt(0);
            Console.Write("M" + startPoint.X + "," + startPoint.Y + " ");
            foreach (System.Drawing.Point p in points)
            {
                if (p != startPoint)
                {
                    Console.Write("L" + p.X + "," + p.Y + " ");
                }
            }
            Console.WriteLine(" Z\" style=\"opacity:1;fill:#ff7100;fill-opacity:0.15;stroke:#930000;stroke-width:0.1;stroke-opacity:1\"/>");
        }

        private static void TrianglesToSvg(ICollection<TriangleNet.Topology.Triangle> triangles)
        {
            Console.WriteLine("\t<g>");
            foreach (var triangle in triangles)
            {
                Vertex v0 = triangle.GetVertex(0);
                Vertex v1 = triangle.GetVertex(1);
                Vertex v2 = triangle.GetVertex(2);

                Console.WriteLine("\t<path d=\"" +
                    " M " + v0.X + " " + v0.Y +
                    " L " + v1.X + " " + v1.Y +
                    " L " + v2.X + " " + v2.Y +
                    " Z\" style=\"opacity:1;fill:#000079;fill-opacity:0.15;stroke:#550092;stroke-width:0.01;stroke-opacity:0.5\" />");
            }
            Console.WriteLine("\t</g>");
        }
    }
}
